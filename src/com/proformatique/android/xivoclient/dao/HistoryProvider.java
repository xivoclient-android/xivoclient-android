package com.proformatique.android.xivoclient.dao;

import org.xivo.cti.model.CallType;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import com.proformatique.android.xivoclient.tools.Constants;

/**
 * Provides the user's history, with call directions, durations, fullnames and dates.
 */
public class HistoryProvider extends ContentProvider {
	
	private final static String TAG = "History provider";
	
	public final static String PROVIDER_NAME = Constants.PACK + ".history";
	public final static Uri CONTENT_URI = Uri.parse("content://" + PROVIDER_NAME + "/history");
	private static final String CONTENT_TYPE
		= "vnd.android.cursor.dir/vnd.proformatique.xivo.history";
	private static final String CONTENT_ITEM_TYPE
		= "vnd.android.cursor.item/vnd.proformatique.xivo.history";
	
	/*
	 * Columns
	 */
	public static final String _ID = "_id";
	public static final String DURATION = "duration";
	public static final String TERMIN = "termin";
	public static final String DIRECTION = "direction";
	public static final String FULLNAME = "fullname";
	public static final String TS = "ts";
	
	/*
	 * uri matchers
	 */
	private static final int HISTORIES = 1;
	private static final int HISTORY_ID = 2;
	private static final UriMatcher uriMatcher;
	static {
		uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		uriMatcher.addURI(PROVIDER_NAME, "history", HISTORIES);
		uriMatcher.addURI(PROVIDER_NAME, "history/#", HISTORY_ID);
	}
	
	/*
	 * DB info
	 */
	private SQLiteDatabase xivoHistoryDB;
	private static final String DATABASE_NAME = "xivo_history";
	private static final String DATABASE_TABLE = "history";
	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_CREATE =
		"create table " + DATABASE_TABLE + " (" +
		_ID + " integer primary key autoincrement, " +
		DURATION + " text not null, " +
		TERMIN + " text not null, " +
		FULLNAME + " text not null, " +
		TS + " text not null, " +
		DIRECTION + " text not null" +
		");";
	
	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		int count = 0;
		switch(uriMatcher.match(uri)) {
		case HISTORIES:
			count = xivoHistoryDB.delete(DATABASE_TABLE, selection, selectionArgs);
			break;
		case HISTORY_ID:
			String id = uri.getLastPathSegment();
			count = xivoHistoryDB.delete(DATABASE_TABLE, _ID + " = " + id +
					(!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : ""),
					selectionArgs);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return count;
	}
	
	@Override
	public String getType(Uri uri) {
		switch(uriMatcher.match(uri)) {
		case HISTORIES:
			return CONTENT_TYPE;
		case HISTORY_ID:
			return CONTENT_ITEM_TYPE;
		default:
			throw new IllegalArgumentException("Unsupported URI: " + uri);
		}
	}
	
	@Override
	public Uri insert(Uri uri, ContentValues values) {
		long rowId = xivoHistoryDB.insert(DATABASE_TABLE, "", values);
		if (rowId > 0) {
			Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowId);
			getContext().getContentResolver().notifyChange(_uri, null);
			return _uri;
		}
		Log.d(TAG, "Failed to insert row: " + uri);
		return null;
	}
	
	@Override
	public boolean onCreate() {
		DBHelper dbHelper = new DBHelper(getContext());
		xivoHistoryDB = dbHelper.getWritableDatabase();
		return xivoHistoryDB != null;
	}
	
	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
			String sortOrder) {
		SQLiteQueryBuilder sqlBuilder = new SQLiteQueryBuilder();
		sqlBuilder.setTables(DATABASE_TABLE);
		
		if (uriMatcher.match(uri) == HISTORY_ID)
			sqlBuilder.appendWhere(_ID + " = " + uri.getLastPathSegment());
		
		Cursor c = sqlBuilder.query(
				xivoHistoryDB, projection, selection, selectionArgs, null, null, sortOrder);
		c.setNotificationUri(getContext().getContentResolver(), uri);
		return c;
	}
	
	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		int count = 0;
		switch (uriMatcher.match(uri)){
		case HISTORIES:
			count = xivoHistoryDB.update(DATABASE_TABLE, values, selection, selectionArgs);
			break;
		case HISTORY_ID:
			count = xivoHistoryDB.update(
					DATABASE_TABLE, values, _ID + " = " + uri.getLastPathSegment() + 
					(!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : ""), 
					selectionArgs);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return count;
	}
	
	private static class DBHelper extends SQLiteOpenHelper {
		
		public DBHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}
		
		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(DATABASE_CREATE);
		}
		
		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
			onCreate(db);
		}
	}
	
	/**
	 * Returns the Cursor pointing to the history items
	 * @param context
	 * @param filter Call filter like in XletHisto: 0=all, 1=incoming, 2=outgoing, 3=missed
	 * @return list
	 */
	public static Cursor getList(Context context, byte filter) {
		String selection = DIRECTION + "=?";
		String[] selectionArgs = new String[1];
		switch(filter) {
		case 0: selection = null; //no selection, return them all
				selectionArgs = null; 
				break;
				
		case 1: selectionArgs[0] = CallType.INBOUND.toString(); break;
		case 2: selectionArgs[0] = CallType.OUTBOUND.toString(); break;
		case 3: selectionArgs[0] = CallType.MISSED.toString(); break;
		}
		
		//return the corresponding cursor, sorted from most recent to oldest
		return context.getContentResolver().query(CONTENT_URI, null, selection, 
					selectionArgs, TS+" DESC");
	}
}
