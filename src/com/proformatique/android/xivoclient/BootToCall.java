package com.proformatique.android.xivoclient;

import java.text.ParseException;

import com.proformatique.android.xivoclient.service.IXivoConnectionService;
import com.proformatique.android.xivoclient.sip.CallingActivityNew;
import com.proformatique.android.xivoclient.sip.SipApiUser;
import com.proformatique.android.xivoclient.tools.Constants;
import com.proformatique.android.xivoclient.xlets.XletDialer;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.sip.SipException;
import android.net.sip.SipManager;
import android.os.Bundle;
import android.os.Looper;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Display;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Toast;

/**
 * This activity is an alternative splash screen, opened when the user is calling a XiVO number from
 * Android's dialpad. It connects the user to CTI and SIP, launches the call, then disconnects him if he was
 * not connected before the call.
 */
public class BootToCall extends ExiterActivity {
	IXivoConnectionService xivoConnectionService;
	private final static String TAG = "Boot to Call";
	
	/**
	 * Changes the splash screen text, using the UI thread.
	 * @param t The new text to display.
	 */
	private void setProgress(final String t) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				((TextView) findViewById(R.id.connectState)).setText(t);
			}
		});
	}
	
	/**
	 * Creates the activity, and launches the Thread handling the operation.
	 */
	@Override
	public void onCreate(Bundle e) {
		super.onCreate(e);
		setContentView(R.layout.splash);
		
		moveTaskToBack = true;
		
		//we don't want any action/title bar on the splash screen
		getSupportActionBar().hide();
		
		//set the logo properly
		ImageView logo = (ImageView) findViewById(R.id.viewLogo);
		LayoutParams p = (LayoutParams) logo.getLayoutParams();
		Display display = getWindowManager().getDefaultDisplay();
		@SuppressWarnings("deprecation") //must use it to keep backwards compatibility
		int width = display.getWidth() - 20;
		p.width = width;
		p.height = width * 412 / 962;
		logo.setLayoutParams(p);
		
		//initialize the thread
		Thread fil = new Thread("Boot then Call") {
			@Override public void run() {
				//we will have to launch a Handler to receive messages from the server.
				//this requires Looper to be prepared.
				Looper.prepare();
				
				Context mContext = getApplicationContext();
				
				  //check out if we are already connected.
				  xivoConnectionService = Connection.INSTANCE.getConnection(mContext);
				  boolean wasConnected = false;
				  try {
					wasConnected = (xivoConnectionService != null
							  && xivoConnectionService.isAuthenticated());
				  } catch (RemoteException e2) {
		          	Log.e(TAG, "Failed to connect!");
		          	return;
				  }

				  
			      if (!wasConnected) {
			    	  //first step: bind to the service
			    	  setProgress(getString(R.string.binding));
			    	  Log.i(TAG, "Binding to the XiVO Connection Service");
			    	  int i = 0;
			          while (xivoConnectionService == null && i < 50) {
			              timer(100);
			              i++;
			              xivoConnectionService = Connection.INSTANCE.getConnection(mContext); //check again
			          }
		              if(xivoConnectionService == null) {
		            	  Log.e(TAG, "Binding failed!");
		            	  return;
		              }
		              
		              //second step: connect to XiVO
					  setProgress(getString(R.string.connection));
		              Log.i(TAG, "Connecting...");
		              try {
		                  if (xivoConnectionService != null && xivoConnectionService.isConnected()) {
		                	  Log.i(TAG, "Already connected!");
		                	  wasConnected = true;
		                  }
		                  if(!wasConnected) {
			                  int res = xivoConnectionService.connect();
			                  if(res != Constants.CONNECTION_OK) {
			                	  Log.e(TAG, "Failed to connect!");
			                	  
			                	  //display a toast
			                	  runOnUiThread(new Runnable() {
										@Override
										public void run() {
											Toast.makeText(BootToCall.this, 
						    			    		  getString(R.string.connection_failed), 
						    			    		  Toast.LENGTH_LONG).show();
										}
								  });
			                	  
			        			  Connection.INSTANCE.releaseService();
			    			      finish();
			    			      moveTaskToBack(true);
			                	  return;
			                  }
		                  }
		              } catch (RemoteException e) {
	              	    Log.e(TAG, "Something bad happened!");
	              	    return;
	                  }
		              
		              	//third step: connect to SIP
		              	SharedPreferences settingsPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		            	
		              	//close the previous SIP user
		        		if(SipApiUser.INSTANCE != null)
		        			SipApiUser.INSTANCE.closeConnection();
		        		
		        		//and open that one (if SIP will be used).
		        		try {
							if(settingsPrefs.getString("sip_server", "").length() != 0 && xivoConnectionService.getUsesSip() &&
									!SipApiUser.badSettings)
								try {
									new SipApiUser(getApplicationContext(), settingsPrefs.getString("sip_server", ""), 
										settingsPrefs.getString("sip_username", ""), settingsPrefs.getString("sip_password", ""));
								} catch (ParseException e) {
									e.printStackTrace();
									SipApiUser.INSTANCE = null;
								} catch (SipException e) {
									e.printStackTrace();
									SipApiUser.INSTANCE = null;
								}
						} catch (RemoteException e2) {
							e2.printStackTrace();
						}

		        	  //fourth step: authenticate
					  setProgress(getString(R.string.authenticating));
		              Log.i(TAG, "Now authenticating...");
		              	try {
							if (xivoConnectionService != null && xivoConnectionService.isAuthenticated()) {
								  Log.i(TAG, "Already authenticated!");
								  wasConnected = true;
							}
						} catch (RemoteException e1) {
		                	  Log.e(TAG, "Something bad happened!");
		                	  return;
		                }
		              	if(!wasConnected) { 
			                int res;
							try {
								res = xivoConnectionService.authenticate();
							} catch (RemoteException e1) {
			                	  Log.e(TAG, "Something bad happened!");
			                	  return;
							}
			                while (res == Constants.ALREADY_AUTHENTICATING) {
			                    synchronized (this) {
			                        try {
			                            wait(100);
			                        } catch (InterruptedException e) {
			                            Log.d(TAG, "Interrupted while waiting for an authentication");
			                            e.printStackTrace();
			                        }
			                    }
			                }
			                if(res != Constants.AUTHENTICATION_OK) {
			                	  Log.e(TAG, "Failed to authenticate!");
			        			  Connection.INSTANCE.releaseService();
			        			  runOnUiThread(new Runnable() {
										@Override
										public void run() {
											Toast.makeText(BootToCall.this, 
						    			    		  getString(R.string.connection_failed), 
						    			    		  Toast.LENGTH_LONG).show();
										}
								  });
			    			      finish();
			    			      moveTaskToBack(true);
			                	  return;
			                  }

							//first, ask it to refresh something. It will force it to 
							//listen to the server, so that we can get the channels
							//(in case we would like to hang up).
							try {
								xivoConnectionService.refreshServices();
							} catch (RemoteException e) {
								e.printStackTrace();
							}
		              	}
			      }
			      
			      Log.i(TAG, "Well, we are ready to call. But how are we going to do that?");
			      
			      if((CallingActivityNew.INSTANCE != null
			    		  && CallingActivityNew.INSTANCE.hasMultipleCalls())) {
				      Log.i(TAG, "Impossible!"); //because we cannot do a triple SIP call.

				      isOK = false;
				      
				      //like everything that's UI-related, run it on the UI thread
				      runOnUiThread(new Runnable() {
				    	  @Override public void run() {
				    		  //show an alert-dialog to notify the user that it is bad!
				    		  AlertDialog.Builder builder = new AlertDialog.Builder(BootToCall.this);
						      builder.setTitle(R.string.sip_error);
							  builder.setMessage(R.string.three_calls);
							  builder.setPositiveButton(R.string.ok, new OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										isOK = true;
									}
								});
							  builder.setOnCancelListener(new OnCancelListener() {
								@Override
								public void onCancel(DialogInterface dialog) {
									isOK = true;
								}
							  });
							  builder.show();
				    	  }
				      });
			    	  
					  //wait until the dialog box is closed
					  while(!isOK) timer(200);
					  //then release the thread.
			      } else //launch the call
			    	  call(getIntent().getStringExtra("numToCall"), BootToCall.this, wasConnected);
			      
			      //ending situation
			      if(!wasConnected) {
			    	  //I was not connected before coming here, so I would not like to stay connected now.
				      setProgress(getString(R.string.disconnecting));
					  Log.i(TAG, "Now disconnecting, we are done");
					  //disconnect CTI
		                if (xivoConnectionService != null) {
							try {
								xivoConnectionService.disconnectRightAway();
							} catch (RemoteException e) {
			                	  Log.e(TAG, "Something bad happened!");
			                	  return;
							}
		                }
		                //disconnect SIP
		                if(SipApiUser.INSTANCE != null)
		                	SipApiUser.INSTANCE.closeConnection();
				  }
			      //prevent some ServiceConnection leaks
				  Connection.INSTANCE.releaseService();
				  
				  //and close this activity.
			      finish();
			      if(moveTaskToBack) moveTaskToBack(true);
			}
		}; fil.start();
	}
	/**
	 * This variable is used to notify the thread that an operation is done.
	 * The thread sets the variable to false, and waits that it becomes true.
	 */
	private boolean isOK = false;
	
	/**
	 * If this is set to true, the whole application will be moved to the background (when the call ends,
	 * it will come back to the contact list/the dialer where the call was launched). 
	 * If this is false, it will only close the activity (when the call ends,
	 * the user will come back to the XiVO application).
	 */
	private boolean moveTaskToBack = true;
	
	/**
	 * Makes a phone call (via SIP or Callback, depending on the situation).
	 * @param phoneNumber the number to call.
	 * @param context the application context
	 * @param wasconnected if the user was connected when the call was launched.
	 */
	private void call(String phoneNumber, final Context context, boolean wasconnected) {
    	try {
			if(xivoConnectionService.getUsesSipNumb(phoneNumber)) {
				//must use SIP!
				Log.i("call() method", "Calling "+phoneNumber+" using SIP!");
					if(SipApiUser.INSTANCE != null) {
						boolean shouldNotWait = CallingActivityNew.INSTANCE != null;
						moveTaskToBack = !shouldNotWait;
						
						//trigger the call
						SipApiUser.INSTANCE.call(phoneNumber, true);
						
						if(!shouldNotWait) {
							//wait until ALL the calls are finished
							while(CallingActivityNew.INSTANCE == null) timer(200); //wait for the beginning
							while(CallingActivityNew.INSTANCE != null) timer(200); //wait for the end
							//then release the thread.
						} else {
							//before launching the call, there was already a SIP call.
							//do not let the splash screen over this call, remove it.
							//so release the thread immediately.
						}
					} else {
						//We MUST use SIP, but we CANNOT use SIP.
						//What is going on?
						final boolean isSipCompat = SipManager.isApiSupported(context) && SipManager.isVoipSupported(context);

						isOK = false;
						
						//like everything that's UI-related, run it on the UI thread
					    runOnUiThread(new Runnable() {
					    	  @Override public void run() {
					    		  	//we are going to show an alert message
									AlertDialog.Builder builder = new AlertDialog.Builder(context);
									builder.setTitle(R.string.sip_error);
									SharedPreferences prefs = 
							        		PreferenceManager.getDefaultSharedPreferences(BootToCall.this);
									if(prefs.getString("HOME_SSID", "").length() == 0)
										//you did not configure the company SSID
										builder.setMessage(R.string.configure_ssid);
									else if(isSipCompat)
										//you did not correctly, or not at all set the SIP settings
											builder.setMessage(SipApiUser.badSettings ?
													R.string.sip_config : R.string.sip_not_set);
									else //your phone is not SIP compatible, so... we are quite blocked now.
										builder.setMessage(R.string.were_doomed);
									
									//show a "OK" button
									builder.setPositiveButton(R.string.ok, new OnClickListener() {
										@Override
										public void onClick(DialogInterface dialog, int which) {
											isOK = true; //when OK is pressed, release the waiting thread.
										}
									});
									builder.setOnCancelListener(new OnCancelListener() {
										@Override
										public void onCancel(DialogInterface dialog) {
											isOK = true; //when the dialog is cancelled, release the waiting thread.
										}
									});
									
									//show the alert dialog
									builder.show();
					    	  }
					      });

						//wait until the dialog box is closed (OK is pressed)
						while(!isOK) timer(200);
						//then release the thread.
					}
			} else {
				//should use the Callback ; pops up the XletDialer that will make the call itself
			    Intent iCall = new Intent(context, XletDialer.class);
			    iCall.putExtra("numToCall", phoneNumber);
			    iCall.putExtra("autoClose", true);
			    iCall.putExtra("wasNotConnected", !wasconnected);
			    context.startActivity(iCall);
			    
			    //wait until the "launching" phase is finished
			    while(!XletDialer.autoPickup) timer(200); //wait for the beginning
			    while(XletDialer.autoPickup) timer(200); //wait for the end
			    //then release the thread.
			}
		} catch (RemoteException e) {
			//some exception thrown for no reason...
			e.printStackTrace();
		} catch (SipException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
    }
	
	/**
	 * Wait for an amount of time
	 * @param i Amount of time in milliseconds.
	 */
	private void timer(int i) {
		try {
			Thread.sleep(i);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Prevent the activity from being closed by pressing Back.
	 */
	@Override
	public void onBackPressed() {
		//do not close the activity!
	}
}
