package com.proformatique.android.xivoclient;

import java.util.Arrays;
import java.util.List;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;

import com.proformatique.android.xivoclient.tools.Constants;
import com.proformatique.android.xivoclient.xlets.XletContactSearch;
import com.proformatique.android.xivoclient.xlets.XletDialer;
import com.proformatique.android.xivoclient.xlets.XletHisto;
import com.proformatique.android.xivoclient.xlets.XletServices;

/**
 * The parent class for all the Activities that have a navigation drawer.
 * This class itself is a {@link XivoActivity}, so it will feature the identity bar.
 */
public class NavigationDrawerActivity extends XivoActivity implements
	NavigationDrawerFragment.NavigationDrawerCallbacks {
	
	private final static String TAG = "Navigation Drawer Activity";

	/**
	 * Fragment managing the behaviors, interactions and presentation of the
	 * navigation drawer.
	 */
	private NavigationDrawerFragment mNavigationDrawerFragment;
	
	/**
	 * Just hide the action bar (the identity bar is already one!)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "onCreate()");
		
		super.onCreate(savedInstanceState);
		
		//hide the action bar (XletIdentity already provides the action bar)
		getSupportActionBar().hide();
	}
	
	/**
	 * Used to set up the navigation drawer after launching setContentView in the Xlets.
	 * @param drawerId id of the NavigationDrawerFragment in the layout
	 * @param layoutId id of the DrawerLayout
	 */
	public void setUpNavigationDrawer(int drawerId, int layoutId) {
		mNavigationDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager()
				.findFragmentById(drawerId);
	
		mDrawerLayout = (DrawerLayout) findViewById(layoutId);
		
		mNavigationDrawerFragment.setUp(drawerId, mDrawerLayout);
	}
	
	/**
	 * Manage the Menu button.
	 */
	@Override
	public boolean onKeyDown(int keycode, KeyEvent e) {
		//react to the "Menu" key... if the user is not in call.
		//if the user is in call, we should not let him access the navigation drawer.
		if(keycode == KeyEvent.KEYCODE_MENU && index != SPECIAL_CONTACT_DURING_CALL 
				&& index != SPECIAL_CONTACT_FROM_DIALER) {
			if(mDrawerLayout.isDrawerOpen(Gravity.LEFT))
				mDrawerLayout.closeDrawer(Gravity.LEFT);
			else
				mDrawerLayout.openDrawer(Gravity.LEFT);
			
			return true;
		}
		return super.onKeyDown(keycode, e);
	}
	
	/**
	 * Show all the navigation drawer options.
	 */
	@Override
    public boolean onPrepareOptionsMenu(Menu m) {
		Log.i(TAG, "Setting up connected/disconnected label");
		//here are the labels of the options
		List<String> listeNoms = Arrays.asList(new String[] {
        		getString(R.string.app_name), //app name
        		getString(R.string.app_name), //separator
        		getString(R.string.dialer_btn_lbl),
        		getString(R.string.userslist_btn_lbl),
        		getString(R.string.history_btn_lbl),
        		getString(R.string.service_btn_lbl),
        		getString(R.string.app_name), //separator
        		getString(R.string.settings), 
        		getString(R.string.about),
        		getString(R.string.connect),
        		getString(R.string.exit)
        });
		//here are the icons corresponding to the options
        List<Integer> listeImages = Arrays.asList(new Integer[] {
        		R.drawable.new_icon, //app name
        		R.drawable.new_icon, //separator
        		R.drawable.drawer_dialer,
        		R.drawable.drawer_contacts,
        		R.drawable.drawer_history,
        		R.drawable.drawer_services,
        		R.drawable.new_icon, //separator
        		R.drawable.drawer_settings,
        		R.drawable.drawer_about,
        		R.drawable.drawer_connect,
        		R.drawable.drawer_exit
        });
        
        //set the navigation drawer list with these two lists
        try {
	        mNavigationDrawerFragment.mDrawerListView
	        	.setAdapter(new PictureAndTextAdapter(this, listeNoms, listeImages));
        } catch(NullPointerException e) {}
        
    	return super.onPrepareOptionsMenu(m); 
    }
	
	/**
	 * React when a navigation drawer item is selected.
	 */
	@Override
	public void onNavigationDrawerItemSelected(int position) {
		Log.d(TAG, "position "+position+" selected!");

		//selecting the section that is currently selected ; so do nothing
		if(position == index+1) return;
		
		Intent b = null;
		boolean quit = false;
		switch(position) {
		case 0: return; //while initializing the activity, onNavigationDrawerItemSelected(0) is called several times
		case 1: return; //click on a separator does not do anything
		case 2: b = new Intent(getApplicationContext(), XletDialer.class); break;
		case 3: b = new Intent(getApplicationContext(), XletContactSearch.class); break;
		case 4: b = new Intent(getApplicationContext(), XletHisto.class); break;
		case 5: b = new Intent(getApplicationContext(), XletServices.class); break;		
		case 6: return; //click on a separator does not do anything
		case 7: b = new Intent(getApplicationContext(), SettingsActivity.class); break;		
		case 8: b = menuAbout(); break;
		case 9: b = null; break; //b = null -> disconnect option (see below)
		case 10: b = null; quit = true; break; //b = null -> disconnect option (see below)
		}
		
		if(b != null) {
			//no OPENING animation when switching tabs
			if(position <= XivoActivity.MAX_INDEX+1)
				b.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			
			//start the new tab or screen
			startActivity(b);
			
			if(position <= XivoActivity.MAX_INDEX+1) {
				//works like tabs: when something is selected, it closes the previous one
				finish();
				
				//no CLOSING animation when switching tabs (so, no animation at all)
				overridePendingTransition(0, 0);
			} else {
				//keep the current index in mind, this will be our index 
				position = index + 1;
			}
		} else {
			//(dis)connecting item
			if(quit) {
				disconnectThenQuit();
			} else {
				//asking to (dis)connect
				menuDisconnect();
			}
		}
	}
	
	/**
	 * Disconnect the service if it is connected, then completely close the app.
	 */
	private void disconnectThenQuit() {
		try {
			if (isXivoServiceRunning() && xivoConnectionService != null
			        && xivoConnectionService.isAuthenticated()) {
				//disconnect, THEN quit (we are currently connected)
				menuDisconnect();
				Thread fil = new Thread() {
					@Override public void run() {
						try {
							while(dialog == null) { Thread.sleep(100); }
							while(dialog != null) { Thread.sleep(100); }
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						XivoActivity.askedToDisconnect = true; //ensure it will not reconnect automatically onResume()
						Intent i = new Intent();
						i.setAction(Constants.ACTION_CLOSE_APP);
						sendBroadcast(i);
					}
				}; fil.start();
			} else {
				//close all the activities right now, as we are not connected.
				XivoActivity.askedToDisconnect = true; //ensure it will not reconnect automatically onResume()
				Intent i = new Intent();
				i.setAction(Constants.ACTION_CLOSE_APP);
				sendBroadcast(i);
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * A press on the "Back" button closes the drawer, or if it is already closed,
	 * it exits the app.
	 */
	@Override
	public void onBackPressed() {
		Log.d(TAG, "Back is pressed");
		if(!mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
			//the navigation drawer is closed. So we should close something.
			//if it is during a call, pressing back closes the activity and goes back to the call screen.
			//if not, it closes the app. (the drawer is already closed)
			Log.d(TAG, "I should close "+(index == SPECIAL_CONTACT_DURING_CALL || index == SPECIAL_CONTACT_FROM_DIALER ?
					"the activity" : "the app"));
			if(index != SPECIAL_CONTACT_DURING_CALL && index != SPECIAL_CONTACT_FROM_DIALER) {
				//display a confirm dialog for closing the app
				new AlertDialog.Builder(this)
					.setMessage(getString(R.string.confirmation_message))
					.setPositiveButton(getString(R.string.confirmation_yes), new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							//the user has chosen "yes", so... quit
							disconnectThenQuit();
						}
					})
					.setNegativeButton(R.string.confirmation_no, null)
					.show();
			} else
				finish();
		} else {
			//the navigation drawer is open. So we should just close it.
			Log.d(TAG, "I should close the drawer");
			mDrawerLayout.closeDrawer(Gravity.LEFT);
		}
	}
	
	/**
	 * Creates an Intent with the "received bytes" extra, to open the "about activity" with this value set correctly.
	 * @return Just run startActivity(menuAbout()) and it will open the AboutActivity
	 */
    private Intent menuAbout() {
    	//if AboutActivity receives -1, it will display "unknown"
    	//so if a problem occurs or xivoConnectionService == null, we will send -1 to it.
        long datareceived = -1L;
        if (xivoConnectionService != null) {
            try {
            	//set the received data.
                datareceived = xivoConnectionService.getReceivedBytes();
            } catch (RemoteException e) {
                datareceived = -1L;
            }
        }
        
        //return the generated intent.
        Intent defineIntent = new Intent(this, AboutActivity.class);
        defineIntent.putExtra("received_data", datareceived);
        return defineIntent;
    }
	
}
