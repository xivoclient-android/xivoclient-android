/* XiVO Client Android
 * Copyright (C) 2010-2011, Proformatique
 *
 * This file is part of XiVO Client Android.
 *
 * XiVO Client Android is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * XiVO Client Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with XiVO client Android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.proformatique.android.xivoclient;

import java.util.Arrays;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

/**
 * The activity displaying the "About" window and the full licenses.
 */
public class AboutActivity extends ExiterActivity {
    
	/**
	 * holds the text of all the menu items listed here.
	 */
    private String[] items;
    
    /**
     * some units, used to display the amount of data received.
     */
    private final static double KB = Math.pow(2, 10), MB = Math.pow(2, 20),
    		GB = Math.pow(2, 30), TB = Math.pow(2, 40);
    
    /**
     * Creates the activity displaying the right screen,
     * depending on the "license" Intent received.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setTitle(getString(R.string.about));
        
        //get the amount of data received
        Intent i = getIntent();
        long received = i.getLongExtra("received_data", -1L);
        setContentView(R.layout.about);
        
        ImageView logo = (ImageView) findViewById(R.id.creditLogo);
        
        //generate the string showing the amount of data received
        StringBuilder sb = new StringBuilder();
        sb.append(getString(R.string.bandwidth_in_label)).append(" ");
        if (received == -1L) {
            sb.append(getString(R.string.unknown));
        } else if (received == 0) {
            sb.append(received).append(" ").append(getString(R.string.unit_byte));
        } else if (received <= KB) {
            sb.append(received).append(" ").append(getString(R.string.unit_byte)).append("s");
        } else if (received <= MB) {
            sb.append(String.format("%.2f ", received / KB)).append(getString(R.string.unit_kb));
        } else if (received <= GB) {
            sb.append(String.format("%.2f ", received / MB)).append(getString(R.string.unit_mb));
        } else if (received <= TB) {
            sb.append(String.format("%.2f ", received / GB)).append(getString(R.string.unit_gb));
        } else {
            sb.append(String.format("%.2f ", received / TB)).append(getString(R.string.unit_tb));
        }
        
        //see if a license or the "About" menu should be displayed.
        switch(getIntent().getIntExtra("license", 0)) {
	        case 0: //"about" menu
		        items = new String[] {
		                getString(R.string.version),
		                getString(R.string.copyright),
		                getString(R.string.credits_part1),
		                getString(R.string.gpl),
		                getString(R.string.about_copyright_pull) + " " + getString(R.string.licensed_apache),
		                sb.toString()
		        };
		        //react when the GNU GPL v3 or Apache v2 options are pressed.
		        ((ListView) findViewById(R.id.aboutList)).
		        	setOnItemClickListener(new OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
							//set here the indexes of GPL and Apache in the list
					        int gpl_index = 3;
					        int apache_index = gpl_index + 1;
					        
					        //launch this activity again to show the full license.
					        if(position == gpl_index) {
					        	Intent iiii = new Intent(AboutActivity.this, AboutActivity.class);
					        	iiii.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					        	iiii.putExtra("license", 1);
					        	AboutActivity.this.startActivity(iiii);
					        }
					        if(position == apache_index) {
					        	Intent iiii = new Intent(AboutActivity.this, AboutActivity.class);
					        	iiii.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					        	iiii.putExtra("license", 2);
					        	AboutActivity.this.startActivity(iiii);
					        }
						}
					});
		        
		        //set up the logo to take up all the screen's width
				LayoutParams p = (LayoutParams) logo.getLayoutParams();
		        Display display = getWindowManager().getDefaultDisplay();
				@SuppressWarnings("deprecation") //must use it to keep backwards compatibility
				int width = display.getWidth() - 20;
				p.width = width;
				p.height = width * 412 / 962;
				logo.setLayoutParams(p);
		        
		        break;
	        case 1: //GNU GPL v3 license
	        	setTitle(getString(R.string.gpl));
	        	items = new String[] {
		                getString(R.string.gpl_part1),
		                getString(R.string.gpl_part2)
		        }; 
	        	logo.setVisibility(View.GONE); //hide the logo
	        	break;
	        case 2: //Apache license
	        	setTitle(getString(R.string.licensed_apache));
	        	items = new String[] { getString(R.string.apache_license) }; 
	        	logo.setVisibility(View.GONE); //hide the logo
	        	break;
        }
        
        //display the items
        ((ListView) findViewById(R.id.aboutList)).
        			setAdapter(new AboutAdapter(Arrays.asList(items)));
        
        //display the "back" arrow in the action bar, to return to the previous screen
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    
    /**
     * Called when the "back" button on the action bar is selected.
     * Finishes this activity, and comes back to the previous one.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem mi) {
    	finish();
    	return true;
    }
    
    /**
     * Used to fill the ListView in the activity with the elements from a List&lt;String&gt;.
     */
    private class AboutAdapter extends BaseAdapter {
        private List<String> list;
        
        public AboutAdapter(List<String> list) {
            this.list = list;
        }
        
        @Override
        public int getCount() {
            return list == null ? 0 : list.size();
        }
        
        @Override
        public Object getItem(int position) {
            return list.get(position);
        }
        
        @Override
        public long getItemId(int position) {
            return position;
        }
        
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v;
            if (convertView == null) { //get a new view from about_row.
                LayoutInflater li = getLayoutInflater();
                v = li.inflate(R.layout.about_row, null);
            } else { //use the provided view.
                v = convertView;
            }
            //set the content...
            TextView text = (TextView) v.findViewById(R.id.about_row_content);
            text.setText(list.get(position));
            //... and return the view.
            return v;
        }
    }
}
