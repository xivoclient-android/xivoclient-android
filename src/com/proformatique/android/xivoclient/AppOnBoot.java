package com.proformatique.android.xivoclient;

import java.text.ParseException;
import java.util.concurrent.TimeoutException;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.sip.SipException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.proformatique.android.xivoclient.dao.CapaservicesProvider;
import com.proformatique.android.xivoclient.service.IXivoConnectionService;
import com.proformatique.android.xivoclient.service.XivoConnectionService;
import com.proformatique.android.xivoclient.sip.SipApiUser;
import com.proformatique.android.xivoclient.sync.ContactSync;
import com.proformatique.android.xivoclient.tools.Constants;
import com.proformatique.android.xivoclient.xlets.XletContactSearch;
import com.proformatique.android.xivoclient.xlets.XletDialer;
import com.proformatique.android.xivoclient.xlets.XletHisto;
import com.proformatique.android.xivoclient.xlets.XletServices;

/**
 * The application starts with this activity being the "splash screen".
 * It tries to connect the user, and if it succeeds or fails, it sends him
 * to his preferred home screen.
 * <p/>
 * The normal startup sequence is:
 * startXivoConnectionService -> bindXivoConnectionService() -> BindingTask -> onBindingComplete()
 * -> ConnectTask -> waitForAuthentication() -> AuthenticationTask.
 * After authentication is complete, this Activity waits to receive {@link Constants}.ACTION_CONTACT_LOAD_COMPLETE,
 * which means that contact loading is complete, and we should go on to the next activity.
 */
public class AppOnBoot extends ExiterActivity {
	private final static String TAG = "Splash Screen";
	
	private SharedPreferences settings;
    protected IXivoConnectionService xivoConnectionService = null;
    protected BindingTask bindingTask = null;
    private ConnectTask connectTask = null;
    private IntentReceiver receiver;
    
    /**
     * The text displayed under the logo, on the splash screen.
     */
    protected TextView connState;
	
    /**
     * Creates the activity and sets the variables.
     */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "The splash screen was created");
		
		//window initialization
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		
		//we don't want any action/title bar on the splash screen
		getSupportActionBar().hide();
		
		//variable initialization
		connState = (TextView) findViewById(R.id.connectState);
        settings = PreferenceManager.getDefaultSharedPreferences(this);
		
		//sets the logo to take all the screen's width
		ImageView logo = (ImageView) findViewById(R.id.viewLogo);
		LayoutParams p = (LayoutParams) logo.getLayoutParams();
		Display display = getWindowManager().getDefaultDisplay();
		@SuppressWarnings("deprecation") //must use it to keep backwards compatibility
		int width = display.getWidth() - 20;
		p.width = width;
		p.height = width * 412 / 962;
		logo.setLayoutParams(p);
	}
	
	/**
	 * Called whenever the activity comes to the foreground.
	 * You should then connect the user and launch another activity.
	 */
	@Override
	public void onResume() {
		Log.d(TAG, "The splash screen was resumed");
		
		super.onResume();
		
		XivoActivity.startInCallScreenKiller(this);
		
		//register the receiver to get the "loading complete" event, and to get the user's fullname.
		Log.i(TAG, "Fullname intent receiver is registered");
        receiver = new IntentReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Constants.ACTION_UPDATE_IDENTITY);
        filter.addAction(Constants.ACTION_CONTACT_LOAD_COMPLETE);
        filter.addAction(Constants.ACTION_NEW_PERCENTAGE);
        registerReceiver(receiver, new IntentFilter(filter));
        
        //if the login/pass is set
        if (!(settings.getString("login", "").equals("")
                || settings.getString("server_adress", "").equals(""))) {
        	//start the XiVO connection service
        	startXivoConnectionService();
        	bindXivoConnectionService();
        	
        	//be sure the contact sync account is set up
            ContactSync.setUpAccount(this);
        } else {
        	//don't even TRY to connect, there are some parameters missing
        	//just launch the default Activity
        	Intent i = getPreferredIntent();
        	if(i != null) startActivity(i);
        	finish();
        }
	}
	
	
	/**
	 * Don't react when the user presses a button
	 */
	@Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
		return true;
	}
	
	/**
     * Binds the Xivo connection service if not done yet
     */
    private void bindXivoConnectionService() {
        xivoConnectionService = Connection.INSTANCE.getConnection(getApplicationContext());
        if (xivoConnectionService == null) {
            bindingTask = new BindingTask();
            bindingTask.execute();
        } else { //the service is already bound
            onBindingComplete();
        }
    }
	
	/**
     * Starts the XiVO connection service.
     */
    private void startXivoConnectionService() {
        Intent iStartXivoService = new Intent();
        iStartXivoService.setClassName(Constants.PACK, XivoConnectionService.class.getName());
        startService(iStartXivoService);
        Log.d(TAG, "Starting XiVO connection service");
    }
	
	
	/**
     * Binds to the service
     */
    protected class BindingTask extends AsyncTask<Void, Void, Integer> {
        
    	/**
    	 * The service is finally bound.
    	 */
        private final static int OK = 0;
        
        /**
         * The binding has timed out.
         */
        private final static int FAIL = -1;
        
        /**
         * Delay between two checks.
         */
        private final static int DELAY = 100;
        
        /**
         * Number of checks before timing out.
         */
        private final static int MAX_WAIT = 50;
        
        //so the timeout is DELAY * MAX_WAIT = 100 * 50 = 5 seconds
        
        @Override
        protected void onPreExecute() {
            Log.d(TAG, "Binding started");
            connState.setText(getString(R.string.binding));
        }
        
        @Override
        protected Integer doInBackground(Void... params) {
            xivoConnectionService = Connection.INSTANCE.getConnection(getApplicationContext());
            int i = 0;
            while (xivoConnectionService == null && i < MAX_WAIT) {
                timer(DELAY);
                i++; 
                //check again
                xivoConnectionService = Connection.INSTANCE.getConnection(getApplicationContext());
            }
            return xivoConnectionService != null ? OK : FAIL;
        }
        
        @Override
        protected void onPostExecute(Integer result) {
            Log.d(TAG, "Binding finished");
            if (result == OK) {
            	//trigger the next step
                onBindingComplete();
            } else {
            	//pop an error
                Toast.makeText(AppOnBoot.this, getString(R.string.binding_error),
                        Toast.LENGTH_SHORT).show();
                //launch the next activity and close this one.
            	Intent i = getPreferredIntent();
            	if(i != null) startActivity(i);
            	finish();
            }
        }
        
        /**
         * Waits an amount of time.
         * @param milliseconds Wait time
         */
        private void timer(long milliseconds) {
            try {
                synchronized (this) {
                    this.wait(milliseconds);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    
    /**
     * Ask to the XivoConnectionService to connect and wait for the result
     */
    private class ConnectTask extends AsyncTask<Void, Void, Integer> {
        @Override
        protected void onPreExecute() {
            connState.setText(getString(R.string.connection));
        }
        
        @Override
        protected Integer doInBackground(Void... params) {
            try {
                if (xivoConnectionService != null && xivoConnectionService.isConnected())
                    return Constants.CONNECTION_OK; //was already connected before asking
                return xivoConnectionService.connect(); //connect now
            } catch (RemoteException e) {
                return Constants.REMOTE_EXCEPTION;
            }
        }
        
        @Override
        protected void onPostExecute(Integer result) {
            switch (result) {
            case Constants.CONNECTION_OK: //move on to the next step.
                try {
                    waitForAuthentication();
                } catch (InterruptedException e) {
                    Log.d(TAG, "Authentication interrupted");
                    e.printStackTrace();
                } catch (TimeoutException e) {
                    Log.d(TAG, "Authentication timedout");
                    e.printStackTrace();
                }
                break;
            //all other cases show up exceptions in a Toast
            case Constants.CONNECTION_REFUSED:
                Toast.makeText(AppOnBoot.this, R.string.connection_refused,
                        Toast.LENGTH_SHORT).show();
                break;
            case Constants.REMOTE_EXCEPTION:
                Toast.makeText(AppOnBoot.this, getString(R.string.remote_exception),
                        Toast.LENGTH_LONG).show();
                break;
            case Constants.NOT_CTI_SERVER:
                Toast.makeText(AppOnBoot.this, getString(R.string.not_cti_server),
                        Toast.LENGTH_LONG).show();
                break;
            case Constants.BAD_HOST:
                Toast.makeText(AppOnBoot.this, getString(R.string.bad_host), Toast.LENGTH_LONG)
                        .show();
                break;
            case Constants.NO_NETWORK_AVAILABLE:
                Toast.makeText(AppOnBoot.this, getString(R.string.no_web_connection),
                        Toast.LENGTH_LONG).show();
                break;
            case Constants.CONNECTION_TIMEDOUT:
                Toast.makeText(AppOnBoot.this, getString(R.string.connection_timedout),
                        Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(AppOnBoot.this, getString(R.string.connection_failed),
                        Toast.LENGTH_LONG).show();
                break;
            }
            if(Constants.CONNECTION_OK != result) {
            	Intent i = getPreferredIntent();
            	if(i != null) startActivity(i);
            	finish();
            }
        }
    }
    
    /**
     * Called when the binding to the service is completed
     */
    protected void onBindingComplete() {
        Log.d(TAG, "onBindingComplete");

        try {
            if (xivoConnectionService != null && xivoConnectionService.isConnected()
                    && xivoConnectionService.isAuthenticated()) {
            	//already all done, go on to the next activity
            	Intent i = getPreferredIntent();
            	if(i != null) startActivity(i);
            	finish();
                return;
            }
        } catch (RemoteException e) {
        	Intent i = getPreferredIntent();
        	if(i != null) startActivity(i);
        	finish();
        }
        //connect to the CTI server
        connectTask = new ConnectTask();
        connectTask.execute();
    }
    
    /**
     * Authenticates the user to SIP and to the CTI server.
     */
    private class AuthenticationTask extends AsyncTask<Void, Void, Integer> {
        @Override
        protected void onPreExecute() {
            connState.setText(getString(R.string.authenticating));
        }
        
        @Override
        protected Integer doInBackground(Void... params) {
            try {
                if (xivoConnectionService != null && xivoConnectionService.isAuthenticated())
                    return Constants.AUTHENTICATION_OK; //already authenticated
                
                int res = xivoConnectionService.authenticate(); //launch the authentication
                while (res == Constants.ALREADY_AUTHENTICATING) {
                    synchronized (this) {
                        try {
                            wait(100);
                        } catch (InterruptedException e) {
                            Log.d(TAG, "Interrupted while waiting for an authentication");
                            e.printStackTrace();
                        }
                        //check if the authentication is done now
                        res = xivoConnectionService.authenticate();
                    }
                }
                return res;
            } catch (RemoteException e) {
                return Constants.REMOTE_EXCEPTION;
            }
        }
        
        @Override
        protected void onPostExecute(Integer result) {
            if (result != Constants.OK && result != Constants.AUTHENTICATION_OK) {
            	//there was a problem, so disconnect cleanly and open the next activity.
                try {
                    xivoConnectionService.disconnect();
                    
                	Intent i = getPreferredIntent();
                	if(i != null) startActivity(i);
                	finish();
                } catch (RemoteException e) {
                    Toast.makeText(AppOnBoot.this, getString(R.string.remote_exception),
                            Toast.LENGTH_SHORT).show();

                	Intent i = getPreferredIntent();
                	if(i != null) startActivity(i);
                	finish();
                }
            }
            switch (result) {
            case Constants.OK:
            case Constants.AUTHENTICATION_OK:
            	//the CTI server is ok. So now, let's connect the SIP server.
                Log.i(TAG, "Authenticated");
                
                //register the SIP Server if not set yet
        		SharedPreferences settingsPrefs = PreferenceManager.getDefaultSharedPreferences(AppOnBoot.this);
        		//close the previous SIP user
        		if(SipApiUser.INSTANCE != null)
        			SipApiUser.INSTANCE.closeConnection();
        		
        		//and open that one.
        		try {
					if(settingsPrefs.getString("sip_server", "").length() != 0 && xivoConnectionService.getUsesSip() &&
							!SipApiUser.badSettings)
						try {
							new SipApiUser(getApplicationContext(), settingsPrefs.getString("sip_server", ""), 
								settingsPrefs.getString("sip_username", ""), settingsPrefs.getString("sip_password", ""));
						} catch (ParseException e) {
							e.printStackTrace();
							SipApiUser.INSTANCE = null;
						} catch (SipException e) {
							e.printStackTrace();
							SipApiUser.INSTANCE = null;
						}
				} catch (RemoteException e2) {
					e2.printStackTrace();
				}
                
        		//launch the contact/history/services loading
            	connState.setText(getString(R.string.loading));
                try {
					if (xivoConnectionService.isAuthenticated()) {
						try {
							xivoConnectionService.loadData();
						} catch (RemoteException e1) {
							e1.printStackTrace();
						}
					}
				} catch (RemoteException e) {
					e.printStackTrace();
				}
                break;
            //al the other cases show toasts.
            case Constants.NO_NETWORK_AVAILABLE:
                Toast.makeText(AppOnBoot.this, getString(R.string.no_web_connection),
                        Toast.LENGTH_SHORT).show();
                break;
            case Constants.JSON_POPULATE_ERROR:
                Toast.makeText(AppOnBoot.this, getString(R.string.login_ko), Toast.LENGTH_LONG)
                        .show();
                break;
            case Constants.FORCED_DISCONNECT:
                Toast.makeText(AppOnBoot.this, getString(R.string.forced_disconnect),
                        Toast.LENGTH_LONG).show();
                break;
            case Constants.LOGIN_PASSWORD_ERROR:
                Toast.makeText(AppOnBoot.this, getString(R.string.bad_login_password),
                        Toast.LENGTH_LONG).show();
                break;
            case Constants.CTI_SERVER_NOT_SUPPORTED:
                Toast.makeText(AppOnBoot.this, getString(R.string.cti_not_supported),
                        Toast.LENGTH_LONG).show();
                break;
            case Constants.VERSION_MISMATCH:
                Toast.makeText(AppOnBoot.this, getString(R.string.version_mismatch),
                        Toast.LENGTH_LONG).show();
                break;
            case Constants.ALGORITH_NOT_AVAILABLE:
                Toast.makeText(AppOnBoot.this, getString(R.string.algo_exception),
                        Toast.LENGTH_LONG).show();
                break;
            case Constants.ALREADY_AUTHENTICATING:
                Log.d(TAG, "Already authenticating");
                break;
            default:
                Log.e(TAG, "Unhandled result " + result);
                Toast.makeText(AppOnBoot.this, getString(R.string.login_ko), Toast.LENGTH_LONG)
                        .show();
                break;
            }
        }
    }
    
    /**
     * Gets the Intent to launch the activity chosen by the user.
     * @return Just do startActivty(this Intent) to launch the home screen chosen by the user.
     */
    private Intent getPreferredIntent() {
    	//about to launch another activity ; unregister the IntentReceiver
		Log.i(TAG, "Fullname intent receiver is unregistered");
    	unregisterReceiver(receiver);
    	
    	Intent i = null;
    	//choose the right Intent, depending on the settings (default is Call log, #3)
    	switch(Integer.parseInt(settings.getString("preferredHome", "3"))) {
        	case 1: i = new Intent(getApplicationContext(), XletDialer.class); break;
        	case 2: i = new Intent(getApplicationContext(), XletContactSearch.class); break;
        	case 3: i = new Intent(getApplicationContext(), XletHisto.class); break;
        	case 4: i = new Intent(getApplicationContext(), XletServices.class); break;
        }
    	return i;
    }
    
    /**
     * Starts an authentication task and wait until it's authenticated
     * 
     * @throws TimeoutException
     * @throws InterruptedException
     */
    private void waitForAuthentication() throws InterruptedException, TimeoutException {
        try {
            if (xivoConnectionService == null || !(xivoConnectionService.isConnected())) {
                Log.d(TAG, "Cannot start authenticating if not connected");
                return;
            }
            if (xivoConnectionService != null && xivoConnectionService.isAuthenticated())
                return; //already authenticated
        } catch (RemoteException e) {
        	//some problem happened, so open the next activity
        	Intent i = getPreferredIntent();
        	if(i != null) startActivity(i);
        	finish();
        }
        //launch the authentication
        AuthenticationTask authenticationTask = new AuthenticationTask();
        authenticationTask.execute();
    }
    
    /**
     * Used to receive user name changes, and load complete events, but also load percentages.
     */
    private class IntentReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
        	if(intent.getAction().equals(Constants.ACTION_CONTACT_LOAD_COMPLETE)) {
        		//load complete event, sent when all the users are loaded.
        		//so you can launch the next Activity
        		Log.i(TAG, "It seems we have to go on.");
        		Intent i = getPreferredIntent();
	            if(i != null) i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	        	if(i != null) startActivity(i);
	        	finish();
        	} else if(intent.getAction().equals(Constants.ACTION_NEW_PERCENTAGE)) {
        		final String s = (intent.getIntExtra("percent", 0) == 0 ? 
        				"" : " ("+intent.getIntExtra("percent", 0) + "%)");
        		runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if(connState != null)
							connState.setText(getString(R.string.loading) + s);
					}
				});
        	} else {
        		//fullname update ; set it in the XiVO connection service
        		//or it will not display in the Xlet Identity later.
	        	Log.i(TAG, "FULLNAME INTENT RECEIVED: "+intent.getStringExtra("fullname"));
	            try {
	            	xivoConnectionService.setFullname(intent.getStringExtra("fullname"));
	                //update the Capaservices database (they are sent together with the fullname)
	                //drop the whole thing
	                getContentResolver().delete(CapaservicesProvider.CONTENT_URI, null, null);
	                //and then recreate it
	                for(String feature : new String[] { "rna", "unc", "busy", "enablednd" }) {
	                	ContentValues newValues = new ContentValues();
	                	newValues.put(CapaservicesProvider.SERVICE, feature);
	                	newValues.put(CapaservicesProvider.ENABLED, 
	                			(intent.getBooleanExtra(feature + "ena", false) ? 1 : 0));
	                	newValues.put(CapaservicesProvider.NUMBER, 
	                			intent.getStringExtra(feature + "nbr"));
	                    getContentResolver().insert(CapaservicesProvider.CONTENT_URI, newValues);
	                	newValues.clear();
	                }

	                //and now, we can send an Intent to notify the things are done
	                Intent i = new Intent();
	                i.setAction(Constants.ACTION_LOAD_FEATURES);
	            } catch (RemoteException e) {
	                e.printStackTrace();
	            }
        	}
        }
    }
}
