package com.proformatique.android.xivoclient;

import com.proformatique.android.xivoclient.service.IXivoConnectionService;
import com.proformatique.android.xivoclient.service.XivoConnectionService;
import com.proformatique.android.xivoclient.tools.Constants;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

/**
 * Implement a Singleton to avoid binding to the service from each activity:
 * binds to the service a single time, then store it in Connection.INSTANCE so that
 * we can use it again without binding to it.
 */
public enum Connection {
    INSTANCE;
    
    private final static String TAG = "XiVO connection";
    private IXivoConnectionService service = null;
    private XivoConnectionServiceConnection con = null;
    private boolean currentlyBinding = false;
    private Context context = null;
    
    /**
     * Establish a binding between the activity and the XivoConnectionService
     */
    private class XivoConnectionServiceConnection implements ServiceConnection {
        
    	/**
    	 * Store the {@link IXivoConnectionService} just received in the service variable.
    	 */
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Connection.this.service = IXivoConnectionService.Stub.asInterface(service);
            Log.d(TAG, "Binding complete");
            currentlyBinding = false;
        }
        
        /**
         * Removes the {@link IXivoConnectionService} as it has just disconnected from the app.
         */
        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d(TAG, "onServiceDisconnected");
            service = null;
        }
    };
    
    /**
     * The first time getConnection is called, it will return a null connection since the
     * binding is done asynchronously. It will return the {@link IXivoConnectionService} when it is finally bound.
     * @param context
     * @return the {@link IXivoConnectionService} or null if it is currently binding.
     */
    public IXivoConnectionService getConnection(Context context) {
        if (this.context == null) this.context = context; //set up the context
        if (service == null && currentlyBinding == false) bind(context); //start a new binding
        return service;
    }
    
    /**
     * Unbinds the service if it is bound currently.
     */
    public void releaseService() {
        try {
            if (con != null && context != null) unbind();
        } catch (IllegalArgumentException e) {
            Log.d(TAG, "Could not release the service: "+e.toString());
        }
    }
    
    /**
     * Launches a new binding.
     * @param context the application context (getApplicationContext() seems better)
     */
    private void bind(Context context) {
        Log.d(TAG, "Starting a new binding");
        currentlyBinding = true;
        con = new XivoConnectionServiceConnection();
        //this Intent identifies the service to connect with: the XiVO connection service.
        Intent iServiceBinder = new Intent();
        iServiceBinder.setClassName(Constants.PACK, XivoConnectionService.class.getName());
        context.bindService(iServiceBinder, con, Context.BIND_AUTO_CREATE);
        /* Log.d(TAG, "XiVO connection service binded");
        currentlyBinding = false; */
    }
    
    /**
     * Unbinds the service.
     */
    private void unbind() {
        if (service != null) {
            Log.d(TAG, "Unbinding");
            context.unbindService(con);
            con = null;
            service = null;
        }
    }
}
