package com.proformatique.android.xivoclient.sync;

import java.util.ArrayList;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentProviderClient;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Context;
import android.content.OperationApplicationException;
import android.content.SyncResult;
import android.database.Cursor;
import android.os.Bundle;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.util.Log;

import com.proformatique.android.xivoclient.dao.UserProvider;

/**
 * Some useful methods for contact sync: one for creating an account, the other to write the contacts to
 * the address book.
 */
public class ContactSync {
	private ContentResolver mContentResolver;
	private Context mContext;

	/**
	 * Constructor to set the context for the method onPerformSync.
	 * @param context the context of the app.
	 */
	public ContactSync(Context context) {
		mContentResolver = context.getContentResolver();
		mContext = context;
	}
	
	/**
	 * Adds the "XiVO Contacts Integration" option to the options menu, with a 2-hour sync interval.
	 * @param context the app context
	 */
	public static void setUpAccount(Context context) {
		Account c = ContactAccountService.GetAccount();
		
		AccountManager accountManager = 
				(AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);
        if(accountManager.addAccountExplicitly(c, null, null)) {
        	ContentResolver.setIsSyncable(c, ContactsContract.AUTHORITY, 1);
        	ContentResolver.addPeriodicSync(c, ContactsContract.AUTHORITY, new Bundle(), 60000 * 120);
        	//ContentResolver.setSyncAutomatically(c, ContactsContract.AUTHORITY, true);
        }
	}
	

	/**
	 * Sync the contacts present in the {@link UserProvider} with those of {@link ContactsContract}.
	 * The arguments should be given by the "real" onPerformSync call.
	 * @param account
	 * @param extras
	 * @param authority
	 * @param provider
	 * @param syncResult
	 */
	public void onPerformSync(Account account, Bundle extras, String authority,
			ContentProviderClient provider, SyncResult syncResult) {
		Log.i("Contact Sync", "Performing sync");
		
		//Here, sync XiVO's UserProvider database with the RawContacts database.
		Cursor c = mContentResolver.query(UserProvider.CONTENT_URI, 
				new String[] { UserProvider._ID	}, null, null, null);
		//don't refresh the contacts if they all have been vanished from the User Provider.
		if(!c.moveToFirst()) {
			c.close();
			return;
		}
		c.close();
		
		//Before all, we got to delete all the contacts created by ourselves
		//(and, hopefully, not the others)
		c = mContentResolver.query(ContactsContract.RawContacts.CONTENT_URI,
				new String[] { ContactsContract.RawContacts._ID },
				ContactsContract.RawContacts.ACCOUNT_NAME + "=?  AND "
			+   ContactsContract.RawContacts.ACCOUNT_TYPE + "=?", 
				new String[] { account.name, account.type }, null);
		
		//going to register names and phones already present in the list
		//so that they aren't added again
		ArrayList<String> names = new ArrayList<String>();
		ArrayList<String> phones = new ArrayList<String>();
		
		//deletion phase
		if(c.moveToFirst()) {
			ArrayList<ContentProviderOperation> operations = new ArrayList<ContentProviderOperation>();
		
			do {
				long id = c.getLong(c.getColumnIndex(ContactsContract.RawContacts._ID));

				//get the name and the phone of the contact
				String name = "";
				String phone = "";
				Cursor infos = mContentResolver.query(ContactsContract.Data.CONTENT_URI, 
						new String[] { ContactsContract.Data.MIMETYPE,
						ContactsContract.Data.DATA1 }, ContactsContract.Data.RAW_CONTACT_ID+"='"+id+"'", 
						null, null);
				if(infos.moveToFirst()) {
					do {
						try {
							if(infos.getString(infos.getColumnIndex(ContactsContract.Data.MIMETYPE))
									.equals( ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE))
								phone = infos.getString(infos.getColumnIndex(ContactsContract.Data.DATA1));
							if(infos.getString(infos.getColumnIndex(ContactsContract.Data.MIMETYPE))
									.equals( ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE))
								name = infos.getString(infos.getColumnIndex(ContactsContract.Data.DATA1));
						} catch(NullPointerException e) {}
					} while(infos.moveToNext());
				}
				infos.close();
				
				//check if the contact is in the app's contact list
				infos = mContentResolver.query(UserProvider.CONTENT_URI, 
						new String[] { UserProvider.FULLNAME, UserProvider.PHONENUM }, 
						UserProvider.FULLNAME+"=? AND "+UserProvider.PHONENUM+"=?", 
						new String[] { name, phone }, null);
				
				//if it is the case, just skip it.
				if(infos.moveToFirst()) {
					infos.close();
					names.add(name);
					phones.add(phone);
					Log.i("Contact Sync", "Contact "+name+"(id "+id+") was kept.");
					continue;
				}
				infos.close();
				
				if(name.equals("")) continue;
				
				//remove all the lines from the contact
				ContentProviderOperation.Builder op = ContentProviderOperation.newDelete(
						ContactsContract.Data.CONTENT_URI);
				op.withSelection(
						ContactsContract.Data.RAW_CONTACT_ID+"='"+id+"'", null);
				operations.add(op.build());
				
				//then remove the contact itself
				op = ContentProviderOperation.newDelete(
						ContactsContract.RawContacts.CONTENT_URI);
				op.withSelection(
						ContactsContract.RawContacts._ID+"='"+id+"'", null);
				op.withYieldAllowed(true);
				operations.add(op.build());
				
				Log.i("Contact Sync", "Contact "+name+"(id "+id+") was removed.");
			} while(c.moveToNext());
			
			try {
				mContext.getContentResolver().applyBatch(ContactsContract.AUTHORITY, operations);
			} catch (RemoteException e) {
				e.printStackTrace();
			} catch (OperationApplicationException e) {
				e.printStackTrace();
			}
		}
		c.close();
		
		//now, the writing part begins!
		c = mContentResolver.query(UserProvider.CONTENT_URI, 
				new String[] { UserProvider.FULLNAME, UserProvider.PHONENUM }, null, null, null);
		if(c.moveToFirst()) {
			do {
				String name = c.getString(c.getColumnIndex(UserProvider.FULLNAME));
				String number = c.getString(c.getColumnIndex(UserProvider.PHONENUM));
				String first_name, last_name;
				if(name.contains(" ")) {
					first_name = name.substring(0, name.indexOf(" "));
					last_name = name.substring(name.indexOf(" ")+1, name.length());
				} else {
					first_name = "";
					last_name = name;
				}
				
				boolean isAlreadyRegistered = false;
				for(int i = 0; i < phones.size(); i++) {
					if(name.equals(names.get(i)) && number.equals(phones.get(i)))
						isAlreadyRegistered = true;
				}
				if(isAlreadyRegistered) continue;	
				
				//I thought this database had mobile numbers... but no. Well, let's pass null.
				addContact(account, first_name, last_name, name, number, null);
				Log.i("Contact Sync", "Contact "+name+" was added.");
			} while(c.moveToNext());
		}
		c.close();
	}
	
	/**
	 * Adds a contact to the {@link ContactsContract} database
	 * @param a The account used to add the contact
	 * @param first The first name of the contact
	 * @param last The last name of the contact
	 * @param name The full name of the contact
	 * @param number The internal number of the contact
	 * @param mobile The mobile number of the contact, or null if the mobile number was not set.
	 */
	private void addContact(Account a, String first, String last, String name, String number, String mobile) {
		ArrayList<ContentProviderOperation> operations = new ArrayList<ContentProviderOperation>();
		
		if(number.equals("....")) return;
		
		//set the contact
		ContentProviderOperation.Builder builder = 
				ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
				.withValue(ContactsContract.RawContacts.ACCOUNT_NAME, a.name)
				.withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, a.type);
		operations.add(builder.build());
		
		//set the first name (GIVEN_NAME column...)
		builder = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
				.withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
				.withValue(ContactsContract.Data.MIMETYPE, 
						ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
				.withValue(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME, first)
				.withValue(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME, last)
				.withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, name);
		operations.add(builder.build());
		
		//set the internal number
		builder = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
				.withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
				.withValue(ContactsContract.Data.MIMETYPE, 
						ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
				.withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, number)
				.withValue(ContactsContract.CommonDataKinds.Phone.TYPE,
						ContactsContract.CommonDataKinds.Phone.TYPE_WORK)
				.withYieldAllowed(true);
		operations.add(builder.build());
		
		//set the mobile number
		if(mobile != null) {
			builder = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
					.withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
					.withValue(ContactsContract.Data.MIMETYPE, 
							ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
					.withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, mobile)
					.withValue(ContactsContract.CommonDataKinds.Phone.TYPE,
							ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE);
			operations.add(builder.build());
		}
		
		try {
			mContext.getContentResolver().applyBatch(ContactsContract.AUTHORITY, operations);
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (OperationApplicationException e) {
			e.printStackTrace();
		}
	}

	
}
