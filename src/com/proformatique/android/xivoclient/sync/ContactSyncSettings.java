package com.proformatique.android.xivoclient.sync;

import com.proformatique.android.xivoclient.ExiterActivity;
import com.proformatique.android.xivoclient.R;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

/**
 * Simple activity explaining how the sync works to the user.
 * Can be launched from the app settings.
 */
public class ContactSyncSettings extends ExiterActivity {

	/**
	 * Just creates the view and attach the event to the button.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sync_expl);
		setTitle(getString(R.string.sync_settings1));
		
		findViewById(R.id.open_phone_settings).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(android.provider.Settings.ACTION_SYNC_SETTINGS);
				i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(i);
			}
		});
		
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	}
	
	/**
	 * React when the action bar "back" icon is pressed
	 */
	 @Override
	    public boolean onOptionsItemSelected(MenuItem mi) {
	    	finish();
	    	return true;
	    }
}
