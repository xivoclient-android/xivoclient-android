package com.proformatique.android.xivoclient.sync;


import android.accounts.Account;
import android.annotation.TargetApi;
import android.app.Service;
import android.content.AbstractThreadedSyncAdapter;
import android.content.BroadcastReceiver;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SyncResult;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import com.proformatique.android.xivoclient.Connection;
import com.proformatique.android.xivoclient.service.IXivoConnectionService;
import com.proformatique.android.xivoclient.tools.Constants;

/**
 * Service used to trigger the contact sync.
 */
public class ContactSyncService extends Service {
	 private static final String TAG = "ContactsSyncAdapterService";
	 private static SyncAdapterImpl sSyncAdapter = null;
	 
	 /**
	  * The sync adapter used to sync the contacts periodically.
	  */
	 private static class SyncAdapterImpl extends AbstractThreadedSyncAdapter {
		 private Context mContext;
		 private IXivoConnectionService xivoConnectionService;
		 private boolean isLoadingComplete = false;
		 
		 /**
		  * Creates the sync adapter and stores the context.
		  * @param c the context
		  * @param a
		  */
		 public SyncAdapterImpl(Context c, boolean a) {
			 super(c, a);
			 mContext = c;
		 }
		 
		 /**
		  * Creates the sync adapter and stores the context.
		  * @param c the context
		  * @param a
		  * @param p
		  */
		 @TargetApi(Build.VERSION_CODES.HONEYCOMB)
		 public SyncAdapterImpl(Context c, boolean a, boolean p) {
			 super(c, a, p);
			 mContext = c;
		 }
		 
		 /**
		  * Stores the last sync time. Each sync must be separated by 2 minutes.
		  * When the user enables sync, the system will launch it twice in a row ; block the second time.
		  */
		 long lastSyncMillis = 0;
   		 boolean timeout = false;
	 
   		 /**
   		  * Sync the contacts.
   		  */
		 @Override
		 public void onPerformSync(Account account, Bundle extras, String authority, 
			  ContentProviderClient provider, SyncResult syncResult) {
			  Log.i(TAG, "performSync: " + account.toString());
			  //"Each sync must be separated by 2 minutes."
			  if(System.currentTimeMillis() - lastSyncMillis < 120000) {
				  Log.e(TAG, "Sync request refused, too close to the previous");
				  return;
			  }
			  lastSyncMillis = System.currentTimeMillis();
			  
			  //get if the Connection Service is connected
			  xivoConnectionService = Connection.INSTANCE.getConnection(mContext.getApplicationContext());
			  boolean wasConnected = false;
				try {
					wasConnected = (xivoConnectionService != null
							  && xivoConnectionService.isAuthenticated());
				} catch (RemoteException e2) {
	          	  Log.e(TAG, "Failed to connect!");
	          	  return;
				}
		      if (!wasConnected) {
		    	  //it is not, step one: bind to it
		    	  Log.i(TAG, "Binding to the XiVO Connection Service");
		    	  int i = 0;
		          while (xivoConnectionService == null && i < 50) {
		              timer(100);
		              i++;
		              xivoConnectionService = Connection.INSTANCE.getConnection(mContext.getApplicationContext());
		          }
	              if(xivoConnectionService == null) {
	            	  Log.e(TAG, "Binding failed!");
	            	  return;
	              }
	              //step 2: connect
	              Log.i(TAG, "Connecting...");
	              try {
	                  if (xivoConnectionService != null && xivoConnectionService.isConnected()) {
	                	  Log.i(TAG, "Already connected!");
	                	  wasConnected = true;
	                  }
	                  if(!wasConnected) {
		                  int res = xivoConnectionService.connect();
		                  if(res != Constants.CONNECTION_OK) {
		                	  Log.e(TAG, "Failed to connect!");
		        			  Connection.INSTANCE.releaseService();
		                	  return;
		                  }
	                  }
	              } catch (RemoteException e) {
                	  Log.e(TAG, "Something bad happened!");
                	  return;
                  }
	              //step 3: authenticate
	              Log.i(TAG, "Now authenticating...");
	              	try {
						if (xivoConnectionService != null && xivoConnectionService.isAuthenticated()) {
							  Log.i(TAG, "Already authenticated!");
							  wasConnected = true;
						}
					} catch (RemoteException e1) {
	                	  Log.e(TAG, "Something bad happened!");
	                	  return;
	                }
	              	if(!wasConnected) { 
		                int res = Constants.REMOTE_EXCEPTION;
						try {
							res = xivoConnectionService.authenticate();
						} catch (RemoteException e1) {
							e1.printStackTrace();
						}
						
		                while (res == Constants.ALREADY_AUTHENTICATING) {
		                    synchronized (this) {
		                        try {
		                            wait(100);
		                        } catch (InterruptedException e) {
		                            Log.d(TAG, "Interrupted while waiting for an authentication");
		                            e.printStackTrace();
		                        }
		                    }
		                }
		                if(res != Constants.AUTHENTICATION_OK) {
		                	  Log.e(TAG, "Failed to authenticate!");
		        			  Connection.INSTANCE.releaseService();
		                	  return;
		                  }
	              	}
	              	//step 4: load date
	              	if(!wasConnected) {
	              		timeout = false;
	              		
		              	Log.i(TAG, "Downloading the contacts and all stuff...");
		              	//this thread is here to trigger a timeout if the sync takes more than 5 minutes (!!!)
		              	//if it is the case, it will set the "timeout" boolean to true.
		              	Thread th = new Thread() {
		              		@Override public void run() {
		              			try {
									Thread.sleep(5*60000);
									Log.e(TAG, "Sync takes too much time! Connection may have been lost.");
			              			Connection.INSTANCE.releaseService();
			              			timeout = true;
								} catch (InterruptedException e) {
									Log.d(TAG, "Sync is alright");
								}
		              		}
		              	}; th.start();
		              	
		              	try {
							xivoConnectionService.loadData();
							IntentFilter filter = new IntentFilter();
							//workaround : don't hang eternally  if there is only ONE
							//contact left (happens randomly, often when the app was killed before)
					        filter.addAction(Constants.ACTION_CONTACT_LOAD_ONE_LEFT);
					        isLoadingComplete = false;
					        BroadcastReceiver br = new BroadcastReceiver() {
								@Override
								public void onReceive(Context context, Intent intent) {
									Log.i(TAG, "Received ONE LEFT...");
									timer(5000);
									Log.i(TAG, "Now, it's time!");
									isLoadingComplete = true;
								}
							};
					        mContext.registerReceiver(br, new IntentFilter(filter));
					        //also register the real receiver when there is 0 contact left
							filter = new IntentFilter();
							filter.addAction(Constants.ACTION_CONTACT_LOAD_COMPLETE);
					        BroadcastReceiver br2 = new BroadcastReceiver() {
								@Override
								public void onReceive(Context context, Intent intent) {
									Log.i(TAG, "Received COMPLETE");
									isLoadingComplete = true;
								}
							};
					        mContext.registerReceiver(br2, new IntentFilter(filter));
					        //now wait for a timeout or for a complete loading
					        while(!isLoadingComplete && !timeout) timer(100);
					        mContext.unregisterReceiver(br);
					        mContext.unregisterReceiver(br2);
						} catch (RemoteException e) {
		                	  Log.e(TAG, "Something bad happened!");
		                	  return;
						}
		              	
		              	//break the thread and return if a timeout happens (= connection lost during the sync?)
		              	if(th.isAlive()) th.interrupt();
		              	if(timeout) return;
		              	
		              	Log.i(TAG, "Connection is FINALLY complete!");
	              	}
		      }
			  
			  //sync the contacts right here!
			  new ContactSync(mContext).onPerformSync(account, extras, authority, provider, syncResult);
			  
			  if(!wasConnected) {
				  //we were not connected, so disonnect now.
				  Log.i(TAG, "Now disconnecting, we are done");
	                if (xivoConnectionService != null) {
						try {
							xivoConnectionService.disconnectRightAway();
						} catch (RemoteException e) {
							e.printStackTrace();
						}
	                }
			  }
			  
			  //prevent service leaks
			  Connection.INSTANCE.releaseService();
		 }

		 /**
		  * Wait for an amount of time
		  * @param i the amount in milliseconds
		  */
		private void timer(int i) {
			try {
				Thread.sleep(i);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	 }
	 
	 /**
	  * Creates the service and the sync adapter instance.
	  */
	 @Override
	 public void onCreate() {
		 super.onCreate();
		 if (sSyncAdapter == null)
			  sSyncAdapter = new SyncAdapterImpl(this, true);
	 }
	 
	 /**
	  * Returns the sync adapter binder.
	  */
	 @Override
	 public IBinder onBind(Intent intent) {
		 IBinder ret = getSyncAdapter().getSyncAdapterBinder();
		 return ret;
	 }
	 
	 /**
	  * Returns the sync adapter.
	  * @return
	  */
	 private SyncAdapterImpl getSyncAdapter() {		  
		  return sSyncAdapter;
	 }
}
