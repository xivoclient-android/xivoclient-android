package com.proformatique.android.xivoclient;

import com.proformatique.android.xivoclient.sip.SipApiUser;
import com.proformatique.android.xivoclient.tools.Constants;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.view.Display;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;

/**
 * Another "splash screen" activity that just disconnects the user.
 * Opened when the user clicks the Disconnect button in the notification.
 */
public class DisconnectNow extends XivoActivity {
	
	/**
	 * Creates the activity and set up the visual aspect
	 */
	@Override 
	protected void onCreate(Bundle b) {
		//by default, XivoActivity tries to connect in onResume()
		//this special index will prevent it from trying this
		index = SPECIAL_DISCONNECT;
		super.onCreate(b);
		setContentView(R.layout.splash);
		getSupportActionBar().hide();
		
		//set the logo properly
		ImageView logo = (ImageView) findViewById(R.id.viewLogo);
		LayoutParams p = (LayoutParams) logo.getLayoutParams();
		Display display = getWindowManager().getDefaultDisplay();
		@SuppressWarnings("deprecation") //must use it to keep backwards compatibility
		int width = display.getWidth() - 20;
		p.width = width;
		p.height = width * 412 / 962;
		logo.setLayoutParams(p);
		
		//set the "Disconnecting..." text
		((TextView) findViewById(R.id.connectState)).setText(R.string.disconnecting);
	}
	
	/**
	 * Launches the disconnecting thread
	 */
	@Override
	protected void onResume() {
		super.onResume();
		Thread fil = new Thread("Disconnect from Notification") { 
			@Override public void run() {
				try {
					//disconnect from SIP
					if(SipApiUser.INSTANCE != null) 
						SipApiUser.INSTANCE.closeConnection();
					
					Log.i("Disconnect Now", "XiVO Connection service is --> "+
								xivoConnectionService);
					
					if (isXivoServiceRunning() && xivoConnectionService != null
					        && xivoConnectionService.isAuthenticated()) {
						//disconnect from CTI too
						try {
							xivoConnectionService.disconnectRightAway();
						} catch (RemoteException e) {
							e.printStackTrace();
						}
					} else {
						//we are already disconnected, but for some reason, the notification is always here
						//(for example, the user killed the app) so just remove it!
						NotificationManager notifManager = (NotificationManager)
								getSystemService(Context.NOTIFICATION_SERVICE);
						notifManager.cancel("XIVO",Constants.XIVO_NOTIF);
					}
					
					//send the Intent causing all activities to close (see ExiterActivity)
					XivoActivity.askedToDisconnect = true; //ensure it will not reconnect automatically onResume()
					Intent i = new Intent();
					i.setAction(Constants.ACTION_CLOSE_APP);
					sendBroadcast(i);
				} catch (RemoteException e) {
					e.printStackTrace();
				} //just quit
			}
		}; fil.start();
	}
}
