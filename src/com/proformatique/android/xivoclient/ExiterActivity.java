package com.proformatique.android.xivoclient;

import com.proformatique.android.xivoclient.tools.Constants;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;

/**
 * Superclass of every single Activity, enabling to close everything by sending a single Intent.
 * Closing the activities with finish() is cleaner than using System.exit(0) or something like this.
 */
public class ExiterActivity extends ActionBarActivity {
	/**
	 * When this Intent is received, the activity will be closed.
	 */
	private class IntentReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			Log.d("Exiter Activity", "I shall close!");
			finish();
		}
		
	}
	
	private IntentReceiver receiver;
	
	/**
	 * Register the closing receiver.
	 */
	@Override
	protected void onCreate(Bundle b) {
		super.onCreate(b);
		receiver = new IntentReceiver();
		IntentFilter f = new IntentFilter();
		f.addAction(Constants.ACTION_CLOSE_APP);
		registerReceiver(receiver, f);
	}
	
	/**
	 * Unregister the closing receiver.
	 */
	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(receiver);
	}
}
