/* XiVO Client Android
 * Copyright (C) 2010-2011, Proformatique
 *
 * This file is part of XiVO Client Android.
 *
 * XiVO Client Android is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * XiVO Client Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with XiVO client Android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.proformatique.android.xivoclient;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.sip.SipException;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.proformatique.android.xivoclient.dao.CapapresenceProvider;
import com.proformatique.android.xivoclient.dao.CapaservicesProvider;
import com.proformatique.android.xivoclient.dao.UserProvider;
import com.proformatique.android.xivoclient.service.IXivoConnectionService;
import com.proformatique.android.xivoclient.service.XivoConnectionService;
import com.proformatique.android.xivoclient.sip.SipApiUser;
import com.proformatique.android.xivoclient.tools.Constants;
import com.proformatique.android.xivoclient.tools.GraphicsManager;

/**
 * An overloaded Activity class to make UI changes and options consistent across
 * the application (such as keeping the XiVO connection service bound and
 * setting up the identity toolbar at the top of the screen)
 * 
 * @author Pascal Cadotte-Michaud
 * 
 */
public class XivoActivity extends ExiterActivity implements OnClickListener {
	/**
	 * Should be set on the number of Xlets currently present.
	 * (See the xlets/Index List.txt file)
	 * <p/>
	 * Changing this will move the limit between "big" and "small" options, the small options not having the
	 * navigation drawer but a "back" button instead.
	 */
	public static final int MAX_INDEX = 4;
	/**
	 * This Xlet's index.
	 * Prevents re-opening the Xlet when re-selecting it in the navigation drawer, and allows comparisons with MAX_INDEX.
	 */
	protected int index;
    
    private final static String TAG = "XivoActivity";
    protected static boolean askedToDisconnect = false;
    private static boolean wrongLoginInfo = false;
    
    //handler-related
    private Handler mHandler = new Handler();
    /**
     * The refresh interval of the user's fullname.
     */
    private final static long UPDATE_DELAY = 5000L;

    //service-related
    protected BindingTask bindingTask = null;
    protected IXivoConnectionService xivoConnectionService = null;
    private ConnectTask connectTask = null;
    private DisconnectTask disconnectTask = null;
    private AuthenticationTask authenticationTask = null;
    private IntentReceiver receiver = null;
    
    //settings
    private SharedPreferences settings;
    
    //UI-related
    protected ProgressDialog dialog;
    private TextView longname;
	protected DrawerLayout mDrawerLayout;
    
    /**
     * Called when the activity is created.
     * Makes sure the settings are here.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        settings = PreferenceManager.getDefaultSharedPreferences(this);
        
        //if some settings are missing, open the settings now.
        if (settings.getString("login", "").equals("")
                || settings.getString("server_adress", "").equals("")) {
            startActivity(new Intent(this, SettingsActivity.class));
        }
        //if fullscreen has been selected, apply it!
        if (settings.getBoolean("use_fullscreen", false)) {
            this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        //register all the receivers.
        receiver = new IntentReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Constants.ACTION_MY_STATUS_CHANGE);
        filter.addAction(Constants.ACTION_MY_PHONE_CHANGE);
        filter.addAction(Constants.ACTION_UPDATE_IDENTITY);
        filter.addAction(Constants.ACTION_UPDATE_SERVICES);
        filter.addAction(Constants.ACTION_SETTINGS_CHANGE);
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        filter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        registerReceiver(receiver, new IntentFilter(filter));
    }
    
    /**
     * Called when the activity is resumed.
     * Makes sure we are always connected to the service.
     */
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        mHandler.removeCallbacks(mUpdateIdentity);
        mHandler.postDelayed(mUpdateIdentity, UPDATE_DELAY);
        if (!(settings.getString("login", "").equals("")
                || settings.getString("server_adress", "").equals(""))) {
        	//if there are good settings, we should try to bind to the service.
            if(index != SPECIAL_DISCONNECT) startXivoConnectionService();
            bindXivoConnectionService();
        }
    }
    
    /**
     * Called when the activity is destroyed.
     * Unregisters all the receivers and makes some cleanup.
     */
    @Override
    protected void onDestroy() {
        unregisterReceiver(receiver);
        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
        if (mHandler != null) {
            mHandler.removeCallbacks(mUpdateIdentity);
            mHandler = null;
        }
        super.onDestroy();
    }
    
    /**
     * Called when the binding to the service is completed
     */
    protected void onBindingComplete() {
    	if(index == SPECIAL_DISCONNECT) return;
    	
        Log.d(TAG, "onBindingComplete");
        try {
        	//update the infos displayed in the identity toolbar.
            updateMyStatus(xivoConnectionService.getStateId());
            updatePhoneStatus(xivoConnectionService.getPhoneStatusColor(), xivoConnectionService
                    .getPhoneStatusLongname());
            displayFullname();
        } catch (RemoteException e) {
            Log.d(TAG, "Could not set my state id");
        }
        
        if (!askedToDisconnect && !wrongLoginInfo) {
        	//if there are credentials and it is not proved that they are wrong, try to connect with them.
            try {
                waitForConnection();
            } catch (InterruptedException e) {
                Log.d(TAG, "Connection interrupted");
                e.printStackTrace();
            } catch (TimeoutException e) {
                Log.d(TAG, "Connection timedout");
                e.printStackTrace();
            }
        } else {
            setUiEnabled(true);
        }
    }
    
    /**
     * Gets the current user's phone ID.
     * @return
     */
    protected long getPhoneStatusId() {
        if (xivoConnectionService == null) return 0L;
        Cursor c = null;
        try {
            String astid = xivoConnectionService.getAstId();
            String xivoId = xivoConnectionService.getXivoId();
            c = getContentResolver().query(
                    UserProvider.CONTENT_URI,
                    new String[] {
                        UserProvider._ID,
                        UserProvider.ASTID,
                        UserProvider.XIVO_USERID},
                        UserProvider.ASTID + " = '" + astid + "' AND '" + xivoId + "' = " +
                            UserProvider.XIVO_USERID , null, null);
            if (c.getCount() > 0) {
                c.moveToFirst();
                long ret = c.getLong(c.getColumnIndex(UserProvider._ID));
                c.close();
                return ret;
            }
        } catch (RemoteException e) {
            Log.d(TAG, "Failed to retrieve my hintstatus");
        }
        if(c != null && !c.isClosed()) c.close();
        return 0L;
    }
    
    /**
     * Displays "not connected" in English if state = false...
     * @param state
     */
    protected void setUiEnabled(boolean state) {
        if (state == false) {
            Toast.makeText(this, "Not connected", Toast.LENGTH_SHORT).show();
        }
    }
    
    /**
	 * Adapter used to provide the dropdown status items, and to allow the user to select a new status.
	 */
	private class AlternativeAdapter extends SimpleAdapter implements SpinnerAdapter {
		
		List<? extends Map<String, ?>> data;
		
		public AlternativeAdapter(Context context,
				List<? extends Map<String, ?>> data, int resource, String[] from,
				int[] to) {
			super(context, data, resource, from, to);
			this.data = data;
		}
		
		@Override
		public int getCount() {
			return data.size();
		}
		
		@Override
		public View getDropDownView(int position, View convertView, ViewGroup parent) {
			
			View view = super.getView(position, convertView, parent);
			@SuppressWarnings("unchecked")
			HashMap<String, String> line = (HashMap<String, String>) data.get(position);
			
			ImageView icon = (ImageView) view.findViewById(R.id.identity_state_image);
			String stateIdColor = line.get("color");
			
			GraphicsManager.setIconStateDisplay(XivoActivity.this, icon, stateIdColor);
			return view;
		}
	}
	
	/**
	 * Perform a state change
	 * 
	 * @param stateId, longName, color
	 */
	public void clickLine(String stateId, String longName, String color){
		
		try {
			xivoConnectionService.setState(stateId);
		} catch (RemoteException e) {
			Toast.makeText(this, getString(R.string.remote_exception), Toast.LENGTH_SHORT).show();
		}
	}
	
	
    
    /**
     * Register the button to unfold the drawer, and sets up the status Spinner.
     */
    protected void registerButtons() {
        if(index > MAX_INDEX) {
        	//there is no drawer here, so don't display the drawer icon
        	((ImageView) findViewById(R.id.iconeDrawer)).setImageResource(R.drawable.abc_ic_ab_back_holo_dark);
        	findViewById(R.id.statusContact).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					finish();
				}
			});
        } else {
        	//unfold the drawer if the uses clicks statusContact
        	findViewById(R.id.statusContact).setOnClickListener(this);
        }
        longname = (TextView) findViewById(R.id.user_identity);
        
        final Spinner spi = (Spinner) findViewById(R.id.statusSpinner);
        
        List<HashMap<String, String>> identityStateList = CapapresenceProvider.getStateList(this);
		Log.d(TAG, identityStateList.toString());
		AlternativeAdapter stateAdapter = new AlternativeAdapter(
				this, identityStateList, R.layout.xlet_identity_state_items,
				new String[] {"longname", "stateid", "color"},
				new int[] {R.id.identity_state_longname,
						R.id.identity_stateid, R.id.identity_color});
		
		spi.setAdapter(stateAdapter);
		
		spi.setOnItemSelectedListener(new OnItemSelectedListener() {
			boolean firstTime = true;
			
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				if(firstTime) {
					//don't react the first time, when the spinner was just created
					firstTime = false;
					return;
				}
				@SuppressWarnings("unchecked")
				HashMap<String, String> line = (HashMap<String, String>) spi.getItemAtPosition(arg2);
				clickLine(line.get("stateid"), line.get("longname"), line.get("color"));
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// Nothing to do here... but have to implement this.
			}
		});
    }
    
    /**
     * Called when a "open drawer" button is clicked.
     */
    @Override
    public void onClick(View v) {
        mDrawerLayout.openDrawer(GravityCompat.START);
    }   
    
    /**
     * Checks if the service is running
     * @return true if it is running
     */
    public boolean isXivoServiceRunning() {
        ActivityManager am = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> rs = am.getRunningServices(100);
        for (int i = 0; i < rs.size(); i++) {
            ActivityManager.RunningServiceInfo info = rs.get(i);
            if (info.service.getClassName().equals(XivoConnectionService.class.getName())) {
                Log.d(TAG, "Running == true");
                return true;
            }
        }
        Log.d(TAG, "Running == false");
        return false;
    }
    
    /**
     * Reacts to a click on the "(dis)connect" option in the navigation drawer.
     */
    protected void menuDisconnect() {
        try {
            if (isXivoServiceRunning() && xivoConnectionService != null
                    && xivoConnectionService.isAuthenticated()) {
            	//disconnect the user
                askedToDisconnect = true;
                stopInCallScreenKiller(this);
                stopXivoConnectionService();
            } else {
            	//connect the user.
                askedToDisconnect = false;
                startXivoConnectionService();
                bindXivoConnectionService();
                startInCallScreenKiller(this);
            }
        } catch (RemoteException e) {
        }
    }
    
    /**
     * Disconnects the service from the CTI server.
     */
    protected void disconnect() {
        if (xivoConnectionService != null) {
            try {
                xivoConnectionService.disconnect();
            } catch (RemoteException e) {
                Toast.makeText(this, getString(R.string.remote_exception), Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }
    
    /**
     * Starts the XiVO connection service.
     */
    private void startXivoConnectionService() {
        Intent iStartXivoService = new Intent();
        iStartXivoService.setClassName(Constants.PACK, XivoConnectionService.class.getName());
        startService(iStartXivoService);
        Log.d(TAG, "Starting XiVO connection service");
    }
    
    /**
     * Stops the XivoConnectionService
     */
    protected void stopXivoConnectionService() {
        disconnectTask = new DisconnectTask();
        disconnectTask.execute();
        Log.d(TAG, "Stopping XiVO connection service");
    }
    
    /**
     * Starts a connection task, if not already connected.
     * 
     * @throws TimeoutException
     * @throws InterruptedException
     */
    private void waitForConnection() throws InterruptedException, TimeoutException {
        try {
            if (xivoConnectionService != null && xivoConnectionService.isConnected()
                    && xivoConnectionService.isAuthenticated())
                return;
        } catch (RemoteException e) {
            dieOnBindFail();
        }
        connectTask = new ConnectTask();
        connectTask.execute();
    }
    
    /**
     * Starts an authentication task, if not already authenticated.
     * 
     * @throws TimeoutException
     * @throws InterruptedException
     */
    private void waitForAuthentication() throws InterruptedException, TimeoutException {
        try {
            if (xivoConnectionService == null || !(xivoConnectionService.isConnected())) {
                Log.d(TAG, "Cannot start authenticating if not connected");
                return;
            }
            if (xivoConnectionService != null && xivoConnectionService.isAuthenticated())
                return;
        } catch (RemoteException e) {
            dieOnBindFail();
        }
        authenticationTask = new AuthenticationTask();
        authenticationTask.execute();
    }
    
    /**
     * Starts loading the user list/call log/services, if authenticated/
     */
    private void startLoading() {
        try {
            if (xivoConnectionService.isAuthenticated()) {
                xivoConnectionService.loadData();
            }
        } catch (RemoteException e) {
            dieOnBindFail();
        }
    }
    
    /**
     * Retrieves our status from the DB and update the header
     * 
     * @param id The current user's ID
     */
    private void updateMyStatus(long id) {
        Log.d(TAG, "updateMyStatus " + id);
        Cursor c = getContentResolver().query(
                CapapresenceProvider.CONTENT_URI,
                new String[] { CapapresenceProvider._ID, CapapresenceProvider.LONGNAME,
                        CapapresenceProvider.COLOR }, CapapresenceProvider._ID + " = " + id, null,
                null);
        if (c.getCount() != 0) {
            c.moveToFirst();
            if(findViewById(R.id.identity_current_state_longname) != null) {
	            ((TextView) findViewById(R.id.identity_current_state_longname)).setText(c.getString(c
	                    .getColumnIndex(CapapresenceProvider.LONGNAME)));
	            GraphicsManager.setIconStateDisplay(this,
	                    (ImageView) findViewById(R.id.identity_current_state_image), c.getString(c
	                            .getColumnIndex(CapapresenceProvider.COLOR)));
            }
        }
        c.close();
    }
    
    /**
     * Sets up the phone status displayed in the Xlet Identity.
     * @param color The status color
     * @param longname The status name
     */
    private void updatePhoneStatus(String color, String longname) {
    	if(findViewById(R.id.identityPhoneLongnameState) != null) {
	        ((TextView) findViewById(R.id.identityPhoneLongnameState)).setText(longname);
	        GraphicsManager.setIconPhoneDisplay(this,
	                (ImageView) findViewById(R.id.identityPhoneStatus), color);
    	}
    }
    
    /**
     * Update the fullname of the identity bar
     */
    private void displayFullname() {
        String name = null;
        try {
            if (xivoConnectionService == null) {
            	//display "User"
                name = getString(R.string.user_identity);
            } else if (xivoConnectionService.isAuthenticated()) {
            	//display "Hello World"
                name = xivoConnectionService.getFullname();
            } else {
            	//display "Hello World (not connected)"
                name = xivoConnectionService.getFullname() + " "
                    + getString(R.string.disconnected);
            }
        } catch (RemoteException e) { }
        if (longname != null) longname.setText(name);
    }
    
    /**
     * Binds the XivoConnection service, if not bound yet. When finished, will call onBindingComplete().
     */
    private void bindXivoConnectionService() {
        xivoConnectionService = Connection.INSTANCE.getConnection(getApplicationContext());
        if (xivoConnectionService == null) {
            bindingTask = new BindingTask();
            bindingTask.execute();
        } else {
            onBindingComplete();
        }
    }
    
    /**
     * Binds to the service. When finished, will call onBindingComplete().
     */
    protected class BindingTask extends AsyncTask<Void, Void, Integer> {
        
        private final static int OK = 0;
        private final static int FAIL = -1;
        private final static int DELAY = 100;
        private final static int MAX_WAIT = 50;
        
        @Override
        protected void onPreExecute() {
            Log.d(TAG, "Binding started");
            if (dialog == null)
                dialog = new ProgressDialog(XivoActivity.this);
            dialog.setCancelable(false);
            dialog.setMessage(getString(R.string.binding));
            dialog.show();
        }
        
        @Override
        protected Integer doInBackground(Void... params) {
            xivoConnectionService = Connection.INSTANCE.getConnection(getApplicationContext());
            int i = 0;
            while (xivoConnectionService == null && i < MAX_WAIT) {
                timer(DELAY);
                i++;
                xivoConnectionService = Connection.INSTANCE.getConnection(getApplicationContext());
            }
            return xivoConnectionService != null ? OK : FAIL;
        }
        
        @Override
        protected void onPostExecute(Integer result) {
            Log.d(TAG, "Binding finished");
            if (dialog != null) {
                dialog.dismiss();
                dialog = null;
            }
            if (result == OK) {
                onBindingComplete();
            } else {
                Toast.makeText(XivoActivity.this, getString(R.string.binding_error),
                        Toast.LENGTH_SHORT).show();
            }
        }
        
        private void timer(long milliseconds) {
            try {
                synchronized (this) {
                    this.wait(milliseconds);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    
    /**
     * Kills the app and display a message when the binding to the service
     * cannot be established ___This should NOT happen___
     */
    private void dieOnBindFail() {
        Toast.makeText(this, getString(R.string.binding_error), Toast.LENGTH_LONG).show();
        Log.e(TAG, "Failed to bind to the service");
        finish();
    }
    
    /**
     * Authenticates the user, and on success, starts to load data and connect to SIP.
     */
    private class AuthenticationTask extends AsyncTask<Void, Void, Integer> {
        
        @Override
        protected void onPreExecute() {
            if (dialog == null)
                dialog = new ProgressDialog(XivoActivity.this);
            dialog.setCancelable(false);
            dialog.setMessage(getString(R.string.authenticating));
            dialog.show();
        }
        
        @Override
        protected Integer doInBackground(Void... params) {
            try {
                if (xivoConnectionService != null && xivoConnectionService.isAuthenticated())
                    return Constants.AUTHENTICATION_OK;
                int res = xivoConnectionService.authenticate();
                while (res == Constants.ALREADY_AUTHENTICATING) {
                    synchronized (this) {
                        try {
                            wait(100);
                        } catch (InterruptedException e) {
                            Log.d(TAG, "Interrupted while waiting for an authentication");
                            e.printStackTrace();
                        }
                    }
                    //check if the authentication is done now
                    res = xivoConnectionService.authenticate();
                }
                return res;
            } catch (RemoteException e) {
                return Constants.REMOTE_EXCEPTION;
            }
        }
        
        @Override
        protected void onPostExecute(Integer result) {
            if (dialog != null) {
                dialog.dismiss();
                dialog = null;
            }
            if (result != Constants.OK && result != Constants.AUTHENTICATION_OK) {
                try {
                    xivoConnectionService.disconnect();
                } catch (RemoteException e) {
                    Toast.makeText(XivoActivity.this, getString(R.string.remote_exception),
                            Toast.LENGTH_SHORT).show();
                }
            }
            switch (result) {
            case Constants.OK:
            case Constants.AUTHENTICATION_OK:
                Log.i(TAG, "Authenticated");
                setUiEnabled(true);
                startLoading();
                
                //register the SIP Server if not set yet
                try {
					if(SipApiUser.INSTANCE == null && xivoConnectionService.getUsesSip()
							&& settings.getString("sip_server", "").length() != 0) {
					    try {
							new SipApiUser(getApplicationContext(), settings.getString("sip_server", ""), 
									settings.getString("sip_username", ""), settings.getString("sip_password", ""));
						} catch (ParseException e) {
							e.printStackTrace();
							SipApiUser.INSTANCE = null;
						} catch (SipException e) {
							e.printStackTrace();
							SipApiUser.INSTANCE = null;
						}
					}
				} catch (RemoteException e) {
					e.printStackTrace();
				}
                break;
            case Constants.NO_NETWORK_AVAILABLE:
                Toast.makeText(XivoActivity.this, getString(R.string.no_web_connection),
                        Toast.LENGTH_SHORT).show();
                break;
            case Constants.JSON_POPULATE_ERROR:
                Toast.makeText(XivoActivity.this, getString(R.string.login_ko), Toast.LENGTH_LONG)
                        .show();
                break;
            case Constants.FORCED_DISCONNECT:
                Toast.makeText(XivoActivity.this, getString(R.string.forced_disconnect),
                        Toast.LENGTH_LONG).show();
                break;
            case Constants.LOGIN_PASSWORD_ERROR:
                Toast.makeText(XivoActivity.this, getString(R.string.bad_login_password),
                        Toast.LENGTH_LONG).show();
                wrongLoginInfo = true;
                break;
            case Constants.CTI_SERVER_NOT_SUPPORTED:
                Toast.makeText(XivoActivity.this, getString(R.string.cti_not_supported),
                        Toast.LENGTH_LONG).show();
                wrongLoginInfo = true;
                break;
            case Constants.VERSION_MISMATCH:
                Toast.makeText(XivoActivity.this, getString(R.string.version_mismatch),
                        Toast.LENGTH_LONG).show();
                wrongLoginInfo = true;
                break;
            case Constants.ALGORITH_NOT_AVAILABLE:
                Toast.makeText(XivoActivity.this, getString(R.string.algo_exception),
                        Toast.LENGTH_LONG).show();
                break;
            case Constants.ALREADY_AUTHENTICATING:
                Log.d(TAG, "Already authenticating");
                break;
            default:
                Log.e(TAG, "Unhandled result " + result);
                Toast.makeText(XivoActivity.this, getString(R.string.login_ko), Toast.LENGTH_LONG)
                        .show();
                break;
            }
        }
    }
    
    /**
     * Ask to the XivoConnectionService to connect and wait for the result.
     * Will call waitForAuthentication() on success.
     */
    private class ConnectTask extends AsyncTask<Void, Void, Integer> {
        
        @Override
        protected void onPreExecute() {
            if (dialog == null)
                dialog = new ProgressDialog(XivoActivity.this);
            dialog.setCancelable(false);
            dialog.setMessage(getString(R.string.connection));
            dialog.show();
        }
        
        @Override
        protected Integer doInBackground(Void... params) {
            try {
                if (xivoConnectionService != null && xivoConnectionService.isConnected())
                    return Constants.CONNECTION_OK;
                return xivoConnectionService.connect();
            } catch (RemoteException e) {
                return Constants.REMOTE_EXCEPTION;
            }
        }
        
        @Override
        protected void onPostExecute(Integer result) {
            if (dialog != null) {
                dialog.dismiss();
                dialog = null;
            }
            switch (result) {
            case Constants.CONNECTION_OK:
                try {
                    waitForAuthentication();
                } catch (InterruptedException e) {
                    Log.d(TAG, "Authentication interrupted");
                    e.printStackTrace();
                } catch (TimeoutException e) {
                    Log.d(TAG, "Authentication timedout");
                    e.printStackTrace();
                }
                break;
            case Constants.CONNECTION_REFUSED:
                Toast.makeText(XivoActivity.this, R.string.connection_refused,
                        Toast.LENGTH_SHORT).show();
                break;
            case Constants.REMOTE_EXCEPTION:
                Toast.makeText(XivoActivity.this, getString(R.string.remote_exception),
                        Toast.LENGTH_LONG).show();
                break;
            case Constants.NOT_CTI_SERVER:
                Toast.makeText(XivoActivity.this, getString(R.string.not_cti_server),
                        Toast.LENGTH_LONG).show();
                wrongLoginInfo = true;
                break;
            case Constants.BAD_HOST:
                Toast.makeText(XivoActivity.this, getString(R.string.bad_host), Toast.LENGTH_LONG)
                        .show();
                wrongLoginInfo = true;
                break;
            case Constants.NO_NETWORK_AVAILABLE:
                Toast.makeText(XivoActivity.this, getString(R.string.no_web_connection),
                        Toast.LENGTH_LONG).show();
                break;
            case Constants.CONNECTION_TIMEDOUT:
                Toast.makeText(XivoActivity.this, getString(R.string.connection_timedout),
                        Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(XivoActivity.this, getString(R.string.connection_failed),
                        Toast.LENGTH_LONG).show();
                break;
            }
        }
    }
    
    /**
     * Disconnect the XiVO connection service from the server.
     */
    private class DisconnectTask extends AsyncTask<Void, Void, Integer> {
        
        @Override
        protected void onPreExecute() {
            if (dialog == null)
                dialog = new ProgressDialog(XivoActivity.this);
            dialog.setCancelable(false);
            dialog.setMessage(getString(R.string.disconnecting));
            dialog.show();
        }
        
        @Override
        protected Integer doInBackground(Void... params) {
            try {
            	//disconnect from SIP
                if(SipApiUser.INSTANCE != null) SipApiUser.INSTANCE.closeConnection();
                //and from CTI
                if (xivoConnectionService != null) xivoConnectionService.disconnect();
            } catch (RemoteException e) {
                Log.d(TAG, "Could not contact the xivo connection service");
            }
            return 0;
        }
        
        @Override
        protected void onPostExecute(Integer result) {
            Log.d(TAG, "Disconnected");
            if (dialog != null) {
                dialog.dismiss();
                dialog = null;
            }
        }
    }
    /**
     * This window has been opened during a SIP call: "Back" closes it, there is no navigation drawer and there is a
     * "Back" button on the identity toolbar instead.
     */
    protected static final int SPECIAL_CONTACT_DURING_CALL = 2712;
    /**
     * This window has been opened to disconnect the user: do not try to reconnect him
     */
    protected static final int SPECIAL_DISCONNECT = 100195;
    /**
     * This window is the "contact search" Xlet opened from the "Dialer" Xlet: picking a contact writes it
     * back to the dialer, "Back" closes it, there is no navigation drawer and there is a
     * "Back" button on the identity toolbar instead.
     */
    protected static final int SPECIAL_CONTACT_FROM_DIALER = 4012;
    
    /**
     * Stores a new fullname in the XiVO connection service and display it in the identity toolbar.
     * @param fullname The new fullname
     */
    private void updateFullname(String fullname) {
        try {
            xivoConnectionService.setFullname(fullname);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        displayFullname();
    }

    /**
     * Receives this user's updates, service updates, and network/settings events.
     */
    private class IntentReceiver extends BroadcastReceiver {
        
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
        	Log.v(TAG, action);
            if (action.equals(Constants.ACTION_MY_STATUS_CHANGE)) {
                updateMyStatus(intent.getLongExtra("id", 0));
            } else if (action.equals(Constants.ACTION_MY_PHONE_CHANGE)) {
                updatePhoneStatus(intent.getStringExtra("color"),
                        intent.getStringExtra("longname"));
            } else if (action.equals(Constants.ACTION_UPDATE_IDENTITY)) {
                updateFullname(intent.getStringExtra("fullname"));
                //update the Capaservices database
                //drop the whole thing
                getContentResolver().delete(CapaservicesProvider.CONTENT_URI, null, null);
                //and then recreate it
                for(String feature : new String[] { "rna", "unc", "busy", "enablednd" }) {
                	ContentValues newValues = new ContentValues();
                	newValues.put(CapaservicesProvider.SERVICE, feature);
                	newValues.put(CapaservicesProvider.ENABLED, 
                			(intent.getBooleanExtra(feature + "ena", false) ? 1 : 0));
                	newValues.put(CapaservicesProvider.NUMBER, 
                			intent.getStringExtra(feature + "nbr"));
                    getContentResolver().insert(CapaservicesProvider.CONTENT_URI, newValues);
                	newValues.clear();
                }

                //and now, we can send an Intent to notify the things are done
                Intent i = new Intent();
                i.setAction(Constants.ACTION_LOAD_FEATURES);
                sendBroadcast(i);
            } else if (action.equals(Constants.ACTION_UPDATE_SERVICES)) {
                //Update the Capaservices database
                for(String feature : new String[] { "rna", "unc", "busy", "enablednd" }) {
                	Log.v(TAG, "For feature "+feature+": "+intent.getStringExtra(feature + "nbr"));
                	if(intent.getStringExtra(feature + "nbr") == null
                			&& !feature.equals("enablednd")) continue;
                	ContentValues newValues = new ContentValues();
                	newValues.put(CapaservicesProvider.SERVICE, feature);
                	newValues.put(CapaservicesProvider.ENABLED, 
                			(intent.getBooleanExtra(feature + "ena", false) ? 1 : 0));
                	newValues.put(CapaservicesProvider.NUMBER, 
                			intent.getStringExtra(feature + "nbr"));
                	getContentResolver().update(CapaservicesProvider.CONTENT_URI, newValues,
                            CapaservicesProvider.SERVICE + " = '" + feature + "'", null);
                	newValues.clear();
                	break;
                }

                //and now, we can send an Intent to notify the things are done
                Intent i = new Intent();
                i.setAction(Constants.ACTION_LOAD_FEATURES);
                sendBroadcast(i);
            } else if (action.equals(Constants.ACTION_SETTINGS_CHANGE)
                    || action.equals(ConnectivityManager.CONNECTIVITY_ACTION)
                    || action.equals(WifiManager.WIFI_STATE_CHANGED_ACTION)) {
                wrongLoginInfo = false;
            }
        }
    }
    
    /**
     * Runnable to update the displayed fullname every 5 seconds.
     */
    private Runnable mUpdateIdentity = new Runnable() {
        @Override
        public void run() {
            displayFullname();
            mHandler.postDelayed(mUpdateIdentity, UPDATE_DELAY);
        }
    };
    
    /**
     * Starts the InCallScreenKiller. /!\ this has been disabled.
     */
    public static void startInCallScreenKiller(Context context) {
    	/*
        Intent inCallScreenKillerIntent = new Intent();
        inCallScreenKillerIntent.setClassName(Constants.PACK, InCallScreenKiller.class.getName());
        context.startService(inCallScreenKillerIntent);
        Log.d(TAG, "InCallScreenKiller started");
        */
    }
    
    /**
     * Stops the InCallScreenKiller. /!\ this has been disabled.
     */
    public static void stopInCallScreenKiller(Context context) {
    	/*
        Intent inCallScreenKillerIntent = new Intent();
        inCallScreenKillerIntent.setClassName(Constants.PACK, InCallScreenKiller.class.getName());
        context.stopService(inCallScreenKillerIntent);
        Log.d(TAG, "InCallScreenKilled stopped");
        */
    }

    /**
     * Gets the {@link IXivoConnectionService}
     * @return
     */
	public IXivoConnectionService getXivoConnectionService() {
		return xivoConnectionService;
	}
}
