package com.proformatique.android.xivoclient.sip;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.media.ToneGenerator;
import android.net.Uri;
import android.net.sip.SipAudioCall;
import android.net.sip.SipException;
import android.net.sip.SipProfile;
import android.os.Bundle;
import android.os.Vibrator;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar.Tab;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.proformatique.android.xivoclient.R;
import com.proformatique.android.xivoclient.dao.UserProvider;
import com.proformatique.android.xivoclient.tools.Constants;

/**
 * A {@link Fragment} holding a single {@link SipAudioCall}.
 */
public class CallingFragment extends Fragment {
	
	private TextView time;
	private boolean isCalling; //the Call Chronometer will stop when this boolean becomes false.
	private int minutes, seconds;
	private BroadcastReceiver br;
	private Context theFragContext;
	protected Tab actionBarsTab;
	private View v;
	private boolean alreadyRinging = false;
	private Ringtone ringtone;
	protected SipAudioCall currentCall;
	private AlertDialog dialog;
	
	/**
	 * When the users transfers a call, this is set to 15 and is decremented each second.
	 * All the buttons disappear. When this reaches 5, all the buttons will appear again and 
	 * an error message will display. (timeout is ten seconds after the DTMF messages)
	 * When this reaches 0, the call chronometer will be displayed again.
	 * <p>
	 * So, only if this is equal to 0, the call chronometer will be displayed.
	 * Else, it will be discounted, but not displayed.
	 * <p>
	 * You can watch the LogCat in Verbose level and with tag "Call Chronometer" while trying to transfer
	 * a conference to see this variable in action.
	 * 
	 */
	private byte secondDiscount;
	
	/**
	 * The call chronometer, updates the time spent since the beginning of the call.
	 * we got to have only ONE Call Chronometer at once, to avoid a mess!
	 * when it is null, it means that it is not running yet.
	*/
	private Thread callChronometer;
	
	/**
	 * Called when the fragment is created, at the very beginning of the call.
	 */
	public CallingFragment(int id) {
		
		//should init the variables here, the view will be created several times!
		//so read them now, and don't use them anymore later.
		currentCall = SipApiUser.INSTANCE.currentCall;
		alreadyRinging = SipApiUser.INSTANCE.isAlreadyRinging;
		
		secondDiscount = 0;
	}
	
	/**
	 * Called when the fragment is destroyed (after the call).
	 */
	@Override public void onDestroy() {
		super.onDestroy();
		
		if(br != null) try {
			theFragContext.unregisterReceiver(br);
		//this exception is thrown in a very particular situation: when the user hangs up after calling himself.
		//in this case, br does not get registered
		} catch(RuntimeException re) {}
	}
	
	private boolean isInCall;
	protected View mute, speaker, holdcall, HangupButton, HangupButtonText, show_dialer, accept, reject, 
		imageView2, dialpad, TransferButton, TransferButtonText;
	private Vibrator vibrator;
	
	/**
	 * Called each time the fragment is created.
	 * @param inflater
	 * @param container
	 * @param savedInstanceState
	 * @return
	 */
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		Log.i("Calling Fragment", "Inflating call");
		isInCall = getArguments().getBoolean("isInCall", false);
		
		//first, inflate the view.
		v = inflater.inflate(R.layout.call_screen, null, false);
		
		//then, backup all the views contained in it
		theFragContext = v.getContext();
		mute = v.findViewById(R.id.mute);
		speaker = v.findViewById(R.id.HP);
		holdcall = v.findViewById(R.id.holdcall);
		HangupButton = v.findViewById(R.id.HangupButton);
		HangupButtonText = v.findViewById(R.id.HangupButtonText);
		TransferButton = v.findViewById(R.id.TransferButton);
		TransferButtonText = v.findViewById(R.id.TransferButtonText);
		show_dialer = v.findViewById(R.id.show_dialer);
		accept = v.findViewById(R.id.accept);
		reject = v.findViewById(R.id.reject);
		imageView2 = v.findViewById(R.id.userNameOrder);
		dialpad = v.findViewById(R.id.dialpad);
		
		//preparing the services
		AudioManager am = (AudioManager)theFragContext.getSystemService(Context.AUDIO_SERVICE);
		vibrator = (Vibrator) theFragContext.getSystemService(Context.VIBRATOR_SERVICE);
		
		if(getArguments().getBoolean("incoming", false) && !currentCall.isInCall()) {
			//it is an incoming call! Hide the mute/hangup/speaker icons
			(mute).setVisibility(View.INVISIBLE);
			(speaker).setVisibility(View.INVISIBLE);
			(holdcall).setVisibility(View.INVISIBLE);
			(HangupButton).setVisibility(View.INVISIBLE);
			(HangupButtonText).setVisibility(View.INVISIBLE);
			(TransferButton).setVisibility(View.INVISIBLE);
			(TransferButtonText).setVisibility(View.INVISIBLE);
			(show_dialer).setVisibility(View.INVISIBLE);
			//and show the accept/reject button instead
			(accept).setVisibility(View.VISIBLE);
			(reject).setVisibility(View.VISIBLE);
			
			//and don't forget to ring the bell
			//IF the user is not in a call
			if(!isInCall) {
				//and IF the user is in ring mode
				if(am.getRingerMode() == AudioManager.RINGER_MODE_NORMAL) {
					Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
					if(ringtone != null && ringtone.isPlaying()) ringtone.stop();
					//notification may be null if no ringtone is set 
					if(notification != null) {
						ringtone = RingtoneManager.getRingtone(theFragContext, notification);
						ringtone.setStreamType(AudioManager.STREAM_RING); //sets the same volume as the phone's ringtone
						ringtone.play();
					}
				}
				//and let's vibrate too.
				if(am.getRingerMode() != AudioManager.RINGER_MODE_SILENT) {
					//on 500ms, off 500ms, then repeat until I want you to stop.
					vibrator.vibrate(new long[] { 500, 500 }, 0);
				}
			} else {
				//if the user is in call, vibrate anyway, but play no sound.
				vibrator.vibrate(new long[] { 500, 500 }, 0);
			}
		}
		
		//when the user accepts an incoming call
		(accept).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//hold all the other calls
				CallingActivityNew.INSTANCE.switchOnCall(CallingFragment.this);
				
				//must accept the call
				try {
					currentCall.answerCall(30);
				} catch (SipException e) {
					e.printStackTrace();
				}
				currentCall.startAudio();
				currentCall.setSpeakerMode(false);
	            if(currentCall.isMuted()) {
	            	currentCall.toggleMute();
	            }

	            //and take all the icons back
				(mute).setVisibility(View.VISIBLE);
				(speaker).setVisibility(View.VISIBLE);
				(holdcall).setVisibility(View.VISIBLE);
				(show_dialer).setVisibility(View.VISIBLE);
				(HangupButton).setVisibility(View.VISIBLE);
				(HangupButtonText).setVisibility(View.VISIBLE);
				(TransferButton).setVisibility(View.VISIBLE);
				(TransferButtonText).setVisibility(View.VISIBLE);
				(accept).setVisibility(View.INVISIBLE);
				(reject).setVisibility(View.INVISIBLE);
				
				//set the right color for the transfer button
				TransferButton.setBackgroundColor(Color.parseColor("#0050AB"));
				
				if(ringtone != null) ringtone.stop(); //stop the ringtone
				vibrator.cancel(); //AND stop vibrating
				
				//and finish by suspending the other calls
				CallingActivityNew.INSTANCE.switchOnCall(CallingFragment.this);
			}
		});
		
		//when the user rejects an incoming call
		(reject).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//call rejected ; just quit the activity closing the call
				try {
					currentCall.endCall();
				} catch (SipException e) {
					e.printStackTrace();
				}
				finish();
				
				if(ringtone != null) ringtone.stop(); //stop the ringtone
				vibrator.cancel(); //AND stop vibrating
			}
		});
		
		//receiver to get all the events broadcasted by the SipAudioCall.Listeners.
		br = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, final Intent intent) {
				//just react if OUR call is involved.
				if(!currentCall.equals(SipApiUser.INSTANCE.currentCall)) return;
				
				if(intent.getAction().equals(Constants.ACTION_SIP_RINGING)) {
					//displays "Ringing..."
					setTime(context.getString(R.string.calling_screen));
					alreadyRinging = true;
				}
				if(intent.getAction().equals(Constants.ACTION_SIP_ESTABLISHED)) {
					//set the right color for the transfer button
					TransferButton.setBackgroundColor(Color.parseColor("#0050AB"));
					
					if(!isCalling) {
						//start call chronometer
						if(callChronometer == null) {
							callChronometer = newChrono();
							callChronometer.start();
							
							//and by the time, switch to this call
							//(handles the crazy case where the user calls a second person
							// before the first has answered the call yet!)
							//if the two ones are enabled at the same time, it will be a mess.
							CallingActivityNew.INSTANCE.switchOnCall(CallingFragment.this);
							
							//set the right color for the transfer button
							TransferButton.setBackgroundColor(Color.parseColor("#0050AB"));
						}
					}
				} 
				if(intent.getAction().equals(Constants.ACTION_SIP_HUNG_UP)) {
					//stop the ring and the vibrator, if the call is missed
					if(ringtone != null) ringtone.stop(); //stop the ringtone
					vibrator.cancel(); //AND stop vibrating
					
					if(dialog != null) dialog.cancel();
					
					//set the right color for the transfer button
					TransferButton.setBackgroundColor(Color.parseColor("#666666"));
					
					//this thread is here to make the time blink before closing the activity
					Thread fil = new Thread("Activity Finisher on Hang Up") {
						@Override public void run() {
							isCalling = false;
							String text = time.getText().toString();
							String seek = theFragContext.getString(R.string.transfering);
							if(text.contains(seek.substring(0, seek.indexOf("%s")))) {
								text = (getString(R.string.transfer_success));
							}
							if(time.getText().equals(" ") || CallingActivityNew.INSTANCE.mBar.isShowing()) {
								//means that it is a missed incoming call, just close.
								//OR we are in a multiple call, and wait before closing a finished call
								//seems very unstable. So close this call IMMEDIATELY.
								finish();
								return;
							}
							//flash text for 3 seconds
							for(int i = 0; i < 3; i++) {
								try {
									setTime(text);
									Thread.sleep(500);
									setTime("");
									Thread.sleep(500);
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
							}
							
							//and close the activity.
							finish();
						}
					}; fil.start();
				}
				if(intent.getAction().equals(Constants.ACTION_SIP_FAILED)) {
					//same thing, but we blink an error message
					if(dialog != null) dialog.cancel();
					
					//set the right color for the transfer button
					TransferButton.setBackgroundColor(Color.parseColor("#666666"));
					
					Thread fil = new Thread("Activity Finisher on Fail") {
						@Override public void run() {
							isCalling = false;
							setTime("Error: "+intent.getStringExtra("errorMessage"));
							//flash text for 3 seconds
							for(int i = 0; i < 3; i++) {
								try {
									setTime("Error: "+intent.getStringExtra("errorMessage"));
									Thread.sleep(500);
									setTime("");
									Thread.sleep(500);
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
							}
							
							//wait 2 s more
							setTime("Error: "+intent.getStringExtra("errorMessage"));
							try {
								Thread.sleep(2000);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
							
							//and close the activity.
							finish();
						}
					}; fil.start();
				}
			}
		};
		//filter so that br receives only call-related broadcasts
		IntentFilter filter = new IntentFilter();
		filter.addAction(Constants.ACTION_SIP_ESTABLISHED);
		filter.addAction(Constants.ACTION_SIP_HUNG_UP);
		filter.addAction(Constants.ACTION_SIP_FAILED);
		filter.addAction(Constants.ACTION_SIP_RINGING);
		
		//react when the user hangs up
		((Button)HangupButton).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
					//handles the case of hanging up before the beginning of a call.
					//in this case, the CALL_HUNG_UP broadcast is not sent, so we have
					//to close the activity ourselves.
					if(!currentCall.isInCall()) finish();
					
					try {
						currentCall.endCall();
					} catch (SipException e) {
						e.printStackTrace();
					}
					
			}
		});
		
		//react when the user wants a transfer
		((Button)TransferButton).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(!currentCall.isInCall()) return;
				
				//shows an alert dialog to get the number
				AlertDialog.Builder alert = new AlertDialog.Builder(theFragContext);
				alert.setTitle(theFragContext.getString(R.string.blind_transfer_title))
				.setMessage(theFragContext.getString(R.string.transfer_prompt)+" :");
				final EditText edit = new EditText(theFragContext);
				edit.setHint(theFragContext.getString(R.string.transfer_hint));
				edit.setInputType(InputType.TYPE_CLASS_PHONE);
				edit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
				    @Override
				    public void onFocusChange(View v, boolean hasFocus) {
				        if (hasFocus) {
				            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
				        }
				    }
				});
				alert.setView(edit);
				alert.setPositiveButton(theFragContext.getString(R.string.ok), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						//actually do the transfer
						final String value = edit.getText().toString().replace(" ", "").replace("-", "")
								.replace("+", "00");
						
						try {
							Long.parseLong(value);
						} catch(NumberFormatException nfe) {
							Toast.makeText(theFragContext, theFragContext.getString(R.string.invalid), 
									Toast.LENGTH_LONG).show();
							return;
						}

						secondDiscount = Byte.MAX_VALUE;
						setTime(theFragContext.getString(R.string.transfering).replace("%s", value));
						
						//hide the dialer, and make EVERY SINGLE BUTTON disappear!
						(imageView2).setVisibility(View.VISIBLE);
						(holdcall).setVisibility(View.VISIBLE);
						(dialpad).setVisibility(View.INVISIBLE);
						speaker.setVisibility(View.INVISIBLE);
						mute.setVisibility(View.INVISIBLE);
						show_dialer.setVisibility(View.INVISIBLE);
						HangupButton.setVisibility(View.INVISIBLE);
						HangupButtonText.setVisibility(View.INVISIBLE);
						TransferButton.setVisibility(View.INVISIBLE);
						TransferButtonText.setVisibility(View.INVISIBLE);
						holdcall.setVisibility(View.INVISIBLE);
						
						//now, here is the DTMF thread
						Thread dtmf = new Thread("DTMF thread") {
							private static final int TIMING = 200;
							@Override public void run() {
								try {
									sendDtmf(10);
									Thread.sleep(TIMING);
									sendDtmf(1);
									Thread.sleep(2000);
									
									for(int i = 0; i < value.length(); i++) {
										int dtmf = Integer.parseInt(""+value.charAt(i));
										sendDtmf(dtmf);
										Thread.sleep(TIMING);
									}

									secondDiscount = 15;
								} catch(InterruptedException ie) { }
							}
						}; dtmf.start();
					}
				});
				alert.setNegativeButton(theFragContext.getString(R.string.cancel_label), null);
				alert.setOnCancelListener(new OnCancelListener() {
					@Override
					public void onCancel(DialogInterface dialog) {
						dialog = null;
					}
				});
				dialog = alert.show();
			}
		});
		
		//showing or hiding the dialer
		((ToggleButton)show_dialer).setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked) {
					//now, show the dialer and hide that phone!
					(imageView2).setVisibility(View.INVISIBLE);
					(holdcall).setVisibility(View.INVISIBLE);
					(dialpad).setVisibility(View.VISIBLE);
				} else {
					//do the contrary
					(imageView2).setVisibility(View.VISIBLE);
					(holdcall).setVisibility(View.VISIBLE);
					(dialpad).setVisibility(View.INVISIBLE);
				}
			}
		});
		
		//clicking the "mute" button
		((ToggleButton)mute).setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(currentCall.isMuted() != isChecked)
					currentCall.toggleMute();
				
				//update the icon!
				if(isChecked)
					((ToggleButton) mute).setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.mute, 0, 0);
				else
					((ToggleButton) mute).setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.not_mute, 0, 0);
				
				CallingActivityNew.INSTANCE.mCallScreens.toggleAllMutes
					(((ToggleButton)mute).isChecked());
			}
		});
		
		//handle speaker mode
		((ToggleButton)speaker).setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				currentCall.setSpeakerMode(isChecked);
				
				//update the icon!
				if(isChecked)
					((ToggleButton) speaker).setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.speaker, 0, 0);
				else
					((ToggleButton) speaker).setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.speaker_off, 0, 0);
				
				CallingActivityNew.INSTANCE.mCallScreens.toggleAllSpeakers
						(((ToggleButton)speaker).isChecked());
			}
		});
		
		//"hold" / "Continue" button
		((ToggleButton)holdcall).setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				try {
					if(isChecked) {
						//hold the call and set the "resume" icon
						try {
							currentCall.holdCall(0);
						} catch (SipException e) {
							e.printStackTrace();
						}
						((ToggleButton)holdcall).setCompoundDrawablesWithIntrinsicBounds(R.drawable.resume_call, 0, 0, 0);
					} else {
						//just call switchOnCall to hold all the other calls & continue this one.
						CallingActivityNew.INSTANCE.switchOnCall(CallingFragment.this);
					}
				}
				  catch(NullPointerException e) {} //some internal NullPointerException happened during the tests...
			}
		});
		
		//getting the 3 TextViews at the top of the screen
		TextView name = (TextView) v.findViewById(R.id.userNameHeader);
		TextView number = (TextView) v.findViewById(R.id.UserNumber);
		time = (TextView) v.findViewById(R.id.TheTime);
		
		SipProfile profile = currentCall.getPeerProfile();
		if(profile == null) {
			String s = getString(R.string.unknown);
			//looks complicated, but it's just to have a capital first letter.
			name.setText(s.substring(0, 1).toUpperCase() + s.substring(1));
			time.setText(R.string.does_not_exist);
			return v;
		} else {
			//get the user's number.
			String originatingNumber = profile.getUriString();
			//sip:12345@192.168.0.1 --> 12345
			originatingNumber = originatingNumber.substring(4, originatingNumber.indexOf("@"));
			number.setText(originatingNumber);
			
			//get the user's name, according to the user provider's list.
			Cursor user = theFragContext.getContentResolver().query(UserProvider.CONTENT_URI, null,
					UserProvider.PHONENUM + " LIKE '" + originatingNumber + "'",
					null, UserProvider.PHONENUM);
			
			if(user.moveToFirst()) {
				name.setText(user.getString(user.getColumnIndex(UserProvider.FULLNAME)));
			} else {
				//try the ContactsContract database.
				Cursor userII = theFragContext.getContentResolver().query(ContactsContract.Data.CONTENT_URI, 
						//projection: the MIME type and the phone number. We will also need the contact ID.
						new String[] { ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.NUMBER,
							ContactsContract.CommonDataKinds.Phone.RAW_CONTACT_ID},
						//selection: only phone numbers
						ContactsContract.Data.MIMETYPE+ "=?",
						new String[] { ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE }, null);
				
				String contName = null;
				if(userII.moveToFirst()) {
					String comparateNumber = originatingNumber.replace(" ", "").replace("-", "").replace("+33", "0");
					do {
						//get this line's phone number
						String candidateNumber = userII.getString(userII.getColumnIndex(
								ContactsContract.CommonDataKinds.Phone.NUMBER));
						
						//comparate the two
						candidateNumber = candidateNumber.replace(" ", "").replace("-", "").replace("+33", "0");
						Log.v("Calling Fragment", candidateNumber+" == "+comparateNumber);
						if(candidateNumber.equals(comparateNumber)) {
							//it is right, get his id!
							String id = userII.getString(userII.getColumnIndex(
									ContactsContract.CommonDataKinds.Phone.RAW_CONTACT_ID));
							
							Cursor userIII = theFragContext.getContentResolver().query(ContactsContract.Data.CONTENT_URI,
									//projection: we will need the contact id, the MIME type and the display name.
									new String[] { ContactsContract.Data.MIMETYPE, 
									ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME,
									ContactsContract.CommonDataKinds.StructuredName.RAW_CONTACT_ID},
									//selection: matching the contact id, and of the right type.
									ContactsContract.Data.MIMETYPE+ "=? AND "
											 +ContactsContract.CommonDataKinds.StructuredName.RAW_CONTACT_ID+"=?",
									new String[] { ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE, id }
											, null);
							
							if(userIII.moveToFirst()) {
								//extract the name!!!
								contName = userIII.getString(userIII.getColumnIndex(
										ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME));
								userIII.close();
								//now, stop looking for it, we FINALLY found it.
								break;
							}
							userIII.close();
						}
					} while(userII.moveToNext());
				}
				
				if(contName != null) {
					name.setText(contName);
					number.setText(originatingNumber);
				} else {
					number.setHeight(0); //better to completely hide the number field
					name.setText(originatingNumber);
				}
				userII.close();
			}
			user.close();
		}
		
		
		
		//set the name of the tab to the user's name too.
		actionBarsTab.setText(name.getText());
		
		//don't show "Ringing" right now, wait until onRinging is called
		if(!alreadyRinging)
			setTime(" ");

		if(currentCall.isInCall()) {
			//set the right color for the transfer button
			TransferButton.setBackgroundColor(Color.parseColor("#0050AB"));
			
			//got to start the call chronometer right now
			if(callChronometer == null) {
				callChronometer = newChrono();
				callChronometer.start();
			}
		} else {
			//set the right color for the transfer button
			TransferButton.setBackgroundColor(Color.parseColor("#666666"));
		}
		
		//compatibility: clickOn0, etc. do not work anymore when declared in the XML file
		//with the Fragments. So declare them here.
		v.findViewById(R.id.one).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) { clickOn1(v); }
		});
		v.findViewById(R.id.two).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) { clickOn2(v); }
		});
		v.findViewById(R.id.three).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) { clickOn3(v); }
		});
		v.findViewById(R.id.four).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) { clickOn4(v); }
		});
		v.findViewById(R.id.five).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) { clickOn5(v); }
		});
		v.findViewById(R.id.six).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) { clickOn6(v); }
		});
		v.findViewById(R.id.seven).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) { clickOn7(v); }
		});
		v.findViewById(R.id.eight).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) { clickOn8(v); }
		});
		v.findViewById(R.id.nine).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) { clickOn9(v); }
		});
		v.findViewById(R.id.zero).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) { clickOn0(v); }
		});
		v.findViewById(R.id.star).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) { clickOnStar(v); }
		});
		v.findViewById(R.id.pound).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) { clickOnSharp(v); }
		});
		
		//set up the letters and numbers of all the digits
  		int[] toAdapt = { R.id.zero, R.id.one,
  				R.id.two, R.id.three, R.id.four, R.id.five, R.id.six, R.id.seven,
  				R.id.eight, R.id.nine };
  		String[] alpha = { "+", "", "ABC", "DEF", "GHI", "JKL", "MNO", "PQRS", "TUV", "WXYZ" };
  		
  		for(int i1 = 0; i1 < 10; i1++) {
  			FrameLayout that = ((FrameLayout) v.findViewById(toAdapt[i1]));
  			((TextView) that.findViewById(R.id.dialpad_key_number)).setText(""+i1);
  			((TextView) that.findViewById(R.id.dialpad_key_letters)).setText(alpha[i1]);
  		}
  		
		
		//finally, register the receiver
		theFragContext.registerReceiver(br, filter);
		
		return v;
	}
	
	/**
	 * Called each time the "time" text should be changed (on the UI thread)
	 * @param text
	 */
	private void setTime(final String text) {
		//only the UI can touch the views...
		if(getActivity() != null)
			getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					if(time != null)
						time.setText(text);
				}
			});
	}
	
	/**
	 * Creates a new call chronometer.
	 * @return an unstarted (but ready to start!) Call Chronometer.
	 */
	private Thread newChrono() {
		return new Thread("Call Chronometer") {
			@Override public void run() {
				isCalling = true;
				minutes = 0;
				seconds = 0;
				setTime("0:00");
				try {
					while(isCalling) {
						//refresh the time every second
						Thread.sleep(1000);
						seconds++;
						if(seconds == 60) {
							minutes++;
							seconds = 0;
						}
						if(isCalling && secondDiscount == 0)
							setTime(minutes+":"+(seconds < 10 ? "0" : "") + seconds);
					
						if(secondDiscount > 0) {
							secondDiscount --;
							String s = "0";
							for(int i = 1; i < 5; i++) s += (secondDiscount >= i ? "#" : " ");
							s += "5";
							for(int i = 6; i < 15; i++) s += (secondDiscount >= i ? "#" : " ");
							s += "|";
							for(int i = 16; i < 20; i++) s += (secondDiscount >= i ? "#" : " ");
							
							Log.v("Call Chronometer", "Second Discount = "+s);
						}
						if(secondDiscount == 5) {
							if(getActivity() != null)
								getActivity().runOnUiThread(new Runnable() {
									@Override public void run() {
										//here come the buttons again!
										//hide the dialer, and make EVERY SINGLE BUTTON disappear!
										speaker.setVisibility(View.VISIBLE);
										mute.setVisibility(View.VISIBLE);
										show_dialer.setVisibility(View.VISIBLE);
										HangupButton.setVisibility(View.VISIBLE);
										HangupButtonText.setVisibility(View.VISIBLE);
										TransferButton.setVisibility(View.VISIBLE);
										TransferButtonText.setVisibility(View.VISIBLE);
										holdcall.setVisibility(View.VISIBLE);
									}
								});
							
							//and set the time
							setTime(getString(R.string.tranfer_fail));
						}
					}
				} catch (InterruptedException e) {}
			}
		};
	}
	
	/**
	 * Sends a DTMF tone in the call and plays it to the user.
	 * @param code 0~9 for number, 10 for * and 11 for #
	 */
	private void sendDtmf(int code) {
		ToneGenerator dtmfGenerator = new ToneGenerator(0,ToneGenerator.MAX_VOLUME);
		dtmfGenerator.startTone(code, 200);
		currentCall.sendDtmf(code);
	}
	
	//associate the dialer's keys with DTMF tones.
	private void clickOn0(View v) {
		sendDtmf(0);
	}
	private void clickOn1(View v) {
		sendDtmf(1);
	}
	private void clickOn2(View v) {
		sendDtmf(2);
	}
	private void clickOn3(View v) {
		sendDtmf(3);
	}
	private void clickOn4(View v) {
		sendDtmf(4);
	}
	private void clickOn5(View v) {
		sendDtmf(5);
	}
	private void clickOn6(View v) {
		sendDtmf(6);
	}
	private void clickOn7(View v) {
		sendDtmf(7);
	}
	private void clickOn8(View v) {
		sendDtmf(8);
	}
	private void clickOn9(View v) {
		sendDtmf(9);
	}
	private void clickOnStar(View v) {
		sendDtmf(10);
	}
	private void clickOnSharp(View v) {
		sendDtmf(11);
	}

	/**
	 * "Finish" the fragment = close the tab. (compatibility: before being a fragment, the CallingFragment was an Activity)
	 */
	private void finish() {
		if(CallingActivityNew.INSTANCE != null)
			CallingActivityNew.INSTANCE.removeFragment(this);
	}
	
}
