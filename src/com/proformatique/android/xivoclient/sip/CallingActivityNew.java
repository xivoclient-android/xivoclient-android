package com.proformatique.android.xivoclient.sip;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.sip.SipAudioCall;
import android.net.sip.SipException;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.ToggleButton;

import com.proformatique.android.xivoclient.Connection;
import com.proformatique.android.xivoclient.ExiterActivity;
import com.proformatique.android.xivoclient.R;
import com.proformatique.android.xivoclient.xlets.XletContactSearch;
import com.proformatique.android.xivoclient.xlets.XletDialer;
import com.proformatique.android.xivoclient.xlets.XletHisto;

public class CallingActivityNew extends ExiterActivity {
	/**
	 * Object containing the list of the CallingFragments.
	 */
	CallScreens mCallScreens;
	
    ViewPager mViewPager;
    ActionBar mBar;
    
    boolean willMoveTaskToBack = false;
    
    /**
     * Determines if we have two calls running.
     * @return
     */
	public boolean hasMultipleCalls() {
		return mBar.isShowing();
	}
    
    /**
     * is null when the call window is not showing ; there should be only ONE instance of the activity
     */
    public static CallingActivityNew INSTANCE = null;
    
    /**
     * toggles from one view to another when clicking on a tab
     */
    ActionBar.TabListener tabListener = new ActionBar.TabListener() {
		@Override
		public void onTabUnselected(Tab arg0, FragmentTransaction arg1) {
			//never mind...
		}
		
		@Override
		public void onTabSelected(Tab arg0, FragmentTransaction arg1) {
			//go to the selected tab.
			mViewPager.setCurrentItem(arg0.getPosition() ,true);
		}
		
		@Override
		public void onTabReselected(Tab arg0, FragmentTransaction arg1) {
			//never mind.
		}
	};

	/**
	 * Called when the activity is created ; initialize the objects.
	 */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.call_screen_new);
        setTitle(getString(R.string.multicalls));
        
        willMoveTaskToBack = getIntent().getBooleanExtra("fromDialer", false);

        //add a tab action bar
        mBar = getSupportActionBar();
        mBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // ViewPager and its adapters use support library
        // fragments, so use getSupportFragmentManager.
        mCallScreens = new CallScreens(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.pager);
        //add the first call to the swiping view
        addCall(getIntent().getBooleanExtra("incoming", false));
        //select the corresponding tab when changing screen by swiping
        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
        	@Override public void onPageSelected(int position) {
        		mBar.setSelectedNavigationItem(position);
        	}
        });
        
        mViewPager.setAdapter(mCallScreens);
        
        INSTANCE = this;
        
        //If ringing when the phone is locked, the lock screen will be dismissed
        //even when locked with a password. But the lock screen will be immediately shown
        //after the call is finished. (in fact, like a normal call)
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        
        //we want the screen to be on for the whole call(s).
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
    }
    
    /**
     * Adds a call to the list, based on the information given by SipApiUser.INSTANCE.
     * @param incoming if it is an outgoing call, then incoming = false.
     */
    public void addCall(final boolean incoming) {
    	//run everything on the UI thread, or we are going to have trouble adding calls
    	//from elsewhere than the calling activity's own UI thread
    	runOnUiThread(new Runnable() {
			@Override
			public void run() {
				//creates the fragment with all the parameters
		    	CallingFragment frag = new CallingFragment(mBar.getTabCount() + 1);
		    	Bundle args = new Bundle();
		    	args.putBoolean("incoming", incoming);
		    	args.putBoolean("isInCall", mBar.getTabCount() > 0);
		    	frag.setArguments(args);
		    	
		    	//add the tab
		    	Tab tab = mBar.newTab().setText(R.string.unknown).setTabListener(tabListener);
		    	mBar.addTab(tab);
		    	frag.actionBarsTab = tab;
		    	
		    	//now, we are ready to add the fragment to the ViewPager!
		    	mCallScreens.addTab(frag);
		    	mViewPager.setAdapter(mCallScreens);
		    	
		    	//show the action bar (and the tabs) if there are multiple calls
		    	if(mBar.getTabCount() > 1) {
		    		mBar.show();
		    		mCallScreens.toggleAllMutes(false);
		    		mCallScreens.toggleAllSpeakers(false);
		    	} else
		    		mBar.hide();
		    	
		    	//and switch to this new call, if it is not incoming
		    	//(for incoming calls, the switching will occur when the user answers the call)
		    	if(!incoming) switchOnCall(frag);
		    	
		        //switch to the last page
		        mViewPager.setCurrentItem(mCallScreens.getCount() - 1);
		        mBar.setSelectedNavigationItem(mCallScreens.getCount() - 1);
			}
		});
    	
    	
    }
    
    /**
     * Removes a call from the call screen. It will close the activity if it was the only call running.
     * @param f the {@link CallingFragment} showing this call.
     */
    public void removeFragment(final CallingFragment f) {
    	//has to run notifyDataSetChanged() (in mCallScreens.removeFragment) from the UI thread!
    	runOnUiThread(new Runnable() {
			@Override
			public void run() {
				try {
					mCallScreens.removeFragment(f);
					
					//must remove the tab too
					mBar.removeTab(f.actionBarsTab);
			    	mViewPager.setAdapter(mCallScreens);
			    	if(mCallScreens.getCount() == 0) {
			    		if(willMoveTaskToBack) moveTaskToBack(true);
			    		finish();
			    		INSTANCE = null;
			    	}
			    	
			    	//hide the action bar if there is only one call left.
			    	//AND continue this call if it was on hold.
			    	if(mBar.getTabCount() == 1) {
			    		mBar.hide();
			    		switchOnCall(mCallScreens.listFragments.get(0));
			    	}
				} catch(Exception e) {
					e.printStackTrace();
		    	}
			}
		});
    }
    
    /**
     * Holds all the active calls, and continues the one specified in the argument.
     * @param thisOne the Fragment containing the call to continue.
     */
    public void switchOnCall(CallingFragment aFragment) {
    	mCallScreens.switchOnCall(aFragment, getApplicationContext());
    }
    
    /**
     * Disables the back button.
     */
    @Override
    public void onBackPressed() {
    	//let's disable the back button.
    }
    
    /**
     * Inflates all the menu items.
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_in_call, menu);
		return true;
    };
    
    /**
     * Decides whether the menu items will be visible or not.
     * (if the XiVO connection service is online && there is only one call, it will be visible)
     * @param menu
     * @return
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
    	Log.d("Calling Activity", "onPrepareOptionsMenu");
    	try {
			boolean b = (Connection.INSTANCE != null && Connection.INSTANCE.getConnection(getApplicationContext()) != null
					&& Connection.INSTANCE.getConnection(getApplicationContext()).isAuthenticated()
					&& !hasMultipleCalls());
			if(!b) {
			      //show an alert-dialog to notify the user that it is bad!
			      AlertDialog.Builder builder = new AlertDialog.Builder(this);
				  builder.setTitle(R.string.sip_error);
				  builder.setMessage(R.string.three_calls);
				  builder.setPositiveButton(R.string.ok, null);
				  builder.show();
			}
			return b;
		} catch (RemoteException e) {
			e.printStackTrace();
		}
    	
    	return true;
    }
    
    /**
     * Reacts when a menu item is selected, launch the corresponding activity.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
    	Intent i = null;
        switch (item.getItemId()) {
            case R.id.pop_composition:
            	i = new Intent(this, XletDialer.class); i.putExtra("edit", true); break;
            case R.id.pop_contacts:
            	i = new Intent(this, XletContactSearch.class); break;
            case R.id.pop_histo:
            	i = new Intent(this, XletHisto.class); break;
            default:
                return super.onOptionsItemSelected(item);
        }
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		i.putExtra("duringCall", true);
        startActivity(i);
        return true;
    }
}

/**
 * Holds all the call screens, and display them in a "pager" view.
 */
class CallScreens extends FragmentStatePagerAdapter {
	ArrayList<CallingFragment> listFragments;
	
	/**
	 * Adds a tab in the ViewPager
	 * @param withThis the tab to add
	 */
    public void addTab(CallingFragment withThis) {
    	Log.v("Calling Activity", "Added a tab");
    	listFragments.add(withThis);
    	notifyDataSetChanged();
    }
    
    /**
	 * Removes a tab from the ViewPager
	 * @param f the tab to add
     */
    public void removeFragment(Fragment f) {
    	Log.v("Calling Activity", "Removed a tab");
    	listFragments.remove(f);
    	notifyDataSetChanged();
    }
	
    /**
     * Build a new {@link FragmentStatePagerAdapter} with an empty list of {@link Fragment}s.
     * @param fm
     */
	public CallScreens(FragmentManager fm) {
        super(fm);
        listFragments = new ArrayList<CallingFragment>();
    }

	/**
	 * Gets a {@link CallingFragment} from the list.
	 */
    @Override
    public Fragment getItem(int i) {
    	Log.v("Calling Activity", "Got asked the item "+i);
        return listFragments.get(i);
    }
    
    /**
     * Gets the position of the provided item.
     */
    @Override
    public int getItemPosition(Object object) {
    	int index = listFragments.indexOf(object);
    	Log.v("Calling Activity", "Got asked a position, answered "+index);
    	//if object was not found, then we have index = -1
    	//so tell the ViewPager that this object does not exist anymore
    	if(index == -1) return POSITION_NONE;
    	else return index;
    }

    /**
     * Gets the screen count.
     */
    @Override
    public int getCount() {
    	Log.v("Calling Activity", "Got to give a count, gave "+listFragments.size());
        return listFragments.size();
    }
    
    /**
     * Toggle the mute state of all the calls. It will also set the {@link SipAudioCall} states.
     * @param isMute if the call should be mute or not
     */
    public void toggleAllMutes(boolean isMute) {
    	for(CallingFragment one : listFragments) {
    		if(one.mute != null) {
    			((ToggleButton) one.mute).setChecked(isMute);
    			if(one.currentCall.isMuted() != isMute) one.currentCall.toggleMute();
    		}
    	}
    }

    /**
     * Toggle the speaker state of all the calls. It will also set the {@link SipAudioCall} states.
     * @param isMute if the call should be on speaker mode or not
     */
    public void toggleAllSpeakers(boolean isMute) {
    	for(CallingFragment one : listFragments) {
    		if(one.speaker != null) {
    			((ToggleButton) one.speaker).setChecked(isMute);
    			one.currentCall.setSpeakerMode(isMute);
    		}
    	}
    }
    
    /**
     * Holds all the calls, but the one contained in this Fragment: it will be continued instead.
     * @param thisOne this fragment
     * @param con a context to get all the icons
     */
    public void switchOnCall(CallingFragment thisOne, Context con) {
    	for(CallingFragment one : listFragments) {
    		if(one.equals(thisOne)) {
    			//continue the call if it was suspended
    			if(one.currentCall != null && one.currentCall.isOnHold())
	    			try {
						one.currentCall.continueCall(0);
		    			((ToggleButton) one.holdcall).setChecked(false);
						((ToggleButton) one.holdcall).setCompoundDrawablesWithIntrinsicBounds(R.drawable.holdicon, 0, 0, 0);
	    			} catch(NullPointerException npe) {
						Log.w("Calling Activity", "Warning! Could not hold all calls!");
					} catch (SipException e) {
						e.printStackTrace();
					}
    		} else {
    			if(!one.currentCall.isOnHold()) //hold this call if not already held
    				try {
						one.currentCall.holdCall(0);
						((ToggleButton) one.holdcall).setChecked(true);
						((ToggleButton) one.holdcall).setCompoundDrawablesWithIntrinsicBounds(R.drawable.resume_call, 0, 0, 0);
					} catch (NullPointerException npe) {
						Log.w("Calling Activity", "Warning! Could not hold all calls!");
					} catch (SipException e) {
						e.printStackTrace();
					}
    		}
    	}
    }
}
