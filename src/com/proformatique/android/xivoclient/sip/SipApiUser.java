package com.proformatique.android.xivoclient.sip;

import java.io.IOException;
import java.text.ParseException;

import com.proformatique.android.xivoclient.BringToFront;
import com.proformatique.android.xivoclient.R;
import com.proformatique.android.xivoclient.tools.Constants;

import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.sip.SipAudioCall;
import android.net.sip.SipErrorCode;
import android.net.sip.SipException;
import android.net.sip.SipManager;
import android.net.sip.SipProfile;
import android.net.sip.SipRegistrationListener;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

/**
 * Utility to make common SIP operations.
 */
public class SipApiUser implements OnPreparedListener {
	public static final String TAG = "SIP API User";
	
	public Context theContext;
	public SipManager theManager;
	public SipProfile theProfile;
	public MediaPlayer soundEffects;
	
	private boolean isClosed = false;
	
	/**
	 * When a new call will be added, CallingFragment will copy this to get the call.
	 * And when an Intent is sent, this is set to the concerned call, so that the receivers know which call is concerned.
	 */
	public SipAudioCall currentCall;
	/**
	 * If the last launched call has already begun to ring.
	 */
	public boolean isAlreadyRinging = false;
	
	/*
	 * TODO this INSTANCE should be moved to the XivoConnectionService, and we also should make this service
	 * remote (so that it is not killed with our app). But it looks very complicated to do that...
	 */
	/**
	 * This instance is unique, and is null if SIP is not connected.
	 */
	public static SipApiUser INSTANCE;
	
	/**
	 * If registration fails and this may be related to bad settings, this becomes true.
	 */
	public static boolean badSettings = false;
	
	/**
	 * Creates a SIP API User and updates the INSTANCE. The user will be connected with the server/username/password provided.
	 * @param c
	 * @param domain
	 * @param username
	 * @param password
	 * @throws ParseException
	 * @throws SipException
	 */
	public SipApiUser(Context c, String domain, String username, String password) throws ParseException, SipException {
		//do not even TRY to connect to SIP if the phone does not actually support it!
		if(!SipManager.isApiSupported(c) || !SipManager.isVoipSupported(c)) 
				throw new SipException("SIP not supported");
		
		Log.i(TAG, "Creating SIP Connection");
		
		theContext = c;
		theManager = SipManager.newInstance(c);
		Log.i(TAG, "Creating a SIP API User");
		generateProfile(username, password, domain);
		Log.i(TAG, "Created a SIP API User");
		
		INSTANCE = this;
	}
	
	
	/**
	 * Generates the SipProfile and sets the auto-register option.
	 * @param username
	 * @param password
	 * @param domain
	 * @throws ParseException
	 * @throws SipException
	 */
	private void generateProfile(String username, String password, String domain) throws ParseException, SipException {
		//build the profile
		SipProfile.Builder b = new SipProfile.Builder(username, domain);
		b.setPassword(password);
		b.setAutoRegistration(true);
		theProfile = b.build();
		Log.v(TAG, "Built theProfile "+theProfile.getAutoRegistration());
		
		//make ready to receive incoming calls
		Intent intent = new Intent();
		intent.setAction(Constants.ACTION_SIP_INCOMING);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(theContext, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
		
		//be notified when the registration is OK or has a problem
		SipRegistrationListener listener = new SipRegistrationListener() {
            public void onRegistering(String localProfileUri) {
                Log.i(TAG, "Registering "+localProfileUri+" with SIP Server...");
            }

            public void onRegistrationDone(String localProfileUri, long expiryTime) {
                Log.i(TAG, "Registered "+localProfileUri+".");
                badSettings = false;
            }

            public void onRegistrationFailed(String localProfileUri, int errorCode,
                    String errorMessage) {
                Log.w(TAG, "Registration failed. "+errorMessage +" ("+errorCode+")");
                if(errorCode == SipErrorCode.INVALID_CREDENTIALS || 
                		errorCode == SipErrorCode.INVALID_REMOTE_URI ||
                		errorCode == SipErrorCode.SERVER_ERROR ||
                		errorCode == SipErrorCode.TRANSACTION_TERMINTED ||
                		errorCode == SipErrorCode.TIME_OUT) {
                	//All these codes may mean that the connect information provided by the user is invalid.
                	//experiments show: invalid server ==> time out
                	//				    invalid credentials ==> transaction terminated
                	//				    no connection: throws an InvalidArgumentException caught by the system
                	
                	//prevents an exception about Looper with Toast.makeText.
                	//RuntimeException is thrown if Looper is already prepared.
                	//But it's not always the case...
                	try {
                		Looper.prepare();
                	} catch(RuntimeException re) {}
                	
                	try {
						theManager.close(theProfile.getUriString());
					} catch (SipException e) {
						e.printStackTrace();
					}
					//Toast.makeText(theContext, theContext.getString(R.string.sip_config), Toast.LENGTH_LONG).show();
					
                	badSettings = true;
                	INSTANCE = null;
                }
            }
        };
		
        //finally, open the profile
		theManager.open(theProfile, pendingIntent, null);
		theManager.setRegistrationListener(theProfile.getUriString(), listener);
		
		Log.i(TAG, "Created the profile of "+theProfile.getUriString());
	}
	
	/**
	 * Closes the user's profile and makes INSTANCE null.
	 */
	public void closeConnection() {
		isClosed = false;
		try {
			theManager.unregister(theProfile, new SipRegistrationListener() {
				@Override
				public void onRegistrationFailed(String localProfileUri, int errorCode,
						String errorMessage) {
					Log.w("Sip API User", "Unregtration failed. "+errorMessage);
					isClosed = true;
				}
				
				@Override
				public void onRegistrationDone(String localProfileUri, long expiryTime) {
					Log.i("Sip API User", "Unregistered user "+localProfileUri);
					isClosed = true;
				}
				
				@Override
				public void onRegistering(String localProfileUri) {
					Log.i("Sip API User", "Unregistering user "+localProfileUri);
				}
			});
		} catch (SipException e1) {
			e1.printStackTrace();
		}
		
		//wait until it is unregistered with a max of 10 seconds
		for(int i = 0; i < 100 && !isClosed; i++)
			try {
				Thread.sleep(100);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
		
		if(theProfile != null) {
			try {
				theManager.close(theProfile.getUriString());
				INSTANCE = null;
				Log.i(TAG, "Removed SIP Connection");
			} catch (SipException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Make a phone call from the app. Will automatically open the call screen or add a tab to the existing one.
	 * @param somebody The number to call
	 * @return The call
	 * @throws SipException
	 * @throws ParseException
	 */
	public SipAudioCall call(String somebody) throws SipException, ParseException {
		return call(somebody, false);
	}
	
	/**
	 * Make a phone call. Will automatically open the call screen or add a tab to the existing one.
	 * @param somebody The number to call
	 * @param fromDialer If the call was made from Android's dialer (the app should move to the background when all calls are finished).
	 * @return The call
	 * @throws SipException
	 * @throws ParseException
	 */
	public SipAudioCall call(String somebody, boolean fromDialer) throws SipException, ParseException {
		isAlreadyRinging = false;
		
		SipAudioCall.Listener listener = new SipAudioCall.Listener() {
			@Override public void onRingingBack(SipAudioCall cll) {
				Log.i(TAG, "Call is ringing");
				
				Intent intent = new Intent();
				intent.setAction(Constants.ACTION_SIP_RINGING);
				currentCall = cll;
				PendingIntent pendingIntent = PendingIntent.getBroadcast(theContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
				try {
					pendingIntent.send();
				} catch (CanceledException e) {
					e.printStackTrace();
				}
				
				isAlreadyRinging = true;
				
				//this plays a sound effect.
				AssetFileDescriptor afd = theContext.getResources().openRawResourceFd(R.raw.ringbacktone);
				if(soundEffects != null && soundEffects.isPlaying()) soundEffects.stop();
				soundEffects = new MediaPlayer();
				try {
					soundEffects.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
				} catch (IllegalArgumentException e1) {
					e1.printStackTrace();
				} catch (IllegalStateException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				soundEffects.setAudioStreamType(AudioManager.STREAM_VOICE_CALL);
				soundEffects.setOnPreparedListener(SipApiUser.this);
				soundEffects.setLooping(true);
				try {
					soundEffects.prepareAsync();
				} catch (IllegalStateException e) {
					e.printStackTrace();
				}
			}
			
			@Override
			public void onCallEstablished(SipAudioCall call) {
				//start the audio
				call.startAudio();
				
				Log.i(TAG, "Call begins now!");
				
				Intent intent = new Intent();
				intent.setAction(Constants.ACTION_SIP_ESTABLISHED);
				currentCall = call;
				PendingIntent pendingIntent = PendingIntent.getBroadcast(theContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
				try {
					pendingIntent.send();
				} catch (CanceledException e) {
					e.printStackTrace();
				}
				
				//do not forget to cancel the sound effect
				if(soundEffects != null && soundEffects.isPlaying()) soundEffects.stop();
			}
			
			@Override
			public void onCallEnded(SipAudioCall call) {
				//nothing for this time.
				Log.i(TAG, "Your call just ended!");
				call.close();
				
				Intent intent = new Intent();
				intent.setAction(Constants.ACTION_SIP_HUNG_UP);
				currentCall = call;
				PendingIntent pendingIntent = PendingIntent.getBroadcast(theContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
				try {
					pendingIntent.send();
				} catch (CanceledException e) {
					e.printStackTrace();
				}
				
				//cancel any running sound effect
				if(soundEffects != null && soundEffects.isPlaying()) soundEffects.stop();
				//and play the "end call" one
				AssetFileDescriptor afd = theContext.getResources().openRawResourceFd(R.raw.endcall);
				if(soundEffects != null && soundEffects.isPlaying()) soundEffects.stop();
				soundEffects = new MediaPlayer();
				try {
					soundEffects.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
				} catch (IllegalArgumentException e1) {
					e1.printStackTrace();
				} catch (IllegalStateException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				soundEffects.setAudioStreamType(AudioManager.STREAM_VOICE_CALL);
				soundEffects.setOnPreparedListener(SipApiUser.this);
				try {
					soundEffects.prepareAsync();
				} catch (IllegalStateException e) {
					e.printStackTrace();
				}
			}
			
			@Override
			public void onError(SipAudioCall call, int errorCode, String errorMessage) {
				Toast.makeText(theContext, "SIP error: "+errorMessage, Toast.LENGTH_LONG).show();
				Log.w(TAG, "Call failed: "+errorMessage);
				
				Intent intent = new Intent();
				intent.setAction(Constants.ACTION_SIP_FAILED);
				currentCall = call;
				intent.putExtra("errorMessage", errorMessage);
				PendingIntent pendingIntent = PendingIntent.getBroadcast(theContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
				try {
					pendingIntent.send();
				} catch (CanceledException e) {
					e.printStackTrace();
				}
			}
		};

		//build the peer SIP profile
		SipProfile.Builder b = new SipProfile.Builder(somebody, theProfile.getSipDomain());
		SipProfile called = b.build();
		Log.i(TAG, "Calling "+called.getUriString());
		
		//launch the audio call for real
		currentCall = theManager.makeAudioCall(theProfile, called, listener, 0);
		
		if(currentCall != null)
			if(CallingActivityNew.INSTANCE == null) {
				//open a new Calling Activity
				Intent i = new Intent(theContext, CallingActivityNew.class);
				i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				i.putExtra("incoming", false);
				i.putExtra("fromDialer", fromDialer);
				theContext.startActivity(i);
			} else {
				//add the call to the existing one
				CallingActivityNew.INSTANCE.addCall(false);
				
				//ensure the activity is on top of the screen
            	Intent i = new Intent(theContext, BringToFront.class);
            	i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            	i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
				theContext.startActivity(i);
			}
		
		if(currentCall != null) {
			//ensure the new call is not muted and not in speaker mode.
			currentCall.setSpeakerMode(false);
			if(currentCall.isMuted()) {
				currentCall.toggleMute();
            }
		}
		
		return currentCall;
	}

	/**
	 * Called when soundEffects is prepared and ready to be played.
	 */
	@Override
	public void onPrepared(MediaPlayer arg0) {
		arg0.start();
		Log.d(TAG, "Sound effect is playing!");
	}
}
