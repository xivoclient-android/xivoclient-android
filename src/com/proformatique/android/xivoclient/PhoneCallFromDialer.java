package com.proformatique.android.xivoclient;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.util.Log;

/**
 * This Receiver intercepts the user's phone calls, and decides
 * whether it is a XiVO call or a normal phone call.
 * (XiVO call = call to a contact that has been written by XiVO in the user's contacts)
 * <p/>
 * In case it is a XiVO call, it launches {@link BootToCall} to handle the call.
 */
public class PhoneCallFromDialer extends BroadcastReceiver {
	public static final String TAG = "Phone Call from Dialer";
	
	@Override
	public void onReceive(Context context, Intent intent) {
		Log.i("Phone Call from Dialer", "The user is making a phone call!");
		
		//gets the phone number to call
		String numToCall = getResultData();
		if(numToCall == null) numToCall = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
	
		boolean shouldUseXivo = false;
		
		//is it a XiVO contact?
		Cursor c = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI, 
				//projection (SELECT)
				new String[] { ContactsContract.Data.RAW_CONTACT_ID, 
				ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.Data.MIMETYPE }, 
				//selection (WHERE) -> should be a phone number, and the one specified
				ContactsContract.CommonDataKinds.Phone.NUMBER+"=? AND "+
				ContactsContract.Data.MIMETYPE+"=?", 
				//selection args
				new String[] { numToCall, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE }, 
				//order by -> never mind
				null);

		//if one of the requests has no results (the contact does not exist),
		//the shouldUseXivo will be false, and a normal call will be done.
		if(c.moveToFirst()) {
			//the user is in the contacts, let's look who created it.
			String rawId = c.getString(c.getColumnIndex(ContactsContract.Data.RAW_CONTACT_ID));
			Cursor c2 = context.getContentResolver().query(ContactsContract.RawContacts.CONTENT_URI, 
					new String[] { ContactsContract.RawContacts._ID, 
					ContactsContract.RawContacts.ACCOUNT_TYPE },
					ContactsContract.RawContacts._ID+"=?", new String[] { rawId }, null);
			if(c2.moveToFirst()) {
				if(c2.getString(c2.getColumnIndex(ContactsContract.RawContacts.ACCOUNT_TYPE))
						.equals("com.xivo.contacts")) //well, it's our contact. So make a XiVO call.
					shouldUseXivo = true;
			}
			c2.close();
		}
		c.close();
		
		if(shouldUseXivo) {
			Log.i("Phone Call from Dialer", "Well, let's use XiVO.");
			
			//launch the BootToCall activity which will do the call for us.
			Intent i = new Intent(context, BootToCall.class);
			i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			i.putExtra("numToCall", numToCall);
			context.startActivity(i);
		      
			//cancel the Android call that was going to be launched.
			setResultData(null);
		} else {
			Log.i("Phone Call from Dialer", "Just use the dialer this time!");
			//and let the ordered broadcast go
		}
	}
	
	
}
