package com.proformatique.android.xivoclient;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.Thread.UncaughtExceptionHandler;

import android.os.Environment;

/**
 * Exception handler that writes any uncaught exception to a "xivoclient_crashdumps" directory on your SD card.
 * It is not used although.
 */
public class FileDumpExceptionHandler implements UncaughtExceptionHandler {
    
    private static String path = "/sdcard/pf";
    private static String file = "stacktrace.txt";
    private UncaughtExceptionHandler dueh;
    
    /**
     * Creates the exception handler and initializes the variables
     */
    public FileDumpExceptionHandler() {
        dueh = Thread.getDefaultUncaughtExceptionHandler();
        //the path to sdcard is not always /sdcard... on my S2 it is /storage/extSdCard for instance
        path = Environment.getExternalStorageDirectory().getPath() + "/xivoclient_crashdumps";
    }
    
    /**
     * An exception is being thown... Write it.
     */
    @Override
    public void uncaughtException(Thread thread, Throwable ex) {
    	file = "stacktrace_"+System.currentTimeMillis()+".txt";

        //prints the stack trace to a String
        final Writer result = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(result);
        ex.printStackTrace(printWriter);
        String stacktrace = result.toString();
        printWriter.close();
        //write the stack trace
        writeToFile(stacktrace);
        //and retrow the exception
        dueh.uncaughtException(thread, ex);
    }
    
    /**
     * Write the String to the file.
     * @param stacktrace The String to write
     */
    private void writeToFile(String stacktrace) {
        File traceDirectory = new File(path);
        traceDirectory.mkdirs();
        File outputFile = new File(traceDirectory, file);
        try {
            outputFile.createNewFile();
            FileOutputStream fos = new FileOutputStream(outputFile);
            fos.write(stacktrace.getBytes());
            fos.flush();
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
