/* XiVO Client Android
 * Copyright (C) 2010-2011, Proformatique
 *
 * This file is part of XiVO Client Android.
 *
 * XiVO Client Android is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * XiVO Client Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with XiVO client Android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.proformatique.android.xivoclient.tools;

import org.xivo.cti.model.CallType;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.widget.ImageView;

import com.proformatique.android.xivoclient.R;

/**
 * Utilities to get colored icons from white ones, used to show phone/user statuses with the right colors.
 */
public class GraphicsManager {
	
	private static final String LOG_TAG = "GRAPHICS_MANAGER";

	/**
	 * Pick from the incoming, the outgoing and the missed call graphics
	 * @param direction the icon wanted
	 * @return the resource id of the corresponding drawable.
	 */
    public static int getCallIcon(String direction) {
        int iconId = 0;

        if (direction.equals(CallType.INBOUND.toString())) {
            iconId = R.drawable.new_call_in;
        } else if (direction.equals(CallType.OUTBOUND.toString())) {
            iconId = R.drawable.new_call_out;
        } else {
            iconId = R.drawable.new_call_miss;
        }

        return iconId;
    }

    /**
     * Change the color of an {@link ImageView} to this color
     * @param context (useless)
     * @param icon the icon to set the color
     * @param color the color to set
     */
	public static void setIconStateDisplay(Context context, ImageView icon, 
			String color) {
		  //conversion of bad color strings
		  if (color != null)
			  color = color.replaceFirst("grey", "gray");
		  icon.setColorFilter(null);
		  
		  if (color != null && !color.equals(""))
			  icon.setColorFilter(Color.parseColor(color), PorterDuff.Mode.MULTIPLY);
		  else
			  icon.setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.MULTIPLY);
	}

	/**
     * Change the color of an {@link ImageView} to this color
     * @param context (useless)
     * @param icon the icon to set the color
     * @param color the color to set
     */
	public static void setIconPhoneDisplay(Context context, ImageView icon, 
			String color) {
		setIconStateDisplay(context, icon, color);
	}
	
	/**
	 * Gets a Bitmap of the user status icon, with the right color, for the notification dropdown
	 * @param context the application context
	 * @param color the color to use
	 * @return the resulting {@link Bitmap}.
	 */
	public static Bitmap getIconStatusBitmap(Context context, String color) {		
		Log.d("Graphics Manager", "Wants the color "+color);
		Drawable icon = context.getResources().getDrawable(R.drawable.notif_status_wht);
		
		//Log.d( LOG_TAG, "Color Phone : "+ color);
		  /**
		   * Conversion of bad color strings
		   */
		if (color != null)
			color = color.replaceFirst("grey", "gray");
		icon.setColorFilter(null);
		
		try {
			if (color != null && !color.equals("")) {
				icon.setColorFilter(Color.parseColor(color), PorterDuff.Mode.SRC_ATOP);
				Log.d("Graphics Manager", "Set the color "+color);
			} else
				icon.setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.SRC_ATOP);
		} catch (IllegalArgumentException e) {
			Log.d(LOG_TAG, "Color: " + color + "\n" + e.toString());
			e.printStackTrace();
		}
		
		return drawableToBitmap(icon, color);
	}
	
	/**
	 * Gets a Bitmap of the phone status icon, with the right color, for the notification dropdown
	 * @param context the application context
	 * @param color the color to use
	 * @return the resulting {@link Bitmap}.
	 */
	public static Bitmap getIconPhoneBitmap(Context context, String color) {		
		Log.d("Graphics Manager", "Wants the color "+color);
		Drawable icon = context.getResources().getDrawable(R.drawable.notif_phone_wht);
		
		//Log.d( LOG_TAG, "Color Phone : "+ color);
		  /**
		   * Conversion of bad color strings
		   */
		if (color != null)
			color = color.replaceFirst("grey", "gray");
		icon.setColorFilter(null);
		
		try {
			if (color != null && !color.equals("")) {
				icon.setColorFilter(Color.parseColor(color), PorterDuff.Mode.SRC_ATOP);
				Log.d("Graphics Manager", "Set the color "+color);
			} else
				icon.setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.SRC_ATOP);
		} catch (IllegalArgumentException e) {
			Log.d(LOG_TAG, "Color: " + color + "\n" + e.toString());
			e.printStackTrace();
		}
		
		return drawableToBitmap(icon, color);
	}

	/**
	 * Converts a drawable to a bitmap
	 * @param drawable 
	 * @return
	 */
	private static Bitmap drawableToBitmap (Drawable drawable, String color) {
	    Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Config.ARGB_8888);
	    
	    Canvas canvas = new Canvas(bitmap); 
	    drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
	    drawable.draw(canvas);

	    return bitmap;
	}

}
