package com.proformatique.android.xivoclient.tools;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Builds JSON requests to send to the CTI server.
 * @deprecated We should use the Java CTI lib instead.
 */
public class JSONMessageFactory {
    
    /**
     * Cannot be instanciated
     */
    private JSONMessageFactory() {};
    
    /**
     * Asks the server to refresh the history.
     * @param astid
     * @param userId
     * @param mode
     * @param size
     * @return the JSONObject to send to the server.
     * @deprecated As of 2014-07-22 it does always work, but for how much time ... ?
     */
    public static JSONObject getJsonHistoRefresh(String astid, String userId, String mode, String size) {
        SimpleDateFormat sIso = new SimpleDateFormat("yyyy-MM-dd");
        Date dDay = new Date();
        Calendar c1 = new GregorianCalendar();
        c1.setTime(dDay);
        c1.add(Calendar.DAY_OF_MONTH, -30);
        
        JSONObject jObj = new JSONObject();
        try {
            jObj.accumulate("direction", Constants.XIVO_SERVER);
            jObj.accumulate("class","history");
            jObj.accumulate("peer", astid + "/" + userId);
            jObj.accumulate("size",size);
            jObj.accumulate("mode",mode);
            jObj.accumulate("morerecentthan",sIso.format(c1.getTime()));
        } catch (JSONException e) { }
        return jObj;
    }
    
    /**
     * <b>It does not work!</b>
     * @param type
     * @param src
     * @param dest
     * @return
     */
    public static JSONObject createJsonTransfer(String type, String src, String dest) {
        JSONObject jsonTransfer = new JSONObject();
        try {
            jsonTransfer.accumulate("direction", Constants.XIVO_SERVER);
            jsonTransfer.accumulate("class", type);
            jsonTransfer.accumulate("source", src);
            jsonTransfer.accumulate("destination", "ext:" + dest);
            return jsonTransfer;
        } catch (JSONException e) {
            return null;
        }
    }
}
