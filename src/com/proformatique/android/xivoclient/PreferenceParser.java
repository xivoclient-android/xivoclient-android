package com.proformatique.android.xivoclient;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

/**
 * This activity prompts the user to pick a file on launch, imports it in the settings, then quits.
 */
public class PreferenceParser extends ExiterActivity {
	private static final String TAG = "Preference Parser";

	/**
	 * Just launches the default file picker.
	 */
	@Override
	public void onCreate(Bundle b) {
		super.onCreate(b);
		openFile();
	}
	
	
	private boolean done = false;
	
	/**
	 * On the second time called (so, after returning from the file picker) close the activity.
	 */
	@Override
	public void onResume() {
		super.onResume();
		if(done) {
			finish();
			Log.d(TAG, "Activity finished");
		}
		done=true;
	}
	
	/**
	 * Pops up the file chooser by sending an Intent.
	 * A special intent is sent for Samsung devices, which do not react when receiving "normal" ones.
	 */
	public void openFile() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("file/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        // special intent for Samsung file manager
        Intent sIntent = new Intent("com.sec.android.app.myfiles.PICK_DATA");
        sIntent.addCategory(Intent.CATEGORY_DEFAULT);

        Intent chooserIntent;
        if (getPackageManager().resolveActivity(sIntent, 0) != null){
            // it is device with samsung file manager
            chooserIntent = Intent.createChooser(sIntent, "Open file");
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] { intent});
        }
        else {
        	//there is no Samsung file manager, so let's deal with the normal one.
            chooserIntent = Intent.createChooser(intent, "Open file");
        }

        try {
        	//launch the selected file chooser
            startActivityForResult(chooserIntent, 3);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getApplicationContext(), getString(R.string.file_error), Toast.LENGTH_SHORT).show();
        }
    }
	
	/**
	 * Retreives the URI of the file selected by the user.
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    switch (requestCode) {
	        case 3:
	        if (resultCode == RESULT_OK) {
	            // Get the Uri of the selected file 
	            Uri uri = data.getData();
	            Log.d(TAG, "File Uri: " + uri.toString());
	            // Get the path
	            String path = null;
				path = getPath(this, uri);
	            Log.d(TAG, "File Path: " + path);
	            
	            parsePreferences(path);
	        }
	        break;
	    }
	    super.onActivityResult(requestCode, resultCode, data);
	}
	
	

	/**
	 * Converts an URI to a file path.
	 * @param context The app context
	 * @param uri The file's URI
	 * @return The file's actual path
	 */
	public static String getPath(Context context, Uri uri) {
	    if ("content".equalsIgnoreCase(uri.getScheme())) {
	    	//convert the content:// URI to a file path.
	        String[] projection = { "_data" };
	        Cursor cursor = null;

	        try {
	            cursor = context.getContentResolver().query(uri, projection, null, null, null);
	            int column_index = cursor.getColumnIndexOrThrow("_data");
	            if (cursor.moveToFirst()) {
	                return cursor.getString(column_index);
	            }
	        } catch (Exception e) {
	            // Eat it
	        }
	        
	        if(cursor != null) cursor.close();
	    }
	    else if ("file".equalsIgnoreCase(uri.getScheme())) {
	        return uri.getPath();
	    }

	    return null;
	} 
	
	/**
	 * Gets the preferences from this file, and write them to the app's shared preferences.
	 * @param path Path to the file to import
	 */
	private void parsePreferences(String path) {
		try {
			BufferedReader bf = new BufferedReader(new FileReader(path));
			String s;
			boolean checkPassed = true;
			//check if the file seems to be valid (all lines are :
			//beginning with a #
			//OR empty
			//OR contain at least a space.)
			while((s = bf.readLine()) != null) {
				Log.i(TAG, s);
				if(!s.startsWith("#") && !s.contains(" ") && s.length() != 0) {
					checkPassed = false;
					break;
				}
			}
			bf.close();
			
			if(checkPassed) {
				//it seems to be valid, so import it 
				SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
				bf = new BufferedReader(new FileReader(path));
				while((s = bf.readLine()) != null) {
					//skip empty lines or commented ones
					if(s.length() == 0 || s.startsWith("#")) continue;
					
					//extract name and value
					String paramName = s.substring(0, s.indexOf(" "));
					String paramVal = s.substring(s.indexOf(" ") + 1);
					try {
						//is it a String?
						String info = prefs.getString(paramName, "none");
						//if nothing is set here, we should fall back to a boolean ;
						//false booleans are not set by default.
						if(info.equals("none")) throw new ClassCastException();
						
						Log.i(TAG, "Preference "+paramName+" is a String: "+info);
						SharedPreferences.Editor editor = prefs.edit();
                        editor.putString(paramName, paramVal);
                        editor.commit();
					} catch(ClassCastException cce) {
						try {
							//is it an int?
							int info = prefs.getInt(paramName, -4012);
							//if nothing is set here, we should fall back to a boolean ;
							//false booleans are not set by default.
							if(info == -4012) throw new ClassCastException();
							
							Log.i(TAG, "Preference "+paramName+" is an int: "+info);
							SharedPreferences.Editor editor = prefs.edit();
	                        editor.putInt(paramName, Integer.parseInt(paramVal));
	                        editor.commit();
						} catch(ClassCastException cce2) {
							//is it a boolean? (if ClassCastException is re-thrown here, then it is something
							//else than an int, String or boolean ; so write another block!)
							boolean info = prefs.getBoolean(paramName, false);
							Log.i(TAG, "Preference "+paramName+" is a boolean: "+info);
							SharedPreferences.Editor editor = prefs.edit();
	                        editor.putBoolean(paramName, Boolean.parseBoolean(paramVal));
	                        editor.commit();
						}
					}
				}
				bf.close();
				
				//the RESULT_OK indicates the SettingsActivity that settings have changed.
				setResult(RESULT_OK);
			} else {
	            Toast.makeText(getApplicationContext(), getString(R.string.file_bad), Toast.LENGTH_SHORT).show();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/*
	 * Here is a list of the preferences:
	 * server_adress - CTI server address
	 * server_port 	 - CTI server port
	 * login 		 - CTI login
	 * password  	 - CTI password
	 * context		 - CTI context
	 * 
	 * sip_server	 - SIP server
	 * sip_username  - SIP user name
	 * sip_password  - SIP password
	 * HOME_SSID	 - Voice SSID of the company
	 * 
	 * start_on_boot - Start XiVO connection service on boot
	 * always_connected - Keep the service connected
	 * 
	 * use_mobile_number - If it should use the default mobile number (false) or a custom one (true)
	 * mobile_number - The custom mobile number
	 * 
	 * use_fullscreen- If the notification bar should hide when opening XiVO Client
	 * preferredHome - Preferred home screen for the user: 1=dialer, 2=user list, 3=history, 4=services
	 * android_contacts - Show the Android contacts button on the user list (true) or not (false)
	 */
}
