/* XiVO Client Android
 * Copyright (C) 2010-2011, Proformatique
 *
 * This file is part of XiVO Client Android.
 *
 * XiVO Client Android is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * XiVO Client Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with XiVO client Android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.proformatique.android.xivoclient;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.database.Cursor;
import android.net.sip.SipException;
import android.os.Build;
import android.os.Bundle;
import android.os.RemoteException;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Toast;

import com.proformatique.android.xivoclient.dao.ShortcutProvider;
import com.proformatique.android.xivoclient.service.IXivoConnectionService;
import com.proformatique.android.xivoclient.sip.SipApiUser;
import com.proformatique.android.xivoclient.sync.ContactSyncSettings;
import com.proformatique.android.xivoclient.tools.AndroidTools;
import com.proformatique.android.xivoclient.tools.Constants;

/**
 * Activity used to manage all the app's settings.
 */
public class SettingsActivity extends PreferenceActivity implements OnSharedPreferenceChangeListener {
    
    private final static String TAG = "XiVO settings";
    
    //some (but just some) preference tags. For the full list see the end of PreferenceParser.
    private final static String USE_MOBILE_OPTION = "use_mobile_number";
    private final static String START_ON_BOOT = "start_on_boot";
    private final static String ALWAYS_CONNECTED = "always_connected";
    private final static boolean USE_MOBILE_DEFAULT = false;
    private static final String MOBILE_PHONE_NUMBER = "mobile_number";
    private static final String DEFAULT_MOBILE_PHONE_NUMBER = "";
    private static final String LAST_DIALER_VALUE = "last_dialer_value";
    
    SharedPreferences settingsPrefs;
    
    /**
     * Called when the activity is closed. You should restart the SIP API User
     * (in case the settings have changed).
     */
    @Override
    public void onPause() {
    	super.onPause();
    	
    	//Using a Thread prevents a lag when closing the settings.
    	Thread thread = new Thread("Reconnect To SIP") {
    		@Override public void run() {
    				//we trust the settings again now...
    				SipApiUser.badSettings = false;
    				
    				//get the connection service.
    				IXivoConnectionService conn = null;
    				if(Connection.INSTANCE != null && Connection.INSTANCE.getConnection(getApplicationContext()) != null)
    					conn = Connection.INSTANCE.getConnection(getApplicationContext());
    				
    				//close the previous SIP user
            		if(SipApiUser.INSTANCE != null)
            			SipApiUser.INSTANCE.closeConnection();
            		
            		//and open that one.
            		try {
    					if(settingsPrefs.getString("sip_server", "").length() != 0 && conn != null 
    							&& conn.getUsesSip() &&	!SipApiUser.badSettings)
    						try {
    							new SipApiUser(getApplicationContext(), settingsPrefs.getString("sip_server", ""), 
    								settingsPrefs.getString("sip_username", ""), settingsPrefs.getString("sip_password", ""));
    						} catch (ParseException e) {
    							e.printStackTrace();
    							SipApiUser.INSTANCE = null;
    						} catch (SipException e) {
    							e.printStackTrace();
    							SipApiUser.INSTANCE = null;
    						}
    				} catch (RemoteException e2) {
    					e2.printStackTrace();
    				}
    		}
    	}; thread.start();
    }
    
    /**
     * Fill the settings with default values if not set, create the options and register receivers.
     */
    @SuppressWarnings("deprecation")
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        
        super.onCreate(savedInstanceState);
        
        setTitle(getString(R.string.settings));
        
        settingsPrefs = PreferenceManager.getDefaultSharedPreferences(this);

        //sets the default values once.
        PreferenceManager.setDefaultValues(this.getApplicationContext(), R.xml.settings, false);
        
        //the solution to correct these "deprecated" warnings, would be to move to a
        //fragment-based PreferenceActivity. These methods are deprecated but we must use
        //them as our PreferenceActivity is not based on PreferenceFragments.
        //(http://developer.android.com/reference/android/preference/PreferenceActivity.html)
        addPreferencesFromResource(R.xml.settings);
        
        if (settingsPrefs.getBoolean("use_fullscreen", false)) {
            this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        
        /**
         * Init value for mobile number
         */
        if (settingsPrefs.getString("mobile_number", "").equals("")) {
            
            TelephonyManager tMgr = (TelephonyManager) getApplicationContext().getSystemService(
                    Context.TELEPHONY_SERVICE);
            String mobileNumber = tMgr.getLine1Number();
            SharedPreferences.Editor editor = settingsPrefs.edit();
            editor.putString("mobile_number", mobileNumber == null ? "" : mobileNumber);
            editor.commit();

            Log.i("SettingsActivity", "Just put the mobile number: "+settingsPrefs.getString("mobile_number", ""));
        }
        
        //creating a listener right here does not work, because the created object will be garbage-collected.
        //so we should let the activity implement OnSharedPreferenceChangeListener and register it.
        settingsPrefs.registerOnSharedPreferenceChangeListener(this);
        
        //set up the "open sync settings" options so that it launches the ContactSyncSettings
        findPreference("openSync").setOnPreferenceClickListener(new OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference preference) {
				Intent i = new Intent(SettingsActivity.this, ContactSyncSettings.class);
				i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(i);
				return true;
			}
		});
        
        //set up the "import settings" options so that it launches the PreferenceParser
        findPreference("import").setOnPreferenceClickListener(new OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference preference) {
				Intent i = new Intent(SettingsActivity.this, PreferenceParser.class);
				startActivityForResult(i, 4);
				return true;
			}
		});
        
        //action bar does not exist before Honeycomb (API 11)
        if(Build.VERSION.SDK_INT >= 11)
        	getActionBar().setDisplayHomeAsUpEnabled(true);

        //display the values of the settings as summaries
        final String[] valueList = new String[] { "server_adress", "server_port", "login", 
        		"context", "sip_server", "sip_username", "HOME_SSID", "mobile_number" };
        for(String value : valueList)
        	findPreference(value).setSummary(settingsPrefs.getString(value, ""));
        
        //indicate if there is something set in the password fields
        if(settingsPrefs.getString("password", "").length() != 0)
        	findPreference("password").setSummary("****");
        else
        	findPreference("password").setSummary("");
        if(settingsPrefs.getString("sip_password", "").length() != 0)
        	findPreference("sip_password").setSummary("****");
        else
        	findPreference("sip_password").setSummary("");
        
        //Set the preferred home summary
        String[] listOfScreens = getResources().getStringArray(R.array.pref_screenTitles);
        findPreference("preferredHome").setSummary(listOfScreens[
                                      Integer.parseInt(settingsPrefs.getString("preferredHome", "")) - 1]);
    }

    /**
     * Gets the result from the PreferenceParser.
     * If the result is OK, the settings have changed ; so close the activity, otherwise the old settings will be displayed.
     * (If the user wants to change other things, he will re-open the settings, and they will be up-to-date.
     * If there are no CTI settings set and the preferences file set them, the user will instantly get connected
     * after selecting the file).
     */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	if(requestCode == 4) {
    		if(resultCode == RESULT_OK) {
    			finish();
    		}
    	}
    }
    
    /**
     * You always use the mobile phone in this version of the app.
     * (When you are in the company, you don't use your desk phone anyway ; you use SIP)
     * 
     * @param context
     * @return true if use_mobile_number is set and a mobile number has been set else false
     * @throws NullPointerException if context is null
     */
    public static boolean getUseMobile(Context context) {
    	return true;
        /*SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getBoolean(USE_MOBILE_OPTION, USE_MOBILE_DEFAULT)
                && prefs.getString(MOBILE_PHONE_NUMBER, "").equals("") == false;*/
    }
    
    /**
     * Returns the mobile phone number or the default one if use_mobile_number is not true
     * 
     * @param context
     * @return
     */
    public static String getMobileNumber(Context context) {
    	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
    	
        if (prefs.getBoolean(USE_MOBILE_OPTION, USE_MOBILE_DEFAULT)) {
            return PreferenceManager.getDefaultSharedPreferences(context).getString(
                    MOBILE_PHONE_NUMBER, DEFAULT_MOBILE_PHONE_NUMBER);
        } else {
        	TelephonyManager tMgr = (TelephonyManager) context.getSystemService(
                    Context.TELEPHONY_SERVICE);
            String mobileNumber = tMgr.getLine1Number();
            return mobileNumber;
        }
    }
    
    /**
     * Returns the default context
     */
    public static String getXivoContext(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("context",
                Constants.XIVO_CONTEXT);
    }
    
    /**
     * Returns the Xivo login
     */
    public static String getLogin(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("login", "");
    }
    
    /**
     * The working state of this option has not been checked.
     * @param context
     * @return true if the service should be launched on boot.
     */
    public static boolean getStartOnBoot(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(START_ON_BOOT,
                false);
    }
    
    /**
     * The working state of this option has not been checked.
     * @param context
     * @return true if the service should always be connected
     */
    public static boolean getAlwaysConnected(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(ALWAYS_CONNECTED,
                false);
    }
    
    /**
     * Returns the list of shortcuts
     * 
     * @param context
     * @return
     */
    public static List<HashMap<String, ?>> getShortcuts(Context context) {
        Cursor c = context.getContentResolver().query(ShortcutProvider.CONTENT_URI, null, null,
                null, ShortcutProvider.NAME);
        int size = c != null ? c.getCount() : 0;
        if (size > 0) {
            List<HashMap<String, ?>> list = new ArrayList<HashMap<String, ?>>(size);
            c.moveToFirst();
            while (!c.isAfterLast()) {
                HashMap<String, Object> item = new HashMap<String, Object>();
                String packageName = c.getString(c.getColumnIndex(ShortcutProvider.PACKAGE));
                item.put("package", packageName);
                item.put("name", c.getString(c.getColumnIndex(ShortcutProvider.NAME)));
                item.put("icon", AndroidTools.getPackageIcon(context, packageName));
                list.add(item);
                c.moveToNext();
            }
            c.close();
            return list;
        }
        c.close();
        return null;
    }
    
    /**
     * Returns the number of shortcuts available
     * 
     * @param context
     * @return
     */
    public static int getNbShortcuts(Context context) {
        Cursor c = context.getContentResolver().query(ShortcutProvider.CONTENT_URI, null, null,
                null, null);
        int res = c != null ? c.getCount() : 0;
        c.close();
        return res;
    }
    
    /**
     * Adds a shortcut to ShortcutProvider for persistence
     * 
     * @param context
     * @param packageName
     */
    public static void addShortcut(Context context, String packageName) {
        Log.d(TAG, "Adding the following shortcut " + packageName);
        if (hasShortcut(context, packageName)) {
            Toast.makeText(context, context.getString(R.string.shortcut_already_present),
                    Toast.LENGTH_SHORT).show();
            return;
        }
        ContentValues values = new ContentValues();
        values.put(ShortcutProvider.NAME, AndroidTools.getPackageLabel(context, packageName));
        values.put(ShortcutProvider.PACKAGE, packageName);
        context.getContentResolver().insert(ShortcutProvider.CONTENT_URI, values);
        values.clear();
    }
    
    /**
     * Removes a shortcut from the ShortcutProvider
     * 
     * @param context
     * @param packageName
     */
    public static void removeShortcut(Context context, String packageName) {
        Log.d(TAG, "Deleting the following shortcut: " + packageName);
        context.getContentResolver().delete(ShortcutProvider.CONTENT_URI,
                ShortcutProvider.PACKAGE + " = '" + packageName + "'", null);
    }
    
    /**
     * Check if the ShortcutProvider already contains a given package
     * 
     * @param context
     * @param packageName
     * @return
     */
    private static boolean hasShortcut(Context context, String packageName) {
        Cursor c = context.getContentResolver().query(ShortcutProvider.CONTENT_URI, null,
                ShortcutProvider.PACKAGE + " = '" + packageName + "'", null, null);
        boolean res = c.getCount() > 0;
        c.close();
        return res;
    }
    
    /**
     * Saves the last number that was on the dialer to restore it on the next
     * onResume
     * 
     * @param context
     * @param number
     */
    public static void setLastDialerValue(Context context, String number) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(LAST_DIALER_VALUE,
                number).commit();
    }
    
    /**
     * Retrieves the last dialer phone number that was present on the
     * phoneNumber field of the dialer when onPause was called
     * 
     * @param context
     * @return number
     */
    public static String getLastDialerValue(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(LAST_DIALER_VALUE,
                "");
    }
    
    /**
     * This Listener will trigger when users disable the "save_login"
     * parameter, so the app can erase previously saved login and password
     * ... but this setting is not written anywhere!
     * <p/>
     * Also updates the summary of a changed shared preference.
     */
    @Override
    @SuppressWarnings("deprecation")
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
            String key) {
     	Log.d("Settings Activity", "Changed the preference "+key+"!");
      	
     	final String[] valueList = new String[] { "server_adress", "server_port", "login", 
        		"context", "sip_server", "sip_username", "HOME_SSID", "mobile_number" };
        for(String value : valueList)
        	if(value.equals(key))
        		findPreference(value).setSummary(sharedPreferences.getString(value, ""));
        
        if(key.equals("preferredHome")) {
        	//Set the preferred home summary
            String[] listOfScreens = getResources().getStringArray(R.array.pref_screenTitles);
            findPreference("preferredHome").setSummary(listOfScreens[
                                          Integer.parseInt(settingsPrefs.getString("preferredHome", "")) - 1]);
        }
        
        //indicate if there is something set in the password fields
        if(key.equals("password"))
	        if(settingsPrefs.getString("password", "").length() != 0)
	        	findPreference("password").setSummary("****");
	        else
	        	findPreference("password").setSummary("");
        if(key.equals("sip_password"))
	        if(settingsPrefs.getString("sip_password", "").length() != 0)
	        	findPreference("sip_password").setSummary("****");
	        else
	        	findPreference("sip_password").setSummary("");
        
        if (key.equals("save_login")) {
            Boolean saveLogin = sharedPreferences.getBoolean(key, true);
           
            if (!saveLogin) {
                 
                SharedPreferences loginSettings;
                loginSettings = getSharedPreferences("login_settings", 0);
                 
                SharedPreferences.Editor editor = loginSettings.edit();
                 
                editor.putString("login", "");
                editor.putString("password", "");
                editor.commit();
                 
            }
        }
        Intent prefChanged = new Intent();
        prefChanged.setAction(Constants.ACTION_SETTINGS_CHANGE);
        sendBroadcast(prefChanged);
    }
    
    /**
     * Closes the activity when the top-left button of the settings is clicked.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem mi) {
    	finish();
    	return true;
    }
}
