package com.proformatique.android.xivoclient;

import java.util.List;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.RemoteException;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Adapter used for the navigation drawer. It justs displays text with an icon in front of it.
 */
public class PictureAndTextAdapter extends BaseAdapter {
	
	private XivoActivity activity;
	private List<Integer> pictureIds;
	private List<String> titles;
    
    private static LayoutInflater inflater=null;

	public PictureAndTextAdapter() {

	}
	
	/**
	 * Creates a new Adapter.
	 * @param a The XivoActivity hosting the navigation drawer
	 * @param t The titles list
	 * @param pics The pictures list associated with the titles
	 */
	public PictureAndTextAdapter(XivoActivity a, List<String> t, List<Integer> pics) {
        activity = a;
        pictureIds=pics;
        titles = t;
        if(t.size() != pics.size())
        	throw new IllegalArgumentException("t.size() != pics.size()");
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

	/**
	 * Get the element count
	 */
	@Override
	public int getCount() {
		return titles.size();
	}

	/**
	 * Gets an item
	 */
	@Override
	public Object getItem(int arg0) {
		return arg0;
	}

	/**
	 * Returns the same number
	 */
	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	/**
	 * Gets a View with the given id
	 */
	@Override
	public View getView(int position, View arg1, ViewGroup arg2) {
		View vi; 
        ImageView theImage = null;
        TextView theText = null;
        
        if(position == 0) {
        	//here is the app title
        	
        	//default layout
    		vi = inflater.inflate(R.layout.imageandtext, null);
            theImage = (ImageView) vi.findViewById(R.id.theImage);
            theText = (TextView) vi.findViewById(R.id.theText);
            theImage.setImageResource(pictureIds.get(position));
            theText.setText(titles.get(position));
        	//make the text large and bold
            int screenSize = (activity.getResources().getConfiguration().screenLayout & 
            	    Configuration.SCREENLAYOUT_SIZE_MASK);
            if(screenSize == Configuration.SCREENLAYOUT_SIZE_LARGE ||
               screenSize == Configuration.SCREENLAYOUT_SIZE_XLARGE) {
            	//don't apply this text appearance, because text size was changed.
            } else
            	   theText.setTextAppearance(activity.getApplicationContext(), android.R.style.TextAppearance_Large);
        	theText.setTypeface(theText.getTypeface(), Typeface.BOLD);
        } else if(position == XivoActivity.MAX_INDEX + 2 || position == 1) {
        	//it is just a separator
        	vi = inflater.inflate(R.layout.imageandtext_separator, null);
        } else if(position <= XivoActivity.MAX_INDEX + 1) {
        	//an option corresponding to one of the main screens (that also have the nav drawer).
        	
        	//default layout
    		vi = inflater.inflate(R.layout.imageandtext, null);
            theImage = (ImageView) vi.findViewById(R.id.theImage);
            theText = (TextView) vi.findViewById(R.id.theText);
            theImage.setImageResource(pictureIds.get(position));
            theText.setText(titles.get(position));
            
        	//make the text "medium"
            int screenSize = (activity.getResources().getConfiguration().screenLayout & 
            	    Configuration.SCREENLAYOUT_SIZE_MASK);
            if(screenSize == Configuration.SCREENLAYOUT_SIZE_LARGE ||
               screenSize == Configuration.SCREENLAYOUT_SIZE_XLARGE) {
            	//don't apply this text appearance, because text size was changed.
            } else
            	   theText.setTextAppearance(activity.getApplicationContext(), android.R.style.TextAppearance_Medium);
        } else {
        	//other options, like Settings and About, that will be smaller in the navigation drawer.
        	
        	//change the layout!
        	vi = inflater.inflate(R.layout.imageandtext_smaller, null);
        	theImage = (ImageView) vi.findViewById(R.id.theImage);
            theImage.setImageResource(pictureIds.get(position));
            theText = (TextView) vi.findViewById(R.id.theText);
            theText.setText(titles.get(position));
        }
        
        if(titles.get(position).equals(activity.getString(R.string.connect))) {
        	//it is the "connect" or "disonnect" item.
        	//so decide if we should display "connect" or "disconnect"!
        	Log.i("Picture and Text Adapter", "Setting up connected/disconnected label");
        	try {
        		//update the icon and the text depending on the result
                if (activity.isXivoServiceRunning() && activity.getXivoConnectionService() != null
                        && activity.getXivoConnectionService().isAuthenticated()) {
                	//we are connected -> the button will "disconnect"
                    theText.setText(R.string.disconnect);
                    theImage.setImageResource(R.drawable.drawer_disconnect);
                } else {
                	//we are not connected -> the button will "connect"
                	theText.setText(R.string.connect);
                    theImage.setImageResource(R.drawable.drawer_connect);
                }
            } catch (RemoteException e) {
            } catch (NullPointerException e) {
            	//well, we will never know...
            	Log.w("Picture and Text Adapter", "Opening menu and NullPointerException!");
            	theText.setText("...");
            }
        }
        
        return vi;
	}

}
