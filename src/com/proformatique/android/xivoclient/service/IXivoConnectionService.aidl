package com.proformatique.android.xivoclient.service;

interface IXivoConnectionService {
    int connect();
    int disconnect();
    boolean isConnected();
    int authenticate();
    boolean isAuthenticated();
    void loadData();
    void refreshHistory();
    void refreshServices();
    long getReceivedBytes();
    boolean hasNewVoiceMail();
    long getStateId();
    String getPhoneStatusColor();
    String getPhoneStatusLongname();
    String getFullname();
    void setFullname(String fullname);
    int call(String number);
    boolean isOnThePhone();
    void setState(String stateId);
    void sendFeature(String feature, String value, String phone);
    boolean hasChannels();
    void hangup();
    void transfer(String number);
    void atxfer(String number);
    boolean killDialer();
    String getAstId();
    String getXivoId();
    boolean getUsesSip();
    boolean getUsesSipNumb(String dialedNumber);
    String getUserId();
    boolean isFavorite(int userId);
    void addFavorite(int userId, String phoneState, String userState, String userName);
    void removeFavorite(int userId);
    void decrementContactsToGo();
    String getSSID();
    void disconnectRightAway();
}