package com.proformatique.android.xivoclient.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xivo.cti.MessageDispatcher;
import org.xivo.cti.MessageFactory;
import org.xivo.cti.MessageParser;
import org.xivo.cti.listener.ChannelUpdateListener;
import org.xivo.cti.listener.IdsListener;
import org.xivo.cti.message.AgentIds;
import org.xivo.cti.message.CallHistoryReply;
import org.xivo.cti.message.ChannelStatus;
import org.xivo.cti.message.CtiResponseMessage;
import org.xivo.cti.message.LoginCapasAck;
import org.xivo.cti.message.LoginIdAck;
import org.xivo.cti.message.LoginPassAck;
import org.xivo.cti.message.PhoneConfigUpdate;
import org.xivo.cti.message.PhoneIdsList;
import org.xivo.cti.message.PhoneStatusUpdate;
import org.xivo.cti.message.QueueIds;
import org.xivo.cti.message.QueueMemberIds;
import org.xivo.cti.message.UserConfigUpdate;
import org.xivo.cti.message.UserIdsList;
import org.xivo.cti.message.UserStatusUpdate;
import org.xivo.cti.model.Endpoint;
import org.xivo.cti.model.Endpoint.EndpointType;
import org.xivo.cti.model.UserStatus;
import org.xivo.cti.model.Xlet;
import org.xivo.cti.network.XiVOLink;

import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ReceiverCallNotAllowedException;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.sip.SipAudioCall;
import android.net.sip.SipManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Toast;

import com.proformatique.android.xivoclient.BringToFront;
import com.proformatique.android.xivoclient.R;
import com.proformatique.android.xivoclient.SettingsActivity;
import com.proformatique.android.xivoclient.XivoNotification;
import com.proformatique.android.xivoclient.dao.CapapresenceProvider;
import com.proformatique.android.xivoclient.dao.CapaxletsProvider;
import com.proformatique.android.xivoclient.dao.HistoryProvider;
import com.proformatique.android.xivoclient.dao.UserProvider;
import com.proformatique.android.xivoclient.sip.CallingActivityNew;
import com.proformatique.android.xivoclient.sip.SipApiUser;
import com.proformatique.android.xivoclient.tools.Constants;
import com.proformatique.android.xivoclient.tools.JSONMessageFactory;
import com.proformatique.android.xivoclient.xlets.XletDialer;

/**
 * The Service managing all the communications between the app and the CTI server.
 */
public class XivoConnectionService extends Service implements IdsListener, XiVOLink, 
	ChannelUpdateListener {
	//TODO be able to set this variable without changing the code.
	/**
	 * If this is set to false, we should not use callback in ANY case.
	 */
	private boolean ALLOW_CALLBACK = true;
	
	/**
	 * This counter is incremented when a contact/phone is asked to the system, and decremented when it was
	 * finally got. So when this counter reaches zero, we know that loading is finished.
	 */
	private int contactsToGo = 0;
	/**
	 * The total number of contacts to load.
	 */
	private int contactsTotal = 0;
	/**
	 * The number of contacts currently loaded.
	 */
	private int contactsLoaded = 0;
	
	/**
	 * Whenever the SSID of the WiFi network changes, it is stored here.
	 */
	private String lastKnownSSID = "none";

    private static final String TAG = "XiVO connection service";

    private SharedPreferences prefs = null;
    private Socket networkConnection = null;
    private BufferedReader inputBuffer = null;
    private Handler handler = null;
    /**
     * This thread will send messages to the Handler when received from the server.
     */
    private Thread thread = null;
    private long bytesReceived = 0L;
    private boolean cancel = false;
    private XivoNotification xivoNotif;
    private IntentReceiver receiver = null;
    private DisconnectTask disconnectTask = null;
    
    /**
     * The current favorite list.
     */
    private FavoriteList favList;
    
    /**
     * Class representing a single favorite, containing all its attributes: an ID, a name, a phone status and a user
     * status.
     */
    public class Favorite {
    	public String phoneState, userState, userName;
    	public int userId;
    	
    	/**
    	 * Used to add all the favorites on boot.
    	 * All the other information will be added during the boot.
    	 * @param userId The user's ID
    	 */
    	public Favorite(int userId) {
    		this.userId = userId;
    	}
    	
    	/**
    	 * Used to add a favorite "on the fly" as we already know all its info.
    	 * @param userId The user's ID
    	 * @param ps The user's phone state
    	 * @param us The user's state
    	 * @param un The user's name.
    	 */
    	public Favorite(int userId, String ps, String us, String un) {
    		this.userId = userId;
    		this.userState = us;
    		this.userName = un;
    		this.phoneState = ps;
    	}
    }
    
    /**
     * Class representing a list of {@link Favorite}s.
     * It is based on an ArrayList, but contains some utility methods like getting a favorite by ID,
     * and auto-saves a list of favorite IDs when the list changes.
     */
    public class FavoriteList {
    	ArrayList<Favorite> favList;
    	
    	/**
    	 * Creates an empty favorite list
    	 */
    	public FavoriteList() {
    		favList = new ArrayList<Favorite>();
    	}
    	
    	/**
    	 * Adds a favorite to this list.
    	 * @param f
    	 */
    	public void add(Favorite f) {
    		favList.add(f);
    		if(f.phoneState != null) writeFavorites();
    	}
    	
    	/**
    	 * Gets a Favorite based on his ID.
    	 * @param byId
    	 * @return null if the user is not in that list.
    	 */
    	public Favorite getFavorite(int byId) {
    		for(int i = 0; i < favList.size(); i++) {
    			Favorite f = favList.get(i);
    			if(f.userId == byId)
    				return f;
    		}
    		return null;
    	}
    	
    	/**
    	 * Remove the user with this ID from the list.
    	 * @param userId
    	 */
    	public void remove(int userId) {
    		for(int i = 0; i < favList.size(); i++) {
    			Favorite f = favList.get(i);
    			if(f.userId == userId)
    				favList.remove(f);
    		}
    		writeFavorites();
    	}
    	
    	private void writeFavorites() {
    		try {
				BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
						openFileOutput("favorite_ids", 0)));
				for(int i = 0; i < favList.size(); i++) {
	    			Favorite f = favList.get(i);
					bw.write(f.userId+"\n");
				}
				bw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
    		
    		int count = 0;
        	for(int i = 0; i < favList.size(); i++) {
        		if(favList.get(i).userId > 0) count++;
        	}
        	
        	Favorite[] tab = new Favorite[count];
        	//refresh the notification
        	int j = 0;
        	for(int i = 0; i < favList.size(); i++) {
    			Favorite fav = favList.get(i);
    			if(fav.userId > 0) {
    				tab[j] = fav;
    				j++;
    			}
        	}
        	xivoNotif.createNotification(tab);
    	}
    }

    // Informations that is relevant to a specific connection
    private String sessionId = null;
    private String astId = "xivo";
    private String userId = null;
    private String fullname = null;
    private final int[] mwi = new int[3];
    private int capaId = 0;
    private String thisChannel = null;
    private String peerChannel = null;
    private String oldChannel = null;
    private boolean wrongHostPort = false;
    private boolean connecting = false;
    private boolean authenticated = false;
    private boolean authenticating = false;

    private MessageParser messageParser;
    private MessageFactory messageFactory;
    private MessageDispatcher messageDispatcher;
    private UserUpdateManager userUpdateManager;
    private CallHistoryManager callHistoryManager;
    

    /**
     * Messages to return from the main loop to the handler
     */
    public enum Messages {
        NO_MESSAGE(0), NO_CLASS(1), DISCONNECT(2), UNKNOWN(3), JSON_EXCEPTION(4), USERS_LIST_COMPLETE(5), PHONES_LOADED(
                6), PRESENCE_UPDATE(7), HISTORY_LOADED(8), FEATURES_LOADED(9);

        private int id;
        private static final Map<Integer, Messages> lookup = new HashMap<Integer, Messages>();
        static {
            for (Messages m : EnumSet.allOf(Messages.class)) {
                lookup.put(m.getId(), m);
            }
        }

        public int getId() {
            return id;
        }

        private Messages(int id) {
            this.id = id;
        }

        public static Messages get(int id) {
            return lookup.get(id);
        }
    }

    public XivoConnectionService() {
    }

    /**
     * Adds all the listeners to the {@link MessageDispatcher} so that they receive the right events.
     */
    private void addDispatchers() {
        messageDispatcher.addListener(UserStatusUpdate.class, userUpdateManager);
        messageDispatcher.addListener(UserConfigUpdate.class, userUpdateManager);
        messageDispatcher.addListener(PhoneConfigUpdate.class, userUpdateManager);
        messageDispatcher.addListener(PhoneStatusUpdate.class, userUpdateManager);
        messageDispatcher.addListener(CallHistoryReply.class, callHistoryManager);
        messageDispatcher.addListener(UserIdsList.class, this);
        messageDispatcher.addListener(ChannelStatus.class, this);
    }
    
   
    

    /**
     * Implementation of the methods between the service and the activities
     */
    private final IXivoConnectionService.Stub binder = new IXivoConnectionService.Stub() {
    	/**
    	 * Disconnect in the same thread (not using a parallel task).
    	 */
    	@Override
    	public void disconnectRightAway() {
            Log.d(TAG, "Disconnecting");
            if (xivoNotif != null)
                xivoNotif.removeNotif();
            
            stopThread();
            connectionCleanup();
            resetState();
        }

    	/**
    	 * Connects to the CTI server
    	 */
        @Override
        public int connect() throws RemoteException {
            return XivoConnectionService.this.connect();
        }

        /**
         * Disconnect from the CTI server
         */
        @Override
        public int disconnect() throws RemoteException {
            return disconnectFromServer();
        }

        /**
         * Get if the service is connected to the server
         */
        @Override
        public boolean isConnected() throws RemoteException {
            return (networkConnection != null && networkConnection.isConnected() && inputBuffer != null && (disconnectTask == null || disconnectTask
                    .getStatus() != AsyncTask.Status.RUNNING));
        }

        /**
         * Authenticate with the CTI server.
         */
        @Override
        public int authenticate() throws RemoteException {
            return XivoConnectionService.this.authenticate();
        }

        /**
         * Get if the user is authenticated with the CTI server
         */
        @Override
        public boolean isAuthenticated() throws RemoteException {
            return authenticated && this.isConnected();
        }

        /**
         * Asks the server to refresh the features, the history and the users.
         */
        @Override
        public void loadData() throws RemoteException {
            getContentResolver().delete(UserProvider.CONTENT_URI, null, null);
            XivoConnectionService.this.refreshFeatures();
            XivoConnectionService.this.refreshHistory();
            XivoConnectionService.this.loadList("users");
        }

        /**
         * Get the number of bytes received by the app from the server.
         */
        @Override
        public long getReceivedBytes() throws RemoteException {
            return bytesReceived;
        }

        /**
         * Get if the user has voice mail (untested)
         */
        @Override
        public boolean hasNewVoiceMail() throws RemoteException {
            return mwi != null ? mwi[0] == 1 : false;
        }
        
        @Override
        public long getStateId() throws RemoteException {
            return userUpdateManager.getStateId();
        }

        /**
         * Get the color associated with the user's phone status
         */
        @Override
        public String getPhoneStatusColor() throws RemoteException {
            return userUpdateManager.getPhoneStatusColor();
        }

        /**
         * Get the user's phone status in text
         */
        @Override
        public String getPhoneStatusLongname() throws RemoteException {
            return userUpdateManager.getPhoneStatusLongName();
        }

        /**
         * Get the user's fullname (or "User" if it is unknown)
         */
        @Override
        public String getFullname() throws RemoteException {
            return fullname == null ? getString(R.string.user_identity) : fullname;
        }

        /**
         * Calls a telephone number using the Callback method.
         */
        @Override
        public int call(String number) throws RemoteException {
            return XivoConnectionService.this.call(number);
        }

        /**
         * See if the user is currently calling.
         * (returns always false to disable the non-working transfer options when navigating during a call)
         */
        @Override
        public boolean isOnThePhone() throws RemoteException {
        	return false;
        	/* if (SettingsActivity.getUseMobile(XivoConnectionService.this)) {
                return XivoConnectionService.this.isMobileOffHook();
            } else {
                return thisChannel != null;
            }*/
        }

        /**
         * Changes the user's status.
         */
        @Override
        public void setState(String stateId) throws RemoteException {
            XivoConnectionService.this.setState(stateId);
        }

        /**
         * Make a change in the features.
         */
        @Override
        public void sendFeature(String feature, String value, String phone) throws RemoteException {
            XivoConnectionService.this.sendFeature(feature, value, phone);
        }

        /**
         * Checks that the channels are non-null
         */
        @Override
        public boolean hasChannels() throws RemoteException {
            return thisChannel != null && peerChannel != null;
        }

        /**
         * Hang up the current call using CTI
         */
        @Override
        public void hangup() throws RemoteException {
            XivoConnectionService.this.hangup();
        }

        /**
         * Attended transfer using CTI (does not work)
         */
        @Override
        public void atxfer(String number) throws RemoteException {
            XivoConnectionService.this.atxfer(number);
        }

        /**
         * Blind transfer using CTI (does not work)
         */
        @Override
        public void transfer(String number) throws RemoteException {
            XivoConnectionService.this.transfer(number);
        }
        
        /**
         * Checks if the InCallScreenKiller should kill the dialer.
         */
        @Override
        public boolean killDialer() throws RemoteException {
            return XletDialer.autoPickup;
        }

        /**
         * Get the server ID.
         */
        @Override
        public String getAstId() throws RemoteException {
            return XivoConnectionService.this.astId;
        }

        /**
         * Get the current user's ID.
         */
        @Override
        public String getXivoId() throws RemoteException {
            return XivoConnectionService.this.userId;
        }

        /**
         * Submit a new fullname for the user to the service.
         */
        @Override
        public void setFullname(String fullname) throws RemoteException {
            XivoConnectionService.this.fullname = fullname;
        }
        
        /**
         * Asks the server to refresh the history.
         */
		@Override
		public void refreshHistory() throws RemoteException {
			XivoConnectionService.this.refreshHistory();
		}

		/**
		 * returns true if we should connect and get registered to SIP.
		 */
		@Override
		public boolean getUsesSip() {
			return XivoConnectionService.this.getUsesSip();
		}
		
		/**
		 * Same as getXivoId... returns the user ID
		 */
		@Override
		public String getUserId() {
			return userId;
		}
		
		/**
		 * Asks the server to refresh the features.
		 */
		@Override
		public void refreshServices() {
			refreshFeatures();
		}
		
		/**
		 * Get if the given user id is in the favorites
		 */
		@Override
		public boolean isFavorite(int userId) {
			boolean b = favList.getFavorite(userId) != null;
			Log.v(TAG, userId+(b ? " is " : " is NOT ")+"a favourite");
			return b;
		}

		/**
		 * Add a new favorite
		 */
		@Override
		public void addFavorite(int userId, String phoneState,
				String userState, String userName) throws RemoteException {
			favList.add(new Favorite(userId, phoneState, userState, userName));
		}
		
		/**
		 * Remove the favorite with the given user id
		 */
		@Override
		public void removeFavorite(int userId) throws RemoteException {
			favList.remove(userId);
		}

		/**
		 * Returns true if we should use SIP for this call.
		 * (if SIP is disconnected and this returns true, an error message will display)
		 */
		@Override
		public boolean getUsesSipNumb(String dialedNumber)
				throws RemoteException {
			boolean isOK = false;
			if(prefs.getString("HOME_SSID", "").length() == 0) {
				if(prefs.getString("HOME_SSID", "").length() == 0) {
					//No SSID is set, but are there SIP settings here ?
					if(prefs.getString("sip_server", "").length() != 0
							&& prefs.getString("sip_username", "").length() != 0
							&& prefs.getString("sip_password", "").length() != 0) {
						//yes, there are.
						//so, make no exception. If SIP is supported, this returns true.
						isOK = SipManager.isApiSupported(XivoConnectionService.this) 
								&& SipManager.isVoipSupported(XivoConnectionService.this);
					} else {
						//there is no SIP set, so return false (we will use callback)
						return false;
					}
				}
			} else
				isOK= lastKnownSSID.equals(prefs.getString("HOME_SSID", ""))
					&& SipManager.isApiSupported(XivoConnectionService.this) 
					&& SipManager.isVoipSupported(XivoConnectionService.this);
			Log.i(TAG, "SIP dialing with "+lastKnownSSID+" and "+prefs.getString("HOME_SSID", "")+" is "+(isOK ? "authorized" : "forbidden"));
			return isOK;
		}

		/**
		 * Get the company's SSID (where SIP is allowed)
		 */
		@Override
		public String getSSID() throws RemoteException {
			return prefs.getString("HOME_SSID", "");
		}

		/**
		 * Inform the service than one more contact has been loaded.
		 */
		@Override
		public void decrementContactsToGo() throws RemoteException {
			XivoConnectionService.this.decrementContactsToGo();
		}
    };
    
    /**
	 * returns true if we should connect and get registered to SIP.
	 */
    private boolean getUsesSip() {
	    //isApiSupported and isVoipSupported checks if the phone HAS the capabilities
		//of using SIP.
		boolean isOK = lastKnownSSID.equals(prefs.getString("HOME_SSID", ""))
				&& SipManager.isApiSupported(XivoConnectionService.this) 
				&& SipManager.isVoipSupported(XivoConnectionService.this)
				&& prefs.getString("sip_server", "").length() != 0
			    && prefs.getString("sip_username", "").length() != 0
				&& prefs.getString("sip_password", "").length() != 0;
		Log.i(TAG, "SIP connection with "+lastKnownSSID+" and "+prefs.getString("HOME_SSID", "")+" is "+(isOK ? "authorized" : "forbidden"));
		return isOK;
    }

    /**
     * Check the phone status returns true if it's off hook
     * 
     * @return
     *//*
    private boolean isMobileOffHook() {
        switch (((TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE)).getCallState()) {
        case TelephonyManager.CALL_STATE_OFFHOOK:
        case TelephonyManager.CALL_STATE_RINGING:
            return true;
        default:
            return false;
        }
    }*/

    /**
     * Blind transfer using CTI (does not work)
     * @param number The destination phone number
     */
    private void transfer(String number) {
        Log.d(TAG, "Blind transfer to " + number);
        if (this.peerChannel != null) {
            String src = "chan:" + astId + "/" + userId + ":" + this.peerChannel;
            sendLine(JSONMessageFactory.createJsonTransfer("transfer", src, number).toString());
        } else {
            Log.d(TAG, "Cannot transfer from a null channel");
        }
    }

    /**
     * Attended transfer using CTI (does not work)
     * @param number The destination phone number
     */
    private void atxfer(String number) {
        if (this.thisChannel != null) {
            String src = "chan:" + astId + "/" + userId + ":" + this.thisChannel;
            sendLine(JSONMessageFactory.createJsonTransfer("atxfer", src, number).toString());
        } else {
            Log.d(TAG, "Cannot atxfer from a null. this = " + thisChannel);
        }
    }


    /**
     * Hang-up the current call if AndroidTools.hangup did not work 
	 * It will hang up our channel, or if it does not exist, the peer channel.
     */
    private void hangup() {
    	
        String channel = null;
        if (thisChannel != null) {
            channel = "chan:" + astId + "/" + thisChannel;
        } else if (peerChannel != null) {
            channel = "chan:" + astId + "/" + peerChannel;
        } else if (oldChannel != null) {
            channel = "chan:" + astId + "/" + oldChannel;
        } else {
            Log.d(TAG, "Can't hang-up from the service");
            return;
        }
        //sendLine(JSONMessageFactory.createJsonHangupObject(this, channel).toString());
    	JSONObject object = messageFactory.createHangup();
    	//there is something missing in this JSON request : the channel!
    	try {
			object.put("channelids", channel);
			object.put("command", object.getString("class"));
			object.put("class", "ipbxcommand");
			sendLine(object.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}
        resetChannels();
    }

    /**
     * Change a feature value
     * 
     * @param feature The concerned feature (enablednd, enablerna, enablebusy, enableunc)
     * @param value 0 if disabled, 1 when enabled
     * @param phone The destination number for forwards, unused for DND
     */
    private void sendFeature(String feature, String value, String phone) {
    	if(feature.equals("enablednd"))
    		sendLine(messageFactory.createDND(value.equals("1")).toString());
    	else if(feature.equals("enablerna"))
    		sendLine(messageFactory.createNaForward(phone, value.equals("1")).toString());
    	else if(feature.equals("enablebusy"))
    		sendLine(messageFactory.createBusyForward(phone, value.equals("1")).toString());
    	else if(feature.equals("enableunc"))
    		sendLine(messageFactory.createUnconditionnalForward(phone, value.equals("1")).toString());
    }

    /**
     * Sends an history request to the CTI server
     */
    private void refreshHistory() {
        getContentResolver().delete(HistoryProvider.CONTENT_URI, null, null);
        sendLine(JSONMessageFactory.getJsonHistoRefresh(astId, userId, "0", "10").toString());
        sendLine(JSONMessageFactory.getJsonHistoRefresh(astId, userId, "1", "10").toString());
        sendLine(JSONMessageFactory.getJsonHistoRefresh(astId, userId, "2", "10").toString());
    }

    /**
     * Set a new status to the user
     */
    private void setState(String stateId) {
        sendLine(messageFactory.createUserAvailState(userId, stateId).toString());
        //sendLine(JSONMessageFactory.getJsonState(stateId).toString());
    }

    /**
     * Initiate a call
     * 
     * @param number
     * @return OK or CONNECTION_REFUSED if Callback is not allowed by the configuration
     */
    private int call(String number) {
    	if(!ALLOW_CALLBACK) {
    		return Constants.CONNECTION_REFUSED;
    	}
        Log.d(TAG, "Calling " + number);
        String mobile = SettingsActivity.getMobileNumber(getApplicationContext());
        if(mobile == null) {
	        TelephonyManager tMgr = (TelephonyManager) getApplicationContext().getSystemService(
	                Context.TELEPHONY_SERVICE);
	        mobile = tMgr.getLine1Number();
        }
        
        Endpoint caller = new Endpoint(EndpointType.EXTEN, mobile);
        Endpoint called = new Endpoint(EndpointType.EXTEN, number);
        JSONObject jsonOriginate = messageFactory.createOriginate(caller, called);
        sendMessage(jsonOriginate);
        //sendMessage(messageFactory.createDial(called));
        return Constants.OK;
    }
    

    /**
     * Asks the CTI server for a list
     */
    private void loadList(String list) {
        if (thread == null)
            startLooping(getApplicationContext());
        JSONObject query = messageFactory.createGetUsersList();
        sendMessage(query);
        try {
        	query = messageFactory.createGetUsersList();
			query.put("listname", "meetmes");
	        sendMessage(query);
		} catch (JSONException e) {
			e.printStackTrace();
		}
    }
    
    private NetStateReceiver netReceiver;
    private IncomingCallReceiver incReceiver;
    private MyPhoneStateListener phoneReceiver;

    /**
     * Registers all the receivers, loads the favorites and inits the variables
     */
    @Override
    public void onCreate() {
        super.onCreate();
        receiver = new IntentReceiver();
        incReceiver = new IncomingCallReceiver();
        netReceiver = new NetStateReceiver();
        
        //listen to incoming calls
        TelephonyManager tmgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        phoneReceiver = new MyPhoneStateListener(this.getApplicationContext());
        tmgr.listen(phoneReceiver, PhoneStateListener.LISTEN_CALL_STATE);
        
        IntentFilter filter = new IntentFilter();
        filter.addAction(Constants.ACTION_SETTINGS_CHANGE);
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        filter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        filter.addAction(Constants.ACTION_FAVORITE_NAME_UPDATE);
        filter.addAction(Constants.ACTION_FAVORITE_PHONE_UPDATE);
        filter.addAction(Constants.ACTION_FAVORITE_STATUS_UPDATE);
        registerReceiver(receiver, new IntentFilter(filter));
        filter = new IntentFilter();
        filter.addAction(Constants.ACTION_SIP_INCOMING);
        registerReceiver(incReceiver, filter);
        filter = new IntentFilter();
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(netReceiver, filter);
        
        prefs = PreferenceManager.getDefaultSharedPreferences(this);

        messageParser = new MessageParser();
        messageParser.ignoreSheetPlayload();
        messageFactory = new MessageFactory();
        messageDispatcher = new MessageDispatcher();
        userUpdateManager = new UserUpdateManager(this);
        userUpdateManager.setXivoLink(this);
        callHistoryManager = new CallHistoryManager(this);
        addDispatchers();

        favList = new FavoriteList();
        try {
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(openFileInput("favorite_ids")));
			String s;
			while((s = reader.readLine()) != null) {
				favList.add(new Favorite(Integer.parseInt(s)));
			}
		} catch (FileNotFoundException e) {
			//never mind. It will be created after adding a first favorite.
		} catch (IOException e) {
			e.printStackTrace();
		}
		
    }

    /**
     * Before the service destruction, unregister all the receivers and remove the notification
     */
    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        stopThread();
        connectionCleanup();
        unregisterReceiver(receiver);
        unregisterReceiver(incReceiver);
        unregisterReceiver(netReceiver);
        
        //stop listening to phone calls
        TelephonyManager tmgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        tmgr.listen(phoneReceiver, PhoneStateListener.LISTEN_NONE);
        
        //no XiVO connection service = no message received = no notification to show!
        //(and don't lie to the user saying "service running" too)
        if(xivoNotif != null) xivoNotif.removeNotif();
        super.onDestroy();
    }

    /**
     * Stops the looper thread.
     */
    private void stopThread() {
        cancel = true;
        if (thread != null) {
            Thread dead = thread;
            thread = null;
            dead.interrupt();
        }
    }

    /**
     * Authenticates the user with the CTI server.
     * @return AUTHENTICATION_OK on success, or error message on failure
     */
    private int authenticate() {
        authenticated = false;
        if (authenticating)
            return Constants.ALREADY_AUTHENTICATING;
        authenticating = true;
        int res = loginCTI();
        authenticating = false;
        if (res == Constants.AUTHENTICATION_OK) {
            authenticated = true;
            contactsToGo = 0;
            contactsTotal = 0;
            contactsLoaded = 0;
            wasOne = false;
        }
        return res;
    }

    /**
     * Connects the service to the CTI server
     * @return CONNECTION_OK on success, error code otherwise
     */
    private int connect() {
        if (connecting == true)
            return Constants.ALREADY_CONNECTING;
        while (disconnectTask != null && disconnectTask.getStatus() == AsyncTask.Status.RUNNING) {
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                Log.d(TAG, "Got interrupted while waiting for the running disconnection");
                e.printStackTrace();
            }
        }
        connecting = true;
        int res = connectToServer();
        connecting = false;
        switch (res) {
        case Constants.CONNECTION_OK:
            break;
        case Constants.BAD_HOST:
            wrongHostPort = true;
            break;
        default:
            connectionCleanup();
        }
        return res;
    }

    /**
     * Clean up the network connection and input/output streams
     */
    private void connectionCleanup() {
        connecting = false;
        authenticated = false;
        authenticating = false;
        if (inputBuffer != null) {
            inputBuffer = null;
        }
        if (networkConnection != null) {
            try {
                try {
                    networkConnection.shutdownOutput();
                    networkConnection.shutdownInput();
                } catch (IOException e) {
                    Log.d(TAG, "Input and output were already closed");
                }
                if (networkConnection != null) {
                    Socket tmp = networkConnection;
                    tmp.close();
                    networkConnection = null;
                }
            } catch (IOException e) {
                Log.e(TAG, "Error while cleaning up the network connection");
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onUnbind(Intent i) {
        Log.d(TAG, "Unbind called");
        return super.onUnbind(i);
    }

    /**
     * Give an access to the service.
     */
    @Override
    public IBinder onBind(Intent intent) {
    	Log.d(TAG, "*** asked to bind with me!");
        return binder;
    }

    /**
     * Starts a network connection with the server
     * 
     * @return connection status
     */
    private int connectToServer() {
        if (wrongHostPort)
            return Constants.BAD_HOST;
        resetChannels();
        authenticated = false;
        int port = Constants.XIVO_DEFAULT_PORT;
        try {
            port = Integer.parseInt(prefs.getString("server_port", Integer.toString(Constants.XIVO_DEFAULT_PORT)));
        } catch (NumberFormatException e) {
            Log.d(TAG, "Port number cannot be parsed to int, using default port");
        }
        String host = prefs.getString("server_adress", "");
        stopThread();
        while (thread != null && thread.isAlive()) {
            try {
                wait(100);
            } catch (InterruptedException e) {
                Log.d(TAG, "Wait interrupted");
            }
        }
        try {
            Log.d(TAG, "Connecting to " + host + " " + port);
            networkConnection = new Socket(host, port);
        } catch (UnknownHostException e) {
            Log.d(TAG, "Unknown host " + host);
            wrongHostPort = true;
            return Constants.BAD_HOST;
        } catch (IOException e) {
            return Constants.CONNECTION_REFUSED;
        }
        bytesReceived = 0L;
        xivoNotif = new XivoNotification(getApplicationContext());
        xivoNotif.createNotification(null);
        Log.d(TAG, "connected .....");
        try {
            inputBuffer = new BufferedReader(new InputStreamReader(networkConnection.getInputStream()));

        } catch (IOException e) {
            return Constants.NO_NETWORK_AVAILABLE;
        }
        return Constants.CONNECTION_OK;
    }

    /**
     * Disconnects from the server.
     * @return OK (everytime)
     */
    private int disconnectFromServer() {
        Log.d(TAG, "Disconnecting");
        if (xivoNotif != null)
            xivoNotif.removeNotif();
        if (!(disconnectTask != null && disconnectTask.getStatus() == AsyncTask.Status.RUNNING)) {
            disconnectTask = new DisconnectTask();
            disconnectTask.execute();
        }
        return Constants.OK;
    }
    
    
    /**
     * Task used to disconnect the user from the server.
     */
    private class DisconnectTask extends AsyncTask<Void, Void, Integer> {

        @Override
        protected Integer doInBackground(Void... params) {
            stopThread();
            connectionCleanup();
            resetState();
            return 0;
        }
    }

    /**
     * Asks the server to refresh the features. (it actually gets the whole user config)
     */
    private void refreshFeatures() {
        if (thread == null)
            startLooping(getApplicationContext());
        Log.d(TAG, "*** asking the features");
        sendLine(messageFactory.createGetUserConfig(userId).toString());
    }

    /**
     * Perform authentication on the XiVO CTI server
     * 
     * @return error or success code
     */
    private int loginCTI() {
        int res;

        /**
         * Creating first Json login array First step : check that login is
         * allowed on server
         */
        JSONObject jLogin = messageFactory.createLoginId(SettingsActivity.getLogin(this), "android-"
                + android.os.Build.VERSION.RELEASE);
        res = sendLoginCTI(jLogin);
        if (res != Constants.OK)
            return res;

        /**
         * Second step : check that password is allowed on server
         */
        res = sendPasswordCTI();
        if (res != Constants.OK)
            return res;

        /**
         * Third step : send configuration options on server
         */
        return sendCapasCTI();
    }

    /**
     * Starts the main loop to listen to incoming JSON messages from the CTI
     * server
     */
    private void startLooping(Context context) {
    	try {
    		Looper.prepare(); //for the sync, you'd better loop
    	} catch(RuntimeException re) { } //Looper is already looping so don't loop the looper again
    	
        cancel = false;
        handler = new Handler() {
            /**
             * Receives messages from the json loop and broadcast intents
             * accordingly.
             */
            @Override
            public void handleMessage(Message msg) {
                switch (Messages.get(msg.what)) {
                case USERS_LIST_COMPLETE:
                    Log.d(TAG, "Users list loaded");
                    Intent iLoadUser = new Intent();
                    iLoadUser.setAction(Constants.ACTION_LOAD_USER_LIST);
                    sendBroadcast(iLoadUser);
                    loadList("phones");
                    break;
                case PHONES_LOADED:
                    Log.d(TAG, "Phones list loaded");
                    Intent iLoadPhone = new Intent();
                    iLoadPhone.setAction(Constants.ACTION_LOAD_USER_LIST);
                    sendBroadcast(iLoadPhone);
                    break;
                case PRESENCE_UPDATE:
                    Log.d(TAG, "Presence changed");
                    Intent iLoadPresence = new Intent();
                    iLoadPresence.setAction(Constants.ACTION_LOAD_USER_LIST);
                    sendBroadcast(iLoadPresence);
                    break;
                case HISTORY_LOADED:
                    Intent iLoadHistory = new Intent();
                    iLoadHistory.setAction(Constants.ACTION_LOAD_HISTORY_LIST);
                    sendBroadcast(iLoadHistory);
                    break;
                case FEATURES_LOADED:
                    Intent iLoadFeatures = new Intent();
                    iLoadFeatures.setAction(Constants.ACTION_LOAD_FEATURES);
                    sendBroadcast(iLoadFeatures);
                    break;
                case NO_MESSAGE:
                    break;
                case JSON_EXCEPTION:
                    Log.d(TAG, "JSON error while receiving a message from the CTI server");
                    break;
                default:
                    Log.d(TAG, "handling an unknown message: " + msg.what);
                    break;
                }
            }
        };

        thread = new Thread() {
            @Override
            public void run() {
                Looper.prepare();
                JSONObject newLine = null;
                Log.d(TAG, "Starting the main loop");
                while (!(cancel)) {
                    while ((newLine = readJsonObjectCTI()) == null && !(cancel));
                    handler.sendEmptyMessage(parseIncomingJson(newLine).getId());
                }
            };
        };
        thread.start();
    }

    /**
     * Parses an incoming json message from the cti server and dispatches it to
     * other methods for better message handling.
     * 
     * @return
     */
    private Messages parseIncomingJson(JSONObject line) {
        if (cancel || line == null)
            return Messages.NO_MESSAGE;
        try {
            CtiResponseMessage<?> ctiResponseMessage = messageParser.parse(line);
            messageDispatcher.dispatch(ctiResponseMessage);
        } catch (JSONException e1) {
            e1.printStackTrace();
            Log.d(TAG, "unable to decode message received");
        } catch (IllegalArgumentException e2) {
            Log.d(TAG, "Message received not decoded");
            //see if it was a meetme-related message
            try {
				if(line.getString("function").equals("listid")
						&& line.getString("listname").equals("meetmes")) {
		            Log.v(TAG, "Meeetme List Update Received!");
		            JSONArray liste = line.getJSONArray("list");
		            for(int i = 0; i < liste.length(); i++) {
		            	String s = liste.getString(i);
		            	JSONObject getMeetmeRequest = messageFactory.createGetUserConfig(s);
		            	getMeetmeRequest.put("listname", "meetmes");
		            	sendLine(getMeetmeRequest.toString());
		            	contactsToGo++;
		            	contactsTotal++;
		            }
				}
				if(line.getString("function").equals("updateconfig")
						&& line.getString("listname").equals("meetmes")) {
		            Log.v(TAG, "Meeetme Update Received!");
		            String id = "-"+line.get("tid");
		            line = line.getJSONObject("config");
		            String name = line.getString("name");
		            String number = line.getString("confno");
		            addMeetme(id, name, number);
		            decrementContactsToGo();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
        } catch(NullPointerException e) {
        	Log.d(TAG, "null message");
        }
        return Messages.UNKNOWN;
    }

    /**
     * Adds some conference room in the user list
     * @param id the meetme ID (< 0 ; the opposite of the real meetme ID not to confuse them with users)
     * @param name the meetme name shown to the user
     * @param number the number to call the conference room
     */
    private void addMeetme(String id, String name, String number) {
    	ContentValues user = new ContentValues();
        user.put(UserProvider.ASTID, id);
        user.put(UserProvider.XIVO_USERID, id);
        user.put(UserProvider.FULLNAME, name);
        user.put(UserProvider.PHONENUM, number);
        user.put(UserProvider.STATEID, "disconnected");
        user.put(UserProvider.STATEID_LONGNAME, getString(R.string.conf_room));
        user.put(UserProvider.STATEID_COLOR, "#00FF00");
        user.put(UserProvider.TECHLIST, "techlist");
        user.put(UserProvider.HINTSTATUS_COLOR, "#00FF00");
        user.put(UserProvider.HINTSTATUS_CODE, Constants.DEFAULT_HINT_CODE);
        user.put(UserProvider.HINTSTATUS_LONGNAME, "");
        getContentResolver().insert(UserProvider.CONTENT_URI, user);
        user.clear();
	}

	/**
     * Send capacity options to the CTI server
     * 
     * @return error or success code
     */
    private int sendCapasCTI() {
        JSONObject jsonCapas = messageFactory.createLoginCapas(capaId);

        int res = sendLine(jsonCapas.toString());
        if (res != Constants.OK)
            return res;

        jsonCapas = readJsonObjectCTI();
        LoginCapasAck loginCapasAck = null;
        try {
            loginCapasAck = (LoginCapasAck) messageParser.parse(jsonCapas);
        } catch (JSONException e1) {
            e1.printStackTrace();
            Log.d(TAG, "Unable to parser login capas ack : " + e1.getMessage());
            return Constants.VERSION_MISMATCH;
        }
        resetState();
        userId = loginCapasAck.userId;
        userUpdateManager.setUserId(Integer.valueOf(userId));
        configureXlets(loginCapasAck.xlets);
        configureUserStatuses(loginCapasAck.capacities.getUsersStatuses());
        userUpdateManager.setCapacities(loginCapasAck.capacities);
        JSONObject jsonCtiMessage = readJsonObjectCTI();
        try {
            CtiResponseMessage<?> ctiResponseMessage = messageParser.parse(jsonCtiMessage);
            messageDispatcher.dispatch(ctiResponseMessage);
        } catch (JSONException e) {
            return Constants.JSON_POPULATE_ERROR;
        }
        jsonCtiMessage = readJsonObjectCTI();
        try {
            CtiResponseMessage<?> ctiResponseMessage = messageParser.parse(jsonCtiMessage);
            messageDispatcher.dispatch(ctiResponseMessage);
        } catch (JSONException e) {
            return Constants.JSON_POPULATE_ERROR;
        }

        authenticated = true;
        return Constants.AUTHENTICATION_OK;

    }

    /**
     * Drop the whole Capapresence database and put these user statuses inside instead
     * @param userStatuses The list of {@link UserStatus}es to put in the database.
     */
    private void configureUserStatuses(List<UserStatus> userStatuses) {
        Log.d(TAG, "user statuses configuration");
        getContentResolver().delete(CapapresenceProvider.CONTENT_URI, null, null);
        ContentValues presence = new ContentValues();
        for (UserStatus userStatus : userStatuses) {
            presence.put(CapapresenceProvider.NAME, userStatus.getName());
            presence.put(CapapresenceProvider.COLOR, userStatus.getColor());
            presence.put(CapapresenceProvider.LONGNAME, userStatus.getLongName());
            presence.put(CapapresenceProvider.ALLOWED, 1);
            getContentResolver().insert(CapapresenceProvider.CONTENT_URI, presence);
            presence.clear();
        }
    }

    /**
     * Drop the whole Capaxlets database and put these Xlets inside instead
     * @param xlets The list of {@link Xlet}s to put in the database.
     */
    private void configureXlets(List<Xlet> xlets) {
        Log.d(TAG, "Setting xlets");
        // Remove old entries
        getContentResolver().delete(CapaxletsProvider.CONTENT_URI, null, null);
        ContentValues values = new ContentValues();
        for (Xlet xlet : xlets) {
            values.put(CapaxletsProvider.XLET, xlet.getName());
            getContentResolver().insert(CapaxletsProvider.CONTENT_URI, values);
        }
        Intent i = new Intent();
        i.setAction(Constants.ACTION_LOAD_XLETS);
    }

    /**
     * Reset information about a session
     */
    private void resetState() {
        userId = null;
        sessionId = null;
        //astId = null;
    }

    /**
     * Sends the password to the CTI server.
     * @return OK if success or error code.
     */
    private int sendPasswordCTI() {
        JSONObject jsonPasswordAuthent = messageFactory.createLoginPass(prefs.getString("password", ""), sessionId);
        int res = sendLine(jsonPasswordAuthent.toString());
        if (res != Constants.OK)
            return res;

        JSONObject ctiAnswer = readJsonObjectCTI();
        try {
            LoginPassAck loginPassAck = (LoginPassAck) messageParser.parse(ctiAnswer);
            capaId = loginPassAck.capalist.get(0);
        } catch (JSONException e1) {
            e1.printStackTrace();
            return Constants.JSON_POPULATE_ERROR;
        } catch (IllegalArgumentException e2) {
            disconnectFromServer();
            return parseLoginError(ctiAnswer);
        } catch(ClassCastException cce) {
        	//there is some state problem, so disconnect right now and return an error...
        	disconnectFromServer();
        	Log.d(TAG, "Unexpected answer from cti server");
            cce.printStackTrace();
            return Constants.JSON_POPULATE_ERROR;
        }

        return Constants.OK;
    }

    /**
     * Sends login information to the cti server
     * 
     * @param jLogin
     * @return error or success code
     */
    private int sendLoginCTI(JSONObject jLogin) {
        JSONObject nextJsonObject;
        int res;
        if ((res = sendLine(jLogin.toString())) != Constants.OK)
            return res;

        if ((nextJsonObject = readJsonObjectCTI()) == null) {
            return Constants.JSON_POPULATE_ERROR;
        }
        try {
            LoginIdAck loginAck = (LoginIdAck) messageParser.parse(nextJsonObject);
            sessionId = loginAck.sesssionId;
        } catch (JSONException e1) {
            Log.d(TAG, "Unexpected answer from cti server");
            e1.printStackTrace();
            return Constants.JSON_POPULATE_ERROR;
        } catch (IllegalArgumentException e2) {
            disconnectFromServer();
            return parseLoginError(nextJsonObject);
        } catch(ClassCastException cce) {
        	//there is some state problem, so disconnect right now and return an error...
        	disconnectFromServer();
        	Log.d(TAG, "Unexpected answer from cti server");
            cce.printStackTrace();
            return Constants.JSON_POPULATE_ERROR;
        }
        return Constants.OK;
    }

    /**
     * Retrieves the error when the CTI server doesnt return a LOGIN_OK
     * 
     * @return error code
     */
    private int parseLoginError(JSONObject jsonObject) {
        try {
            if (jsonObject != null && jsonObject.has("errorstring")) {
                String error = jsonObject.getString("errorstring");
                if (error.equals(Constants.XIVO_LOGIN_PASSWORD) || error.equals(Constants.XIVO_LOGIN_UNKNOWN_USER)) {
                    return Constants.LOGIN_PASSWORD_ERROR;
                } else if (error.contains(Constants.XIVO_CTI_VERSION_NOT_SUPPORTED)) {
                    return Constants.CTI_SERVER_NOT_SUPPORTED;
                } else if (error.equals(Constants.XIVO_VERSION_NOT_COMPATIBLE)) {
                    return Constants.VERSION_MISMATCH;
                }
            } else if (jsonObject != null
                    && jsonObject.has("class")
                    && (jsonObject.getString("class").equals("disconn") || jsonObject.getString("class").equals(
                            "disconnect"))) {
                return Constants.FORCED_DISCONNECT;
            }
        } catch (JSONException e) {
            Log.d(TAG, "JSON exception while parsing error string");
        }
        return Constants.LOGIN_KO;
    }

    /**
     * Perform a read action on the stream from CTI server
     * 
     * @return JSON object retrieved
     */
    private JSONObject readJsonObjectCTI() {
        JSONObject ReadLineObject;
        String responseLine;

        try {
            while ((responseLine = getLine()) != null) {
                try {
                    ReadLineObject = new JSONObject(responseLine);
                    Log.d(TAG, "Server: " + responseLine);
                    return ReadLineObject;
                } catch (Exception e) {
                    Log.e(TAG, "readJsonObjectCTI error");
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            if (networkConnection != null && networkConnection.isConnected() == false) {
                lostConnectionEvent();
            }
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Handles a connection lost
     */
    private void lostConnectionEvent() {
        Log.d(TAG, "lostConnectionEvent");
        disconnectFromServer();
    }

    /**
     * Reads the next incoming line and increment the byte counter and returns
     * the line
     * 
     * @throws IOException
     */
    private String getLine() throws IOException {
        if (inputBuffer == null)
            return null;

        String line = inputBuffer.readLine();
        if (line != null) {
            bytesReceived += line.getBytes().length;
        }
        return line;
    }

    /**
     * Sends a line to the networkConnection
     */
    private int sendLine(String line) {
        if (line == null || line.equals("")) {
            return Constants.OK;
        }
        PrintStream output = null;
        try {
            if (networkConnection == null)
                return Constants.NO_NETWORK_AVAILABLE;
            output = new PrintStream(networkConnection.getOutputStream());
        } catch (IOException e) {
            return Constants.NO_NETWORK_AVAILABLE;
        }
        Log.d(TAG, "Client >>>> " + line);
        output.println(line);
        return Constants.OK;
    }

    /**
     * Clears all channels
     */
    private void resetChannels() {
        Log.d(TAG, "Reseting channels");
        if (thisChannel != null && !thisChannel.startsWith("Local/")) {
            oldChannel = thisChannel;
        }
        thisChannel = null;
        peerChannel = null;
    }

    /**
     * Sends a JSON message to the CTI server.
     * @param message the object to send
     */
    private void sendMessage(JSONObject message) {
        int res;
        if ((res = sendLine(message.toString())) != Constants.OK) {
            Log.d(TAG, "Could not send message");
            switch (res) {
            case Constants.NO_NETWORK_AVAILABLE:
                Log.d(TAG, "No network");
                resetState();
                break;
            }
        }
    }

    /**
     * A receiver receiving potential favorites updates, and keeping the notification / WiFi SSID always up to date.
     */
    private class IntentReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
        	boolean mustRefresh = false;
            final String action = intent.getAction();
            if(action.equals(Constants.ACTION_FAVORITE_NAME_UPDATE)) {
            	Log.v(TAG, "Received fav name update "+intent.getStringExtra("info")+
            			" for id "+intent.getIntExtra("id", 0));
            	Favorite fav = favList.getFavorite(intent.getIntExtra("id", 0));
            	if(fav != null) {
            		fav.userName = intent.getStringExtra("info");
            		mustRefresh = true;
            	}
            }
            if(action.equals(Constants.ACTION_FAVORITE_PHONE_UPDATE)) {
            	Log.v(TAG, "Received fav phone update "+intent.getStringExtra("info")+
            			" for id "+intent.getIntExtra("id", 0));
            	Favorite fav = favList.getFavorite(intent.getIntExtra("id", 0));
            	if(fav != null) {
            		fav.phoneState = intent.getStringExtra("info");
            		mustRefresh = true;
            	}
            }
            if(action.equals(Constants.ACTION_FAVORITE_STATUS_UPDATE)) {
            	Log.v(TAG, "Received fav status update "+intent.getStringExtra("info")+
            			" for id "+intent.getIntExtra("id", 0));
            	Favorite fav = favList.getFavorite(intent.getIntExtra("id", 0));
            	if(fav != null) {
            		fav.userState = intent.getStringExtra("info");
            		mustRefresh = true;
            	}
            }
            if (action.equals(Constants.ACTION_SETTINGS_CHANGE)
                    || action.equals(ConnectivityManager.CONNECTIVITY_ACTION)
                    || action.equals(WifiManager.WIFI_STATE_CHANGED_ACTION)) {
                wrongHostPort = false;
            } 
            if(action.equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)) {
            	Log.i("SIP Trigger", "Network State Changed!");
            	NetworkInfo netInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
            	if(netInfo.getType() == ConnectivityManager.TYPE_WIFI) {
            		if(netInfo.isConnected()) {
            			WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
            			WifiInfo info = wifiManager.getConnectionInfo();
            			//remove the quotes surrounding the SSID
            			String ssid = info.getSSID();
            			Log.i("SIP Trigger", "Got it: "+ssid+"!");
            			lastKnownSSID = ssid.replace("\"", "");
            		} else {
            			Log.i("SIP Trigger", "Wiped known SSID!");
            			lastKnownSSID = "none";
            			if(SipApiUser.INSTANCE != null) {
         					Log.i("SIP Trigger", "We're NOT on WIFI, shut that SIP down!");
         					SipApiUser.INSTANCE.closeConnection();
         				}
            		}
            	}
            }
            if(mustRefresh) {
            	int count = 0;
            	for(int i = 0; i < favList.favList.size(); i++) {
            		if(favList.favList.get(i).userId > 0) count++;
            	}
            	
            	Favorite[] tab = new Favorite[count];
            	//refresh the notification
            	int j = 0;
            	for(int i = 0; i < favList.favList.size(); i++) {
        			Favorite fav = favList.favList.get(i);
        			if(fav.userId > 0) {
        				tab[j] = fav;
        				j++;
        			}
            	}
            	xivoNotif.createNotification(tab);
            }
        }

    }

    /**
     * Called when all the user IDs have been loaded.
     */
    @Override
    public void onUserIdsLoaded(UserIdsList userIdsList) {
    	contactsToGo++;
        for (Integer userId : userIdsList.getIds()) {
            Log.d(TAG, "User id : " + userId);
            JSONObject getUserMessage = messageFactory.createGetUserConfig(""+userId);
            sendMessage(getUserMessage);
            contactsToGo++;
            contactsTotal+=2;
        }
    }

    /**
     * Sends a new request to get a phone's config.
     */
    @Override
    public void sendGetPhoneConfig(Integer lineId) {
    	contactsToGo++;
        JSONObject getPhoneConfigMessage = messageFactory.createGetPhoneConfig(""+lineId);
        sendMessage(getPhoneConfigMessage);
    }
    
    private ArrayList<Integer> gettingUserIds;
    /**
     * Sends a new request to get a user's status.
     */
    @Override
    public void sendGetUserStatus(Integer userId) {
    	if(contactsToGo <= 0) {
    		//do this one immediately
	        JSONObject jsonGetUserStatusMessage = messageFactory.createGetUserStatus(""+userId);
	        sendMessage(jsonGetUserStatusMessage);
    	} else {
    		//add it to the waiting requests
    		if(gettingUserIds == null)
    			gettingUserIds = new ArrayList<Integer>();
    		gettingUserIds.add(userId);
    	}
    }

    private ArrayList<Integer> gettingPhoneIds;
    /**
     * Sends a new request to get a phone's status.
     */
    @Override
    public void sendGetPhoneStatus(Integer lineId) {
    	if(contactsToGo <= 0) {
			//do this one immediately
	        JSONObject jsonGetUserStatusMessage = messageFactory.createGetPhoneStatus(""+lineId);
	        sendMessage(jsonGetUserStatusMessage);
		} else {
			//add it to the waiting requests
			if(gettingPhoneIds == null)
				gettingPhoneIds = new ArrayList<Integer>();
			gettingPhoneIds.add(lineId);
		}
    }

    //useless methods for this time BEGIN
	@Override
	public void onAgentIdsLoaded(AgentIds arg0) {
	}

	@Override
	public void onPhoneIdsLoaded(PhoneIdsList arg0) {
	}

	@Override
	public void onQueueIdsLoaded(QueueIds arg0) {
	}

	@Override
	public void onQueueMemberIdsLoaded(QueueMemberIds arg0) {
	}
    //useless methods for this time ENDS

	/**
	 * Called when a channel update occurs, we got to update thisChannel and peerChannel.
	 * @param channelStatus
	 */
	@Override
	public void onChannelStatusUpdate(ChannelStatus channelStatus) {
		if(channelStatus.direction.equals("out")) {
			Log.i(TAG, "Channel status update: my id is: "+channelStatus.channelId);
			thisChannel = channelStatus.channelId;
		} else {
			peerChannel = channelStatus.channelId;
			Log.i(TAG, "Channel status update: my peer's id is: "+channelStatus.channelId);
		}
	}

	boolean wasOne = false;
	/**
	 * Decrements the contactsToGo variable, and send Intents when all contacts are loaded.
	 */
	private void decrementContactsToGo() {
		contactsToGo--;
		Log.v("decrementContactsToGo()", 
				"contacts total="+contactsTotal+", toGo="+contactsToGo+", loaded="+contactsLoaded);
		if(contactsTotal != 0) {
			Intent percent = new Intent();
			percent.setAction(Constants.ACTION_NEW_PERCENTAGE);
			int i = contactsLoaded * 100 / contactsTotal;
			percent.putExtra("percent", (i > 100 ? 100 : i));
			sendBroadcast(percent);
			contactsLoaded++; //if you add it before, it ends up with contacts total=8, toGo=0, loaded=9 -> 112%
		}
		
		if(contactsToGo == 0 && wasOne) {
			//execute the waiting requests for phones...
			if(gettingPhoneIds != null) {
	    		for(int userID : gettingPhoneIds) {
	    			JSONObject jsonGetUserStatusMessage = messageFactory.createGetPhoneStatus(""+userID);
	    	        sendMessage(jsonGetUserStatusMessage);
	    		}
	    		gettingPhoneIds = null;
			}
			//... but also for users
    		if(gettingUserIds != null) {
	    		for(int userID : gettingUserIds) {
	    			JSONObject jsonGetUserStatusMessage = messageFactory.createGetUserStatus(""+userID);
	    	        sendMessage(jsonGetUserStatusMessage);
	    		}
	    		gettingUserIds = null;
    		}
			
			Intent notification = new Intent();
			notification.setAction(Constants.ACTION_CONTACT_LOAD_COMPLETE);
	        sendBroadcast(notification);
	        wasOne = false;
		}
		if(contactsToGo == 1) {
			wasOne = true;
			Intent notification = new Intent();
			notification.setAction(Constants.ACTION_CONTACT_LOAD_ONE_LEFT);
	        sendBroadcast(notification);
		}
	}

	/**
	 * Receiver listening to incoming SIP calls.
	 */
	private class IncomingCallReceiver extends BroadcastReceiver implements OnPreparedListener {
	    @Override
	    public void onReceive(final Context context, Intent intent) {
	    	Log.i("Sip API User", "RECEIVING A CALL !!!");
	        final SipAudioCall incomingCall;
	        try {            
	        	
	        	SipAudioCall.Listener listener = new SipAudioCall.Listener() {
	    			@Override
	    			public void onCallEstablished(SipAudioCall call) {
	    				Log.i("SIP API User", "Call begins now!");
	    				
	    				Intent intent = new Intent();
	    				intent.setAction(Constants.ACTION_SIP_ESTABLISHED);
	    				SipApiUser.INSTANCE.currentCall = call;
	    				PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
	    				
	    				try {
	    					pendingIntent.send();
	    				} catch (CanceledException e) {
	    					e.printStackTrace();
	    				}
	    			}
	    			
	    			@Override
	    			public void onCallEnded(SipAudioCall call) {
	    				//nothing for this time.
	    				Log.i("SIP API User", "Your call just ended!");
	    				call.close();
	    				
	    				Intent intent = new Intent();
	    				intent.setAction(Constants.ACTION_SIP_HUNG_UP);
	    				PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
	    				SipApiUser.INSTANCE.currentCall = call;
	    				try {
	    					pendingIntent.send();
	    				} catch (CanceledException e) {
	    					e.printStackTrace();
	    				}
	    				
	    				AssetFileDescriptor afd = SipApiUser.INSTANCE.theContext.getResources().openRawResourceFd(R.raw.endcall);
	    				SipApiUser.INSTANCE.soundEffects = new MediaPlayer();
	    				try {
	    					SipApiUser.INSTANCE.soundEffects.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
	    				} catch (IllegalArgumentException e1) {
	    					e1.printStackTrace();
	    				} catch (IllegalStateException e1) {
	    					e1.printStackTrace();
	    				} catch (IOException e1) {
	    					e1.printStackTrace();
	    				}
	    				SipApiUser.INSTANCE.soundEffects.setAudioStreamType(AudioManager.STREAM_VOICE_CALL);
	    				SipApiUser.INSTANCE.soundEffects.setOnPreparedListener(IncomingCallReceiver.this);
	    				try {
	    					SipApiUser.INSTANCE.soundEffects.prepareAsync();
	    				} catch (IllegalStateException e) {
	    					e.printStackTrace();
	    				}
	    			}
	    			
	    			@Override
	    			public void onError(SipAudioCall call, int errorCode, String errorMessage) {
	    				Toast.makeText(context, "SIP error: "+errorMessage, Toast.LENGTH_LONG).show();
	    				Log.w("SIP API User", "Call failed: "+errorMessage);
	    				
	    				Intent intent = new Intent();
	    				intent.setAction(Constants.ACTION_SIP_FAILED);
	    				intent.putExtra("errorMessage", errorMessage);
	    				PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
	    				SipApiUser.INSTANCE.currentCall = call;
	    				try {
	    					pendingIntent.send();
	    				} catch (CanceledException e) {
	    					e.printStackTrace();
	    				}
	    			}
	    		};
	        	
	            incomingCall = SipApiUser.INSTANCE.theManager.takeAudioCall(intent, listener);
	        	Log.v("Sip API User", "About to launch the calling activity");
	            
	            
	        	SipApiUser.INSTANCE.currentCall = incomingCall;
	            
	            if(CallingActivityNew.INSTANCE == null) {
		            Intent i = new Intent(context, CallingActivityNew.class);
		    		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		    		i.putExtra("incoming", true);
		        	Log.v("Sip API User", "About to launch the calling activity");
		        	SipApiUser.INSTANCE.isAlreadyRinging = false;
		        	
		    		if(incomingCall != null)
		    			context.startActivity(i);
	            } else {
	            	if(CallingActivityNew.INSTANCE.hasMultipleCalls()) {
	            		//triple call is too unstable ; accept only 2 calls at once.
	            		//reject this THIRD one
	            		incomingCall.endCall();
	            	} else {
	            		SipApiUser.INSTANCE.isAlreadyRinging = false;
		            	CallingActivityNew.INSTANCE.addCall(true);
		            	
		            	//ensure the activity is on top of the screen
		            	Intent i = new Intent(context, BringToFront.class);
		            	i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		            	i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
						context.startActivity(i);
	            	}
	            }
	    		
	    		
	        } catch (Exception e) {
	        }
	    }
	    
	    @Override
		public void onPrepared(MediaPlayer arg0) {
			arg0.start();
			Log.d("SIP API USER", "Sound effect is playing!");
		}
	}
	
	/**
	 * Receiver getting network state updates:
	 * when we are disconnected from the network, we should make the user disconnected.
	 */
	private class NetStateReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(final Context context, Intent intent) {
			Log.d("app","Network connectivity change");
			if(intent.getExtras().getBoolean(ConnectivityManager.EXTRA_NO_CONNECTIVITY,Boolean.FALSE)) {
				Thread fil = new Thread("Assuming disconnection") {
					@Override public void run() {
						Log.i("app","There's no network connectivity!!");
						if(SipApiUser.INSTANCE != null)
							SipApiUser.INSTANCE.closeConnection();
			            try {
			            	if((networkConnection != null && networkConnection.isConnected() 
			            			&& inputBuffer != null && 
			            			(disconnectTask == null || disconnectTask
			                        .getStatus() != AsyncTask.Status.RUNNING))
			                        && authenticated) disconnectFromServer();
						} catch(NullPointerException ne) {
							//if the connection instance is null
							ne.printStackTrace();
						} catch(ReceiverCallNotAllowedException e) {
							//if the connection instance does not contain the service, it will
							//try to bind to the service ; but it is not allowed to do so.
							e.printStackTrace();
						}
					}
				}; fil.start();            
			}
		}
	}
	
/**
 * this listener will trigger the auto-pickup when the phone is ringing
 */
 private class MyPhoneStateListener extends PhoneStateListener {
		 Context c;
		 
	    public MyPhoneStateListener(Context context) {
			c = context;
		}

			public void onCallStateChanged(int state, String incomingNumber) {
	        
				if(state != 1) return;
				//TODO take account of the incoming number!
				
		        Log.d("Phone Call Receiver",state+"   incoming no:"+incomingNumber);
	
		        //avoid picking up ALL the user's calls....
		        if(!XletDialer.autoPickup) {
		          	Log.d("Phone Call Receiver", "Not going to pick up this call");
		          	return;
		        }
		           
		        // Simulate a press of the headset button to pick up the call
		        // (thanks again Stack Overflow)
		        // will not work on all phones, but it's worth trying.
		        // (works on my Galaxy S2)
		        Thread th = new Thread() {
		         	@Override public void run() {
		          		try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
		           		Intent buttonUp = new Intent(Intent.ACTION_MEDIA_BUTTON);
		    	        buttonUp.putExtra(Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_HEADSETHOOK));
		    	        c.sendOrderedBroadcast(buttonUp, "android.permission.CALL_PRIVILEGED");
		          	}
		        }; th.start();
	        }
	 }
}
