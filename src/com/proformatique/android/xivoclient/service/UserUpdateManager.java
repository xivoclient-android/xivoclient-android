package com.proformatique.android.xivoclient.service;

import org.xivo.cti.listener.ConfigListener;
import org.xivo.cti.model.Capacities;
import org.xivo.cti.model.PhoneStatus;
import org.xivo.cti.message.AgentConfigUpdate;
import org.xivo.cti.message.PhoneConfigUpdate;
import org.xivo.cti.message.QueueConfigUpdate;
import org.xivo.cti.message.QueueMemberConfigUpdate;
import org.xivo.cti.message.QueueMemberRemoved;
import org.xivo.cti.message.QueueStatusUpdate;
import org.xivo.cti.message.UserConfigUpdate;
import org.xivo.cti.message.UserStatusUpdate;
import org.xivo.cti.message.PhoneStatusUpdate;
import org.xivo.cti.network.XiVOLink;

import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.RemoteException;
import android.util.Log;

import com.proformatique.android.xivoclient.Connection;
import com.proformatique.android.xivoclient.R;
import com.proformatique.android.xivoclient.dao.CapapresenceProvider;
import com.proformatique.android.xivoclient.dao.CapaservicesProvider;
import com.proformatique.android.xivoclient.dao.UserProvider;
import com.proformatique.android.xivoclient.tools.Constants;

/**
 * Manages all the user and phone updates received by the server
 */
public class UserUpdateManager implements ConfigListener {

    private static final String TAG = "XiVO user update";

    private final Service service;
    private long stateId = 0L;
    private XiVOLink xivoLink;
    private Integer userId;
    private Capacities capacities;
    private PhoneStatus userPhoneStatus;

    /**
     * Creates the UserUpdateManager with default values for getPhoneStatusColor and getPhoneStatusLongName.
     * @param service
     */
    public UserUpdateManager(Service service) {
        this.service = service;
        this.userPhoneStatus = new PhoneStatus("-1", Constants.DEFAULT_HINT_COLOR, service.getResources()
                .getString(R.string.default_hint_longname));
    }

    public long getStateId() {
        return stateId;
    }

    public String getPhoneStatusColor() {
        return userPhoneStatus.getColor();
    }

    public String getPhoneStatusLongName() {
        return userPhoneStatus.getLongName();
    }

    /**
     * Received when a new user is received (or when the services config has changed).
     * Puts this new user in the {@link UserProvider} or the service update in {@link CapaservicesProvider}.
     */
    @Override
    public void onUserConfigUpdate(UserConfigUpdate userConfigUpdate) {
        if (userConfigUpdate.getFullName() == null) {
            Log.d(TAG, "user config update : " + userConfigUpdate.getUserId() + " null full name");
            //partial message! could be a change in the services configuration
            if(userConfigUpdate.getUserId() == this.userId) {
            	Intent iServiceUpdate = new Intent();
                iServiceUpdate.setAction(Constants.ACTION_UPDATE_SERVICES);
            	if(userConfigUpdate.getBusyFwdDestination() != null) {
            		iServiceUpdate.putExtra("busyena", userConfigUpdate.isBusyFwdEnabled());
            		iServiceUpdate.putExtra("busynbr", userConfigUpdate.getBusyFwdDestination());
            	} else if(userConfigUpdate.getUncFwdDestination() != null) {
                    iServiceUpdate.putExtra("uncnbr", userConfigUpdate.getUncFwdDestination());
                    iServiceUpdate.putExtra("uncena", userConfigUpdate.isUncFwdEnabled());
            	} else if(userConfigUpdate.getNaFwdDestination() != null) {
            		iServiceUpdate.putExtra("rnaena", userConfigUpdate.isNaFwdEnabled());
            		iServiceUpdate.putExtra("rnanbr", userConfigUpdate.getNaFwdDestination());
            	} else {
                    iServiceUpdate.putExtra("enabledndena", userConfigUpdate.isDndEnabled());
            	}
                Context context = service.getApplicationContext();
            	context.sendBroadcast(iServiceUpdate);
            }
            return;
        }
        //update the UserProvider.
        Log.d(TAG, "user config update : " + userConfigUpdate.getUserId() + " " + userConfigUpdate.getFullName());
        ContentValues user = new ContentValues();
        user.put(UserProvider.ASTID, userConfigUpdate.getUserId());
        user.put(UserProvider.XIVO_USERID, userConfigUpdate.getUserId());
        user.put(UserProvider.FULLNAME, userConfigUpdate.getFullName());
        user.put(UserProvider.PHONENUM, "....");
        user.put(UserProvider.STATEID, "disconnected");
        user.put(UserProvider.STATEID_LONGNAME, "disconnected");
        user.put(UserProvider.STATEID_COLOR, "#202020");
        user.put(UserProvider.TECHLIST, "techlist");
        user.put(UserProvider.HINTSTATUS_COLOR, Constants.DEFAULT_HINT_COLOR);
        user.put(UserProvider.HINTSTATUS_CODE, Constants.DEFAULT_HINT_CODE);
        user.put(UserProvider.HINTSTATUS_LONGNAME,
                service.getApplicationContext().getString(R.string.default_hint_longname));
        service.getApplicationContext().getContentResolver().insert(UserProvider.CONTENT_URI, user);
        user.clear();
        if (userConfigUpdate.getUserId() == this.userId) {
        	//this is our user, so get his fullname and inform the XivoActivity (or the AppOnBoot)
            Log.i(TAG, "user config update : updating full name :" + userConfigUpdate.getFullName());
            Intent iUpdateIdentity = new Intent();
            iUpdateIdentity.setAction(Constants.ACTION_UPDATE_IDENTITY);
            iUpdateIdentity.putExtra("fullname", userConfigUpdate.getFullName());
            //this user config update also brings the user's features! put them together
            iUpdateIdentity.putExtra("enabledndena", userConfigUpdate.isDndEnabled());
            iUpdateIdentity.putExtra("uncena", userConfigUpdate.isUncFwdEnabled());
            iUpdateIdentity.putExtra("busyena", userConfigUpdate.isBusyFwdEnabled());
            iUpdateIdentity.putExtra("rnaena", userConfigUpdate.isNaFwdEnabled());
            iUpdateIdentity.putExtra("uncnbr", userConfigUpdate.getUncFwdDestination());
            iUpdateIdentity.putExtra("rnanbr", userConfigUpdate.getNaFwdDestination());
            iUpdateIdentity.putExtra("busynbr", userConfigUpdate.getBusyFwdDestination());
            
            Context context = service.getApplicationContext();
            context.sendBroadcast(iUpdateIdentity);
        }
        
        //let's send this info to the favorite-related intent receiver too, it could be a favorite
        Intent fav = new Intent();
        fav.setAction(Constants.ACTION_FAVORITE_NAME_UPDATE);
        fav.putExtra("id", userConfigUpdate.getUserId());
        fav.putExtra("info", userConfigUpdate.getFullName());
        service.getApplicationContext().sendBroadcast(fav);
        
        if(Connection.INSTANCE != null
        		&& Connection.INSTANCE.getConnection(service.getApplicationContext()) != null) {
        	try {
				Connection.INSTANCE.getConnection(service.getApplicationContext())
					.decrementContactsToGo();
			} catch (RemoteException e) {
				e.printStackTrace();
			}
        }
        if(userConfigUpdate.getLineIds().size() > 0) {
            sendGetPhoneConfig(userConfigUpdate.getLineIds().get(0));
            xivoLink.sendGetPhoneStatus(userConfigUpdate.getLineIds().get(0));
        }
        xivoLink.sendGetUserStatus(userConfigUpdate.getUserId());
    }
    
    /**
     * Called when a user changes his status.
     * Updates the {@link UserProvider} corresponding to this user.
     */
    @Override
    public void onUserStatusUpdate(UserStatusUpdate userStatusUpdate) {
        long currentStateId;
        Context context = service.getApplicationContext();
        Log.d(TAG, "user status updated " + userStatusUpdate.getUserId() + ", status [" + userStatusUpdate.getStatus()
                + "]");
        String statusName = userStatusUpdate.getStatus();
        
        //gets more info about this update in the Capapresence provider
        Cursor presence = context.getContentResolver().query(CapapresenceProvider.CONTENT_URI,
                new String[] { CapapresenceProvider._ID, CapapresenceProvider.NAME },
                CapapresenceProvider.NAME + " = '" + statusName + "'", null, null);
        presence.moveToFirst();
        currentStateId = presence.getLong(presence.getColumnIndex(CapapresenceProvider._ID));
        presence.close();
        
        if (userStatusUpdate.getUserId() == this.userId) {
        	//notify the Intent Receiver that my status changed
            Log.d(TAG, "My status changed, new status [" + userStatusUpdate.getStatus() + "]");
            stateId = currentStateId;
            Intent iUpdate = new Intent();
            iUpdate.setAction(Constants.ACTION_MY_STATUS_CHANGE);
            iUpdate.putExtra("id", stateId);
            context.sendBroadcast(iUpdate);
        }
        
        //update the info in the UserProvider
        String userId = String.valueOf(userStatusUpdate.getUserId());
        long id = UserProvider.getDbUserId(context, userId);
        UserProvider.updatePresence(context, id, userStatusUpdate.getStatus());
        Intent iLoadPresence = new Intent();
        iLoadPresence.setAction(Constants.ACTION_LOAD_USER_LIST);
        context.sendBroadcast(iLoadPresence);

        //let's send this info to the favorite-related intent receiver too, it could be a favorite
        Intent fav = new Intent();
        fav.setAction(Constants.ACTION_FAVORITE_STATUS_UPDATE);
        fav.putExtra("id", userStatusUpdate.getUserId());
        fav.putExtra("info", CapapresenceProvider.getColor
        		(context, userStatusUpdate.getStatus()));
        service.getApplicationContext().sendBroadcast(fav);
    }

    /**
     * Gets the config about a phone
     * @param lineId the phone ID
     */
    private void sendGetPhoneConfig(Integer lineId) {
        xivoLink.sendGetPhoneConfig(lineId);
    }

    public void setXivoLink(XiVOLink xivoLink) {
        this.xivoLink = xivoLink;
    }

    /**
     * Receives a new phone.
     * updates the {@link UserProvider} database with this phone.
     */
    @Override
    public void onPhoneConfigUpdate(PhoneConfigUpdate phoneConfigUpdate) {
        Context context = service.getApplicationContext();
        String updatedUserId = phoneConfigUpdate.getUserId().toString();
        long id = UserProvider.getDbUserId(context, updatedUserId);

        Log.d(TAG,
                "Phone config update " + phoneConfigUpdate.getNumber() + " for user : " + phoneConfigUpdate.getUserId()
                        + " internal id " + id);

        //update the info
        ContentValues values = new ContentValues();
        values.put(UserProvider.PHONENUM, phoneConfigUpdate.getNumber());
        values.put(UserProvider.LINEID, phoneConfigUpdate.getId().toString());
        context.getContentResolver().update(Uri.parse(UserProvider.CONTENT_URI + "/" + id), values, null, null);
        Intent iUpdateIntent = new Intent();
        iUpdateIntent.setAction(Constants.ACTION_LOAD_USER_LIST);
        iUpdateIntent.putExtra("id", id);
        context.sendBroadcast(iUpdateIntent);
        
        //inform the connection service that one less contact is to go
        if(Connection.INSTANCE != null
        		&& Connection.INSTANCE.getConnection(service.getApplicationContext()) != null) {
        	try {
				Connection.INSTANCE.getConnection(service.getApplicationContext())
					.decrementContactsToGo();
			} catch (RemoteException e) {
				e.printStackTrace();
			}
        }
    }

    /**
     * Called when a phone status changes.
     * Updates the {@link UserProvider} database.
     */
    @Override
    public void onPhoneStatusUpdate(PhoneStatusUpdate phoneStatusUpdate) {
        Context context = service.getApplicationContext();
        long updatedUserId = UserProvider.getDbUserIdFromLineId(context, phoneStatusUpdate.getLineId().toString());
        long updatedXiVOUserId = UserProvider
                .getXiVOUserIdFromLineId(context, phoneStatusUpdate.getLineId().toString());
        Log.d(TAG,
                "User : [" + updatedUserId + "] XiVO(" + updatedXiVOUserId + ") Phone " + phoneStatusUpdate.getLineId()
                        + " status updated [" + phoneStatusUpdate.getHintStatus() + "]");
        ContentValues values = new ContentValues();
        for (PhoneStatus phoneStatus : capacities.getPhoneStatuses()) {
            if (phoneStatus.getId().equals(phoneStatusUpdate.getHintStatus())) {
            	//we found the phone status in the capacities!
            	//so put the new values
                values.put(UserProvider.HINTSTATUS_CODE, phoneStatus.getId());
                values.put(UserProvider.HINTSTATUS_COLOR, phoneStatus.getColor());
                values.put(UserProvider.HINTSTATUS_LONGNAME, phoneStatus.getLongName());
                Log.d(TAG, "    Phone Status [" + phoneStatus.getId() + "] " + phoneStatus.getColor() + " "
                        + phoneStatus.getLongName());
                context.getContentResolver().update(Uri.parse(UserProvider.CONTENT_URI + "/" + updatedUserId), values,
                        null, null);
                if (updatedXiVOUserId == this.userId) {
                	//notify the intent receiver that I changed too
                    userPhoneStatus = new PhoneStatus(phoneStatus.getId(), phoneStatus.getColor(),
                            phoneStatus.getLongName());
                    Intent i = new Intent();
                    i.setAction(Constants.ACTION_MY_PHONE_CHANGE);
                    i.putExtra("color", phoneStatus.getColor());
                    i.putExtra("longname", phoneStatus.getLongName());
                    context.sendBroadcast(i);
                }

                //let's send this info to the favorite-related intent receiver too, it could be a favorite
                Intent fav = new Intent();
                fav.setAction(Constants.ACTION_FAVORITE_PHONE_UPDATE);
                fav.putExtra("id", (int)updatedXiVOUserId);
                fav.putExtra("info", phoneStatus.getColor());
                service.getApplicationContext().sendBroadcast(fav);
            }
        }
        
        //inform the contacts Xlet that the list changed
        Intent iUpdateIntent = new Intent();
        iUpdateIntent.setAction(Constants.ACTION_LOAD_USER_LIST);
        iUpdateIntent.putExtra("id", updatedUserId);
        context.sendBroadcast(iUpdateIntent);
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public void setCapacities(Capacities capacities) {
        this.capacities = capacities;
    }

	@Override
	public void onAgentConfigUpdate(AgentConfigUpdate arg0) {
		Log.w("UserUpdateManager", "Agent Config Update lost: "+arg0.getFirstName()+" "+arg0.getLastName());
	}

	@Override
	public void onQueueConfigUpdate(QueueConfigUpdate arg0) {
		Log.w("UserUpdateManager", "Queue Config Update lost: "+arg0.getDisplayName());
		
	}

	@Override
	public void onQueueMemberConfigUpdate(QueueMemberConfigUpdate arg0) {
		Log.w("UserUpdateManager", "Queue Member Config Update lost: "+arg0.getQueueName());
		
	}

	@Override
	public void onQueueStatusUpdate(QueueStatusUpdate arg0) {
		Log.w("UserUpdateManager", "Queue Status Update lost: "+arg0.getAgentMembers().size());
		
	}

	@Override
	public void onQueueMemberRemoved(QueueMemberRemoved arg0) {
		
	}

}
