/* XiVO Client Android
 * Copyright (C) 2010-2011, Proformatique
 *
 * This file is part of XiVO Client Android.
 *
 * XiVO Client Android is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * XiVO Client Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with XiVO client Android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.proformatique.android.xivoclient.xlets;

import java.text.Normalizer;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.net.sip.SipException;
import android.net.sip.SipManager;
import android.os.Bundle;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Contacts;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.proformatique.android.xivoclient.Connection;
import com.proformatique.android.xivoclient.NavigationDrawerActivity;
import com.proformatique.android.xivoclient.R;
import com.proformatique.android.xivoclient.SettingsActivity;
import com.proformatique.android.xivoclient.dao.UserProvider;
import com.proformatique.android.xivoclient.sip.SipApiUser;
import com.proformatique.android.xivoclient.tools.Constants;
import com.proformatique.android.xivoclient.tools.GraphicsManager;

/**
 * "User List" screen.
 */
public class XletContactSearch extends NavigationDrawerActivity implements OnItemClickListener, OnItemLongClickListener{
	
	private static final String LOG_TAG = "XiVO search";
	
	/**
	 * This {@link Cursor} always contains the displayed list, and is moved when a contact is selected.
	 */
	private Cursor user;
	private UserAdapter userAdapter = null;
	private IncomingReceiver receiver;
	
	/**
	 * Set to true if the last touch was on the favorite column.
	 */
	private boolean isTouchFavorite = false;
	
	/*
	 * UI
	 */
	private EditText et;
	private ListView lv;
	private Button searchButton;
	
    /**
     * true = order by name
     * false = order by telephone number
     */
    private boolean isOrderingByName;
    
    /**
     * true = ordering in alphabetic/ascending order
     * false = ordering by reverse alphabetic/descending order
     */
    private boolean isOrderingAscendent;
    
    /**
     * true = favorites FIRST, non-favorites AFTER
     * false = don't take favorites into account when ordering
     */
    private boolean isOrderingFavorites;
	
    /**
     * Inits the default values for sorting, and registers receivers/listeners.
     */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        
		//default: order by name, alphabetical order, without favorites
        isOrderingAscendent = true;
        isOrderingByName = true;
        isOrderingFavorites = false;
		
		Intent i = getIntent();
		
		if(i.getBooleanExtra("duringCall", false)) {
			//tells the app not to display the navigation drawer, and to close after launching a call
			index = SPECIAL_CONTACT_DURING_CALL;
			//set the same flags as the call screen (the user should not have to unlock the phone
			//between the call screen and the contact list)
	        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
	        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
		} else if(i.getBooleanExtra("contactFromDialer", false)) {
			//do not display the navigation drawer ; clicking does edit the contact by default.
			index = SPECIAL_CONTACT_FROM_DIALER;
		} else {
			index = 2;
		}
		
		//set up the view
		setContentView(R.layout.xlet_search);
		setUpNavigationDrawer(R.id.navigation_drawer_search, R.id.drawer_layout_search);
		
		//init variables
		et = (EditText)findViewById(R.id.SearchEdit);
		lv = (ListView) findViewById(R.id.users_list);
		searchButton = (Button) findViewById(R.id.button_search_contacts);
		
		//set up the list view
		userAdapter = new UserAdapter();
		lv.setAdapter(userAdapter);
		lv.setOnItemClickListener(this);
		lv.setOnItemLongClickListener(this);
		
		/**
		 *  Register a BroadcastReceiver for Intent action that trigger a change
		 *  in the users list from the Activity
		 */
		receiver = new IncomingReceiver();
		IntentFilter filter = new IntentFilter();
		filter.addAction(Constants.ACTION_LOAD_USER_LIST);
		filter.addAction(Constants.ACTION_REFRESH_USER_LIST);
		registerReceiver(receiver, new IntentFilter(filter));
		
		registerForContextMenu(findViewById(R.id.users_list));
		
		//set up the search field
		et.addTextChangedListener(
				new TextWatcher() {
					public void afterTextChanged(Editable s) {
						String str = s.toString();
						//if the user has just pressed Enter, we are going to find a
						//"new line" character inside the string.
						if(str.contains("\n")) {
							//then, remove this new line and hide the keyboard.
							Log.i("Keyboard", "I'm going to hide!");
							InputMethodManager imm = 
					          (InputMethodManager)XletContactSearch.this.getApplicationContext()
					         		.getSystemService(Context.INPUT_METHOD_SERVICE);
					           
					        imm.hideSoftInputFromWindow(et.getWindowToken(), 0);
					           
					        //and now, remove all these \n things
					        str = str.replace("\n", "");
					        et.setText(str);
						}
					}
					public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
					public void onTextChanged(CharSequence s, int start, int before, int count) {
						Intent definedIntent = new Intent();
						definedIntent.setAction(Constants.ACTION_REFRESH_USER_LIST);
						XletContactSearch.this.sendBroadcast(definedIntent);
					}
				}
		);
		
		//set up the search button
		Button searchButton = (Button) findViewById(R.id.button_search_contacts);
		searchButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent contactPickerIntent = new Intent(Intent.ACTION_PICK,
						Contacts.CONTENT_URI);
				startActivityForResult(contactPickerIntent,
						Constants.CONTACT_PICKER_RESULT);
			}
		});
		
		registerButtons(); //from the XivoActivity

		//set up the button to erase the search field
		View croix = findViewById(R.id.erase_them_all);
		croix.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				et.setText("");
			}
		});
		
		//detect where touch has taken place, so that we can refresh the isTouchFavorite variable
		lv.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				Display d = getWindowManager().getDefaultDisplay();
				@SuppressWarnings("deprecation") //must use it to keep backwards compatibility
				int width = d.getWidth();
				Resources r = getResources();
				float px = 0;
				int screenSize = (getResources().getConfiguration().screenLayout & 
	            	    Configuration.SCREENLAYOUT_SIZE_MASK);
				//if the screen is large, the star width is 50 dp, else it is 36. So what is the size?
	            if(screenSize == Configuration.SCREENLAYOUT_SIZE_LARGE ||
	               screenSize == Configuration.SCREENLAYOUT_SIZE_XLARGE) {
	            	px = TypedValue.applyDimension
							(TypedValue.COMPLEX_UNIT_DIP, 50, r.getDisplayMetrics());
	            } else {
					px = TypedValue.applyDimension
							(TypedValue.COMPLEX_UNIT_DIP, 36, r.getDisplayMetrics());
	            }
				//if distance from the border is < 36 dp, we are on the "favorite" icon.
				isTouchFavorite = (width - event.getX()) < px;
				Log.v("Xlet Contact Search", isTouchFavorite ? "Touched the favorite icon!!" : 
					"Touched something else ...");
				
				//continue to consume events
				return false;
			}
		});
		
		//set up the "name" column header
		View.OnClickListener listener = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(isOrderingByName) {
					//reverse the order
					isOrderingAscendent = !isOrderingAscendent;
					((ImageView) findViewById(R.id.userNameOrder)).setImageResource(
							isOrderingAscendent ? R.drawable.sort_asc : R.drawable.sort_desc);
				} else {
					//select ordering by name and ascending by default
					isOrderingByName = true;
					isOrderingAscendent = true;
					((ImageView) findViewById(R.id.phoneNumberOrder)).setImageResource(
							R.drawable.nothing);
					((ImageView) findViewById(R.id.userNameOrder)).setImageResource(
							R.drawable.sort_asc);
				}
				//and refresh the user list
				getUserList();
			}
		};
		findViewById(R.id.userNameHeader).setOnClickListener(listener);
		findViewById(R.id.userNameOrder).setOnClickListener(listener);
		
		//set up the "phone" column header
		listener = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(!isOrderingByName) {
					//reverse the order
					isOrderingAscendent = !isOrderingAscendent;
					((ImageView) findViewById(R.id.phoneNumberOrder)).setImageResource(
							isOrderingAscendent ? R.drawable.sort_asc : R.drawable.sort_desc);
				} else {
					//select ordering by number and ascending by default
					isOrderingByName = false;
					isOrderingAscendent = true;
					((ImageView) findViewById(R.id.userNameOrder)).setImageResource(
							R.drawable.nothing);
					((ImageView) findViewById(R.id.phoneNumberOrder)).setImageResource(
							R.drawable.sort_asc);
				}
				//and refresh the user list
				getUserList();
			}
		};
		findViewById(R.id.phoneNumberHeader).setOnClickListener(listener);
		findViewById(R.id.phoneNumberOrder).setOnClickListener(listener);
		
		//set up the "favorite" column header
		findViewById(R.id.favoriteOrder).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				isOrderingFavorites = !isOrderingFavorites;
				((ImageView) findViewById(R.id.favoriteOrder)).setImageResource(
						isOrderingFavorites ? R.drawable.favorite : R.drawable.not_favorite);
				getUserList();
			}
		});
	}
	
	/**
	 * An Android contact has been selected, now we should display a menu to call him or edit his number.
	 */
	protected void onActivityResult(
			int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			switch (requestCode) {
			case Constants.CONTACT_PICKER_RESULT:
				Map<String, String> contact = getPickedContact(data);
				promptAfterSelection(contact);
				break;
			}
		} else {
			Log.w(LOG_TAG, "Warning: activity result not ok");
		}
	}
	
	/**
	 * Takes the result of the Android contact picker, and converts it to a {@link Map} compatible with the 
	 * promptAfterSelection method (like if another XiVO contact had been selected).
	 * @param data The {@link Intent} coming from the Android contact picker
	 * @return A {@link Map} that can be used to show the alert dialog with the same methods as a XiVO contact.
	 */
	private Map<String, String> getPickedContact(Intent data) {
		Uri result = data.getData();
		String id = result.getLastPathSegment();
		Cursor cursor = getContentResolver().query(
				Phone.CONTENT_URI, null, Phone.CONTACT_ID + "=?",new String[] {id}, null);
		if (cursor.getCount() > 0) {
			HashMap<String, String> contact = new HashMap<String, String>();
			cursor.moveToFirst();
			do {
					String[] cols = cursor.getColumnNames();
					String number = "";
					for (String col: cols) {
						int index = cursor.getColumnIndex(col);
						if (col.equals("display_name")) {
							contact.put("Name", cursor.getString(index));
						} else if (col.equals("data1")) {
							number = cursor.getString(index).replace(" ", "").replace("-", "");
						} else if (col.equals("data2")) {
							contact.put((String) Phone.getTypeLabel(
									this.getResources(), cursor.getInt( cursor.getColumnIndex(
											Phone.TYPE)), "unknown"), number);
						}
					}
				
			} while (cursor.moveToNext());
			cursor.close();
			return contact;
		} else {
			cursor.close();
			return null;
		}
	}
	
	/**
	 * Refresh the user list on activity resume.
	 */
	protected void onResume() {
		//show android contacts if the user absolutely wants to
		if(PreferenceManager.getDefaultSharedPreferences(this).getBoolean("android_contacts", false))
			searchButton.setVisibility(View.VISIBLE);
		else
			searchButton.setVisibility(View.GONE);
		
		super.onResume();
		getUserList();
	}
	
	/**
	 * Adapter used to show the user list from the "user" {@link Cursor}.
	 */
	private class UserAdapter extends BaseAdapter {
		
		private final static String TAG = "User Adapter";
		
		public UserAdapter() {
			Log.d(TAG, "User adapter");
			Log.d(TAG, user == null ? "0" : Long.toString(user.getCount()));
		}
		
		/**
		 * Get the user count.
		 */
		@Override
		public int getCount() {
			return user != null ? user.getCount() : 0;
		}
		
		/**
		 * Get a Bundle corresponding to the user.
		 */
		@Override
		public Object getItem(int position) {
			user.moveToPosition(position);
			return user.getExtras();
		}
		
		/**
		 * Get the user id at the given position.
		 */
		@Override
		public long getItemId(int position) {
			user.moveToPosition(position);
			return user.getLong(user.getColumnIndex(UserProvider._ID));
		}
		
		/**
		 * Get the view at the given position.
		 */
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
		    Log.d(TAG, "User adapter view " + position);
		    
			View v; //use the provided view or do one yourself
			if (convertView == null) {
				final LayoutInflater inflater = LayoutInflater.from(XletContactSearch.this);
				v = inflater.inflate(R.layout.xlet_search_items, null);
			} else {
				v = convertView;
			}
			user.moveToPosition(position);
			
			// Contact info
			((TextView) v.findViewById(R.id.fullname)).setText(
					user.getString(user.getColumnIndex(UserProvider.FULLNAME)));
			((TextView) v.findViewById(R.id.phonenum)).setText(
					user.getString(user.getColumnIndex(UserProvider.PHONENUM)));
			
			// Contact Status
			ImageView iconState = (ImageView) v.findViewById(R.id.statusContact);
			((TextView) v.findViewById(R.id.longname_state)).setText(
					user.getString(user.getColumnIndex(UserProvider.STATEID_LONGNAME)));
			GraphicsManager.setIconStateDisplay(
					XletContactSearch.this, iconState, user.getString(
							user.getColumnIndex(UserProvider.STATEID_COLOR)));
			
			// Phone status
			ImageView iconPhone = (ImageView) v.findViewById(R.id.phoneStatusContact);
			((TextView) v.findViewById(R.id.phone_longname_state)).setText(
					user.getString(user.getColumnIndex(UserProvider.HINTSTATUS_LONGNAME)));
			GraphicsManager.setIconPhoneDisplay(
					XletContactSearch.this, iconPhone, user.getString(
							user.getColumnIndex(UserProvider.HINTSTATUS_COLOR)));
			
			//favorite icon			
			try {
				if(xivoConnectionService == null && Connection.INSTANCE != null) {
					xivoConnectionService = Connection.INSTANCE.getConnection(getApplicationContext());
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				
				//ask to the service if this contact is a favorite, based on his user id
				if(xivoConnectionService != null && 
						!xivoConnectionService.isFavorite(Integer.parseInt(user.getString
						(user.getColumnIndex(UserProvider.XIVO_USERID))))) {
					ImageView iconFav = (ImageView) v.findViewById(R.id.favorite_icon);
					iconFav.setImageResource(R.drawable.not_favorite);
				} else {
					ImageView iconFav = (ImageView) v.findViewById(R.id.favorite_icon);
					iconFav.setImageResource(R.drawable.favorite);
				}
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (RemoteException e) {
				e.printStackTrace();
			}
			
			return v;
		}
	}
	
	/**
	 * BroadcastReceiver, intercept Intents with action ACTION_LOAD_USER_LIST
	 * to perform an reload of the displayed list
	 * @author cquaquin
	 *
	 */
	public class IncomingReceiver extends BroadcastReceiver {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals(Constants.ACTION_LOAD_USER_LIST)) {
				Log.d( LOG_TAG , "Received Broadcast ");
				getUserList();
				userAdapter.notifyDataSetChanged();
			} else if (intent.getAction().equals(Constants.ACTION_REFRESH_USER_LIST)) {
				Log.d(LOG_TAG, "Received search Broadcast");
				getUserList();
			}
		}
	}
	
	/**
	 * Refresh the user list, sort it as requested by the user, and notify the list adapter
	 */
	private void getUserList() {
	    Log.d(LOG_TAG,"**************** get user list ***********************");
		if (user != null && user.isClosed() == false) {
			user.close();
			user = null;
		}

    	//NORM_NAME is "unaccented", so "unaccent" the search pattern as well
    	//Normalizer transforms � into a`, and replaceAll removes the `
		String unaccent = Normalizer.normalize(et.getText(), Normalizer.Form.NFD)
				.replaceAll("[\u0300-\u036F]", "");
		
		try {
			if(xivoConnectionService != null)
				user = getContentResolver().query(UserProvider.CONTENT_URI, null,
						UserProvider.NORM_NAME + " LIKE '%" + unaccent + "%' AND "
					+   UserProvider.XIVO_USERID + " NOT LIKE "+xivoConnectionService.getUserId()+""
		+ " AND "   +   UserProvider.PHONENUM  + " NOT LIKE '....'",
						null, (isOrderingByName ? UserProvider.NORM_NAME : UserProvider.PHONENUM)
						+ (isOrderingAscendent ? " ASC" : " DESC"));
			else //never mind if the user is in the list, the app would crash anyway
				 //as the service is not bound to the app at this time.
				user = getContentResolver().query(UserProvider.CONTENT_URI, null,
						UserProvider.NORM_NAME + " LIKE '%" + unaccent + "%' AND "
					   +UserProvider.PHONENUM  + " NOT LIKE '....'",
						null, (isOrderingByName ? UserProvider.NORM_NAME : UserProvider.PHONENUM)
						+ (isOrderingAscendent ? " ASC" : " DESC"));
		} catch (RemoteException e) {
			
			e.printStackTrace();
		}
		
		//we should have the favorites first in the list.
		//so, we will make it by creating a new Cursor, adding all the favorites first, then the non-favorites.
		if(user != null && isOrderingFavorites && xivoConnectionService != null) {
			MatrixCursor newCursor = new MatrixCursor(UserProvider.columnList);
			
			//here, add the favorites to the new cursor first
			if(user.moveToFirst()) {
				do {
					try {
						if(xivoConnectionService.isFavorite(Integer.parseInt(user.getString(
								user.getColumnIndex(UserProvider.XIVO_USERID))))) {
							Object[] toAdd = new Object[UserProvider.columnList.length];
							for(int i = 0; i < UserProvider.columnList.length; i++) {
								//all the columns are Strings, but the first one.
								if(i == 0) toAdd[i] = user.getInt(i);
								else toAdd[i] = user.getString(i);
							}
							newCursor.addRow(toAdd);
						}
					} catch (RemoteException e) {
						e.printStackTrace();
					}
				} while(user.moveToNext());
			}
			
			//then, add the non-favorites
			if(user.moveToFirst()) {
				do {
					try {
						if(!xivoConnectionService.isFavorite(Integer.parseInt(user.getString(
								user.getColumnIndex(UserProvider.XIVO_USERID))))) {
							//add all the favorites first
							Object[] toAdd = new Object[UserProvider.columnList.length];
							for(int i = 0; i < UserProvider.columnList.length; i++) {
								//all the columns are Strings, but the first one.
								if(i == 0) toAdd[i] = user.getInt(i);
								else toAdd[i] = user.getString(i);
							}
							newCursor.addRow(toAdd);
						}
					} catch (RemoteException e) {
						e.printStackTrace();
					}
				} while(user.moveToNext());
			}
			//close the cursor before killing it
			user.close();
			
			//now, replace the cursor with the one I just ordered!
			user = newCursor;
		}
		
		//finally notify the list that something changed.
		userAdapter.notifyDataSetChanged();
	}
	
	/**
	 * Unregister the receiver and destroy this activity
	 */
	@Override
	protected void onDestroy() {
		unregisterReceiver(receiver);
		if (user != null)
			user.close();
		super.onDestroy();
	}
	
	/**
	 * When an item on the list is long clicked, show the menu.
	 */
	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
		user.moveToPosition(position);
		String phoneNumber = user.getString(user.getColumnIndex(UserProvider.PHONENUM));
		String name = user.getString(user.getColumnIndex(UserProvider.FULLNAME));
		Log.d(LOG_TAG, "Clicked: " + name + " " + phoneNumber);
		HashMap<String, String> contact = new HashMap<String, String>();
		contact.put("Name", name);
		contact.put(getString(R.string.xivo_phone_type_lbl), phoneNumber);
		try {
			promptAfterSelection(contact, xivoConnectionService
					.isFavorite(Integer.parseInt(user.getString
					(user.getColumnIndex(UserProvider.XIVO_USERID)))));
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return true;
	}

	/**
	 * Toggle the favorite state of the user or call him if a user is clicked.
	 */
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		if(!isTouchFavorite) { //call the user
			user.moveToPosition(position);
			String phoneNumber = user.getString(user.getColumnIndex(UserProvider.PHONENUM));
			String name = user.getString(user.getColumnIndex(UserProvider.FULLNAME));
			Log.d(LOG_TAG, "Clicked: " + name + " " + phoneNumber);
			if(index != SPECIAL_CONTACT_FROM_DIALER)
				call(phoneNumber);
			else {
				//in fact, we should send him back to the dialer and commit suicide after that
				//because we come from the dialer.
				Intent i = new Intent();
				i.putExtra("number", phoneNumber);
				setResult(RESULT_OK, i);
				finish();
			}
		} else { //toggle the user's favorite status
			user.moveToPosition(position);
			try {
				if(xivoConnectionService.isFavorite(Integer.parseInt(user.getString
						(user.getColumnIndex(UserProvider.XIVO_USERID))))) {
					//it IS a favorite, so remove it now!
					xivoConnectionService.removeFavorite(
							Integer.parseInt(user.getString(user.getColumnIndex
									(UserProvider.XIVO_USERID))));
				} else {
					//collect the info about the user, create a new Favorite with that, and add it!
					int userId = Integer.parseInt(user.getString
							(user.getColumnIndex(UserProvider.XIVO_USERID)));
					String userName = user.getString
							(user.getColumnIndex(UserProvider.FULLNAME));
					String userState = user.getString
							(user.getColumnIndex(UserProvider.STATEID_COLOR));
					String phoneState = user.getString
							(user.getColumnIndex(UserProvider.HINTSTATUS_COLOR));
					xivoConnectionService.addFavorite
						(userId, phoneState, userState, userName);
				}
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (RemoteException e) {
				e.printStackTrace();
			}
			getUserList();
		}
	}
	
	/**
	 * Prompt the user to call or transfer to one of his number when selected on the list
	 * or picked on the Android contact picker, with no "add favorite" option.
	 * @param contact The selected contact 
	 */
	private void promptAfterSelection(Map<String, String> contact) {
		promptAfterSelection(contact, false, false);
	}
	
	/**
	 * Prompt the user to call or transfer to one of his number when selected on the list
	 * or picked on the Android contact picker, with the "add favorite" button.
	 * @param contact The selected contact 
	 * @param isFav Is the selected contact a favorite?
	 */
	private void promptAfterSelection(Map<String, String> contact, boolean isFav) {
		promptAfterSelection(contact, isFav, true);
	}
	
	/**
	 * Prompt the user to call or transfer to one of his number when selected on the list
	 * or picked on the Android contact picker
	 * @param contact The selected contact 
	 * @param isFav Is the selected contact a favorite?
	 * @param hasFav Should we display the "add favorite" option?
	 */
	private void promptAfterSelection(Map<String, String> contact, final boolean isFav, final boolean hasFav) {
		if (contact == null)
			return;
		
		//the list of items to display.
		String[] items = null;
		try {
			int i = 0;
			if (xivoConnectionService.isOnThePhone()) {
				//transfer options...
				items = new String[(contact.size() - 1) * 2 + (hasFav ? 2 : 1)];
				for (String key: contact.keySet()) {
					if (key.equals("Name"))
						continue;
					items[i++] = String.format(
							getString(R.string.context_action_atxfer), key, contact.get(key));
					items[i++] = String.format(
							getString(R.string.context_action_transfer), key, contact.get(key));
					if(hasFav)
						items[i++] = getString(isFav ? R.string.notification_rem_favorites :
							R.string.notification_add_favorites);
				}
			} else {
				//call options...
				items = new String[(contact.size() - 1) * 2 + (hasFav ? 2 : 1)];
				for (String key: contact.keySet()) {
					if (key.equals("Name"))
						continue;
					items[i++] = String.format(
							getString(R.string.context_action_call), contact.get(key));
					items[i++] = String.format(
					        getString(R.string.context_edit_before_call), contact.get(key));
					if(hasFav)
						items[i++] = getString(isFav ? R.string.notification_rem_favorites :
							R.string.notification_add_favorites);
				}
			}
			items[i] = getString(R.string.cancel_label);
		} catch (RemoteException e) {
			Toast.makeText(XletContactSearch.this, getString(R.string.remote_exception),
					Toast.LENGTH_SHORT).show();
			return;
		}
		
		//now, display this list in an AlertDialog
		final String[] menuItems = items;
		new AlertDialog.Builder(this)
		.setTitle(contact.get("Name"))
		.setItems(menuItems,
			new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int i) {
					if (i == menuItems.length - 1) {
						Log.d(LOG_TAG, "Cancel selected");
						return;
					}
					if(i == menuItems.length - 2 && hasFav) {
						//add / remove favorite button.
						try {
							if(isFav) {
								xivoConnectionService.removeFavorite(
										Integer.parseInt(user.getString(user.getColumnIndex
												(UserProvider.XIVO_USERID))));
							} else {
								int userId = Integer.parseInt(user.getString
										(user.getColumnIndex(UserProvider.XIVO_USERID)));
								String userName = user.getString
										(user.getColumnIndex(UserProvider.FULLNAME));
								String userState = user.getString
										(user.getColumnIndex(UserProvider.STATEID_COLOR));
								String phoneState = user.getString
										(user.getColumnIndex(UserProvider.HINTSTATUS_COLOR));
								xivoConnectionService.addFavorite
									(userId, phoneState, userState, userName);
							}
							getUserList();
						} catch(RemoteException e) {}
						return;
					}
					String number = menuItems[i].substring(
							menuItems[i].indexOf("(") + 1, menuItems[i].indexOf(")"))
							.replace("-", "").trim();
					try {
						if (xivoConnectionService.isOnThePhone()) {
							//transfer options
							if (i % 2 == 1) {
								xivoConnectionService.transfer(number);
							} else {
								xivoConnectionService.atxfer(number);
							}
						} else {
							//call options
							if(i % 2 == 1) {
								if(index != SPECIAL_CONTACT_FROM_DIALER) {
									//edit number in XletDialer
									Intent intent = new Intent(XletContactSearch.this, XletDialer.class);
									intent.putExtra("numToCall", number);
									intent.putExtra("edit", true);
									if(index == SPECIAL_CONTACT_DURING_CALL) {
										intent.putExtra("duringCall", true);
										finish();
									}
									startActivity(intent);
								} else {
									//don't launch XletDialer again, it is in the background waiting for a number!
									//just give it to him!
									Intent intent = new Intent();
									intent.putExtra("number", number);
									setResult(RESULT_OK, intent);
									finish();
								}
							} else {
								call(number);
							}
						}
						
					} catch (RemoteException e) {
						Toast.makeText(XletContactSearch.this,
								getString(R.string.remote_exception), Toast.LENGTH_SHORT).show();
					}
				}
			}).show();
	}
    
	/**
	 * set enabled state of the list and of the button
	 */
    @Override
    protected void setUiEnabled(boolean state) {
        super.setUiEnabled(state);
        if (lv != null) {
            lv.setEnabled(state);
        }
        if (searchButton != null) {
            searchButton.setEnabled(state);
        }
    }
    
    /**
     * Call the phone number using the most appropriate method
     * @param phoneNumber the number to call
     */
    private void call(String phoneNumber) {
    	//see the BootToCall.call method for more comments.
    	try {
			if(xivoConnectionService.getUsesSipNumb(phoneNumber)) {
				Log.i("call() method", "Calling "+phoneNumber+" using SIP!");
				try {
					if(SipApiUser.INSTANCE != null) {
						SipApiUser.INSTANCE.call(phoneNumber);
					    if(index == SPECIAL_CONTACT_DURING_CALL) finish();
					} else {
						boolean isSipCompat = SipManager.isApiSupported(this) && SipManager.isVoipSupported(this);
						AlertDialog.Builder builder = new AlertDialog.Builder(XletContactSearch.this);
						builder.setTitle(R.string.sip_error);
						SharedPreferences prefs = 
				        		PreferenceManager.getDefaultSharedPreferences(this);
						if(prefs.getString("HOME_SSID", "").length() == 0)
							builder.setMessage(R.string.configure_ssid);
						else if(isSipCompat)
							builder.setMessage(SipApiUser.badSettings ? R.string.sip_config : R.string.sip_not_set);
						else
							builder.setMessage(R.string.were_doomed);
						
						builder.setNegativeButton(R.string.ok, null);
						
						if(isSipCompat)
							builder.setPositiveButton(R.string.settings, 
									new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									Intent i = new Intent(XletContactSearch.this, SettingsActivity.class);
									i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
									startActivity(i);
								}
							});
						builder.show();
					}
						
						//Toast.makeText(XletContactSearch.this, getString(R.string.sip_not_set), Toast.LENGTH_LONG).show();
				} catch (SipException e) {
					e.printStackTrace();
				} catch (ParseException e) {
					e.printStackTrace();
				}
			} else {
				//old method ; pops up the XletDialer that will make the call itself
			    Intent iCall = new Intent(XletContactSearch.this, XletDialer.class);
			    iCall.putExtra("numToCall", phoneNumber);
			    startActivity(iCall);
			}
		} catch (RemoteException e) {
			//some exception thrown for no reason...
			e.printStackTrace();
		}
    }
}
