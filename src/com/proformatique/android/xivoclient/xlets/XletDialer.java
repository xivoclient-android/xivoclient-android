/* XiVO Client Android
 * Copyright (C) 2010-2011, Proformatique
 *
 * This file is part of XiVO Client Android.
 *
 * XiVO Client Android is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * XiVO Client Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with XiVO client Android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.proformatique.android.xivoclient.xlets;

import java.text.ParseException;
import java.util.Arrays;
import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.PorterDuff;
import android.net.sip.SipException;
import android.net.sip.SipManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.InputType;
import android.text.Layout;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.proformatique.android.xivoclient.NavigationDrawerActivity;
import com.proformatique.android.xivoclient.R;
import com.proformatique.android.xivoclient.SettingsActivity;
import com.proformatique.android.xivoclient.dao.UserProvider;
import com.proformatique.android.xivoclient.sip.SipApiUser;
import com.proformatique.android.xivoclient.tools.Constants;

/**
 * Shows a dialer, allowing the user to make phone calls.
 * This Activity also handles all the "Callback" calls.
 */
public class XletDialer extends NavigationDrawerActivity {
    
    private static final String LOG_TAG = "XiVO Dialer";
    private final int VM_DISABLED_FILTER = 0xff555555;
    
    /**
     * If this variable is true, every incoming call will be picked up.
     */
    public static boolean autoPickup = false;
    //UI
    private EditText phoneNumber;
    private ImageButton dialButton;
    //receiver
    private IncomingReceiver receiver;
    private Handler mHandler = new Handler();
    
    /**
     * As the "during call" interface is disabled, I set a VERY long time for the hangup button to refresh...
     * as it is useless to refresh it.
     */
    private final static long UPDATE_DELAY = 20000;
    
    /**
     * If this variable is true, the dialer will close after having run the callback.
     */
    private boolean isAutoClose = false;
    
    /**
     * This Runnable updates the hangup button to be red or green depending on the phone state.
     */
    private Runnable updateHangup = new Runnable() {
        @Override
        public void run() {
            refreshHangupButton();
            mHandler.postDelayed(updateHangup, UPDATE_DELAY);
        }
    };
    
    /**
     * Displayed to show the state of the callback.
     */
    private static Dialog dialog;
    private static String dialogText;
    
    private static CallTask callTask = null;
    private PhoneStateListener phoneStateListener = null;
    private TelephonyManager telephonyManager = null;
    
    private final List<String> sOnThePhoneCode = Arrays.asList(new String[] {"1", "8", "9", "16"});
    
    /**
     * Inits the variables and registers listeners.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	Log.d("Xlet Dialer", "Activity created");
    	
        super.onCreate(savedInstanceState);

        //set up the UI
        setContentView(R.layout.xlet_dialer);
        setUpNavigationDrawer(R.id.navigation_drawer_dialer, R.id.drawer_layout_dialer);

		Intent i = getIntent();
        if(i.getBooleanExtra("duringCall", false)) {
			//tells the app not to display the navigation drawer, and to close after launching a call
			index = SPECIAL_CONTACT_DURING_CALL;
			//set the same flags as the call screen (the user should not have to unlock the phone
			//between the call screen and the contact list)
	        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
	        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
		} else {
			index = 1;
		}
        
        //get the config from the intent
        isAutoClose = i.getBooleanExtra("autoClose", false);
        if(i.getBooleanExtra("wasNotConnected", false)) askedToDisconnect = true; //no auto-connect!
        
        //init the variables
        phoneNumber = (EditText) findViewById(R.id.number);
        dialButton = (ImageButton) findViewById(R.id.dialButton);
        refreshHangupButton();
        registerButtons();
        
        //get the dialed number and the edit mode.
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            phoneNumber.setText(bundle.getString("numToCall"));
            if (!bundle.getBoolean("edit")) {
            	//we will go back after launching the call.
            	isAutoClose = true;
            	
            	//but call anyway.
            	if(callTask == null || !autoPickup) {
            		Log.i("Xlet Dialer", "Launching a new call!");
	                callTask = new CallTask();
	                callTask.execute();
            	}
            }
        }
        
        //show the calling dialog if currently calling.
        if(autoPickup) showCallDialog();
        
        //listen to the phone state
        telephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
        
        //Register a BroadcastReceiver for Intent action that trigger a call
        receiver = new IncomingReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Constants.ACTION_OFFHOOK);
        filter.addAction(Constants.ACTION_MWI_UPDATE);
        filter.addAction(Constants.ACTION_CALL_PROGRESS);
        filter.addAction(Constants.ACTION_ONGOING_CALL);
        filter.addAction(Constants.ACTION_UPDATE_PEER_HINTSTATUS);
        registerReceiver(receiver, new IntentFilter(filter));
        
        //dummy out various methods for touch or long touch
        //(we don't want the keyboard to appear when we click the number text field)
        phoneNumber.setOnTouchListener(new OnTouchListener(){
        	@Override
        	public boolean onTouch(View v, MotionEvent event) {
        		//set the new cursor position
        		Layout layout = ((EditText) v).getLayout();
			    float x = event.getX() + phoneNumber.getScrollX();
			    int offset = layout.getOffsetForHorizontal(0, x);
				Log.d("Xlet Dialer", "On Touch, offset calculated is "+offset);
			    if(offset>0)
			        if(x>layout.getLineMax(0))
			         	phoneNumber.setSelection(offset);     // touch was at end of text
			        else
			          	phoneNumber.setSelection(offset - 1);
        		
	        	int inType = phoneNumber.getInputType(); // backup the input type
	        	phoneNumber.setInputType(InputType.TYPE_NULL); // disable soft input
	        	phoneNumber.onTouchEvent(event); // call native handler
	        	phoneNumber.setInputType(inType); // restore input type
	        	
	        	InputMethodManager imm = (InputMethodManager)getSystemService(
	        			Context.INPUT_METHOD_SERVICE);
	        			imm.hideSoftInputFromWindow(phoneNumber.getWindowToken(), 0);
	        	return true; // consume touch event
        	}
        });
        phoneNumber.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
	        	return true; // consume touch event
			}
		});
        
        //when long clicking the delete button, clear the text field
        findViewById(R.id.deleteButton).setOnLongClickListener(new View.OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				phoneNumber.setText("");
				return true;
			}
		});
        
        //set up the letters and numbers of all the digits
      	int[] toAdapt = { R.id.zero, R.id.one,
      			R.id.two, R.id.three, R.id.four, R.id.five, R.id.six, R.id.seven,
      			R.id.eight, R.id.nine };
      	String[] alpha = { "+", "", "ABC", "DEF", "GHI", "JKL", "MNO", "PQRS", "TUV", "WXYZ" };
      	
      	for(int i1 = 0; i1 < 10; i1++) {
      		FrameLayout that = ((FrameLayout) findViewById(toAdapt[i1]));
      		((TextView) that.findViewById(R.id.dialpad_key_number)).setText(""+i1);
      		((TextView) that.findViewById(R.id.dialpad_key_letters)).setText(alpha[i1]);
      	}
      	
      	//compatibility: clickOn0, etc. do not work anymore when declared in the XML file
    	//because of the usage of <include>, so declare them here.
    	findViewById(R.id.one).setOnClickListener(new View.OnClickListener() {
    		@Override
    		public void onClick(View v) { clickOn1(v); }
    	});
    	findViewById(R.id.two).setOnClickListener(new View.OnClickListener() {
    		@Override
    		public void onClick(View v) { clickOn2(v); }
    	});
    	findViewById(R.id.three).setOnClickListener(new View.OnClickListener() {
    		@Override
    		public void onClick(View v) { clickOn3(v); }
    	});
    	findViewById(R.id.four).setOnClickListener(new View.OnClickListener() {
    		@Override
    		public void onClick(View v) { clickOn4(v); }
    	});
    	findViewById(R.id.five).setOnClickListener(new View.OnClickListener() {
    		@Override
    		public void onClick(View v) { clickOn5(v); }
    	});
    	findViewById(R.id.six).setOnClickListener(new View.OnClickListener() {
    		@Override
    		public void onClick(View v) { clickOn6(v); }
    	});
    	findViewById(R.id.seven).setOnClickListener(new View.OnClickListener() {
    		@Override
    		public void onClick(View v) { clickOn7(v); }
    	});
    	findViewById(R.id.eight).setOnClickListener(new View.OnClickListener() {
    		@Override
    		public void onClick(View v) { clickOn8(v); }
    	});
    	findViewById(R.id.nine).setOnClickListener(new View.OnClickListener() {
    		@Override
    		public void onClick(View v) { clickOn9(v); }
    	});
    	findViewById(R.id.zero).setOnClickListener(new View.OnClickListener() {
    		@Override
    		public void onClick(View v) { clickOn0(v); }
    	});
    	findViewById(R.id.zero).setOnLongClickListener(new View.OnLongClickListener() {
    		@Override
    		public boolean onLongClick(View v) { clickOn0Long(v); return true; }
    	});
    	findViewById(R.id.star).setOnClickListener(new View.OnClickListener() {
    		@Override
    		public void onClick(View v) { clickOnStar(v); }
    	});
    	findViewById(R.id.pound).setOnClickListener(new View.OnClickListener() {
    		@Override
    		public void onClick(View v) { clickOnSharp(v); }
    	});
    }
    
    /**
     * Trigger a refresh for voice mail, then call the parent.
     * (Voice mail is unused anyway!)
     */
    @Override
    protected void onBindingComplete() {
        try {
            newVoiceMail(xivoConnectionService.hasNewVoiceMail());
        } catch (RemoteException e) {
            newVoiceMail(false);
        }
        mHandler.postDelayed(updateHangup, UPDATE_DELAY);
        super.onBindingComplete();
    }
    
    /**
     * Get if the service is ready (it exists and it is authenticated)
     * @return
     */
    private boolean isServiceReady() {
        try {
            return xivoConnectionService != null && xivoConnectionService.isAuthenticated();
        } catch (RemoteException e) {
            return false;
        }
    }
    
    /**
     * Set up the voice mail icon, depending on if there is voice mail or not.
     * @param status true if there is voice mail, false otherwise
     */
    private void newVoiceMail(final boolean status) {
        ImageButton vm_button = (ImageButton) findViewById(R.id.voicemailButton);
        vm_button.setEnabled(true);
        if (status) {
            vm_button.setColorFilter(null);
        } else {
            vm_button.setColorFilter(VM_DISABLED_FILTER, PorterDuff.Mode.SRC_ATOP);
        }
    }
    
    /**
     * Hang up a call by sending a message to the CTI server.
     */
    private void hangup() {
    	//the AndroidTools do not work anymore, and all calls are made with the mobile phone
    	Log.i("Xlet Dialer", "We should hang up the call!");
    	
    	try {
    		if (xivoConnectionService != null && xivoConnectionService.isAuthenticated())
    			xivoConnectionService.hangup();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
    }
    
    /**
     * Starts a new CallTask
     */
    private void call() {
        if (isServiceReady()) {
        	if(callTask == null || !autoPickup) {
        		Log.i("Xlet Dialer", "Launching a new call!");
                callTask = new CallTask();
                callTask.execute();
        	} else {
        		//just display the calling dialog
        		Log.i("Xlet Dialer", "Displaying dialog");
        		showCallDialog();
        	}
        } else {
            Toast.makeText(this, getString(R.string.service_not_ready), Toast.LENGTH_SHORT).show();
            Log.d(LOG_TAG, "Calling before the service is ready, aborting");
        }
    }
    
    /**
     * Direct transfer a call with the number currently entered
     */
    private void transfer() {
        if (isServiceReady()) {
            try {
                xivoConnectionService.transfer(phoneNumber.getText().toString());
                return;
            } catch (RemoteException e) {
                // Checked by isServiceReady
                e.printStackTrace();
            }
        }
        Toast.makeText(this, getString(R.string.service_not_ready), Toast.LENGTH_SHORT).show();
    }
    
    /**
     * Attended transfer a call with the number currently entered
     */
    private void atxfer() {
        if (isServiceReady()) {
            try {
                xivoConnectionService.atxfer(phoneNumber.getText().toString());
                return;
            } catch (RemoteException e) {
                // Checked by isServiceReady
                e.printStackTrace();
            }
        }
        Toast.makeText(this, getString(R.string.service_not_ready), Toast.LENGTH_SHORT).show();
    }
    
    /**
     * Called when the "call" button is pressed.
     * @param v
     */
    public void clickOnCall(View v) {
        if (phoneNumber.getText().toString().equals("")) {
            hangup();
        } else {
            callOrTransfer();
        }
    }
    
    /**
     * Decides to call using SIP or Callback, <s>or transfer the current call</s>.
     */
    private void callOrTransfer() {
    	//override the default behaviour if SIP is available
    	//see BootToCall.call() for more comments!
    	try {
    		if(xivoConnectionService.getUsesSipNumb(phoneNumber.getText().toString())) {
    			Log.i("call() method", "Calling "+phoneNumber.getText()+" using SIP!");
				try {
					if(SipApiUser.INSTANCE != null) {
						SipApiUser.INSTANCE.call(phoneNumber.getText().toString());
					    if(index == SPECIAL_CONTACT_DURING_CALL) finish();
					} else {
						boolean isSipCompat = SipManager.isApiSupported(this) && SipManager.isVoipSupported(this);
						AlertDialog.Builder builder = new AlertDialog.Builder(XletDialer.this);
						builder.setTitle(R.string.sip_error);
						
				        SharedPreferences prefs = 
				        		PreferenceManager.getDefaultSharedPreferences(this);
						if(prefs.getString("HOME_SSID", "").length() == 0)
							builder.setMessage(R.string.configure_ssid);
						else if(isSipCompat)
							builder.setMessage(SipApiUser.badSettings ? R.string.sip_config : R.string.sip_not_set);
						else
							builder.setMessage(R.string.were_doomed);
						
						builder.setNegativeButton(R.string.ok, null);
						
						if(isSipCompat)
							builder.setPositiveButton(R.string.settings, 
									new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									Intent i = new Intent(XletDialer.this, SettingsActivity.class);
									i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
									startActivity(i);
								}
							});
						builder.show();
					}
						//Toast.makeText(XletDialer.this, getString(R.string.sip_not_set), Toast.LENGTH_LONG).show();
				} catch (SipException e) {
					e.printStackTrace();
				} catch (ParseException e) {
					e.printStackTrace();
				}
    			return;
    		}
    	} catch(RemoteException someException) {}
    	
    	//if we are here, it's because SIP is forbidden.
        // Not on the phone, call with callback
        if (SettingsActivity.getUseMobile(this)) {
            if (!isMobileOffHook()) {
                call();
                return;
            }
        } else {
                if (isServiceReady() && !isXivoOffHook()) {
                    call();
                    return;
                }
        }
        // On the phone
        if (!(isServiceReady())) {
            Log.d(LOG_TAG, "Trying to call or transfer before the service is ready");
            Toast.makeText(this, getString(R.string.service_not_ready), Toast.LENGTH_SHORT).show();
            return;
        }
        
        //giving the choice to hang up, transfer or atxfer
        final String num = phoneNumber.getText().toString();
        final String[] menu = new String[] {
                String.format(getString(R.string.transfer_number), num),
                String.format(getString(R.string.atxfer_number), num),
                getString(R.string.hangup), getString(R.string.cancel_label) };
        new AlertDialog.Builder(this).setTitle(getString(R.string.context_action))
                .setItems(menu, new DialogInterface.OnClickListener() {
                    
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                        case 0:
                            transfer();
                            break;
                        case 1:
                            atxfer();
                            break;
                        case 2:
                            hangup();
                            break;
                        default:
                            Log.d(LOG_TAG, "Canceling");
                            break;
                        }
                    }
                }).show();
    }
    
    /**
     * Set the status of the phone to update the dial button
     * 
     * @param offHook
     */
    public void setPhoneOffHook(final boolean offHook) {
        if (offHook) {
            /* dialButton.setImageDrawable(getResources()
                    .getDrawable(R.drawable.ic_dial_action_hangup)); */
        } else {
            dialButton.setImageDrawable(getResources().getDrawable(R.drawable.new_action_call));
            ((EditText) findViewById(R.id.number)).setEnabled(true);
        }
    }
    
    /**
     * Gets the phone status of the user
     * @return
     */
    private String getMyHintstatus() {
        if (xivoConnectionService == null) return "";
        Cursor c = null;
        try {
            String astid = xivoConnectionService.getAstId();
            String xivoId = xivoConnectionService.getXivoId();
            c = getContentResolver().query(
                    UserProvider.CONTENT_URI,
                    new String[] {
                        UserProvider.ASTID,
                        UserProvider.XIVO_USERID,
                        UserProvider.HINTSTATUS_CODE},
                        UserProvider.ASTID + " = '" + astid + "' AND '" + xivoId + "' = " +
                            UserProvider.XIVO_USERID , null, null);
            if (c.getCount() > 0) {
                c.moveToFirst();
                String ret = c.getString(c.getColumnIndex(UserProvider.HINTSTATUS_CODE));
                c.close();
                return ret;
            }
        } catch (RemoteException e) {
            Log.d(LOG_TAG, "Failed to retrieve my hintstatus");
        }
        if(c != null && !c.isClosed()) c.close();
        return "";
    }
    
    /**
     * Check if our phone is Off hook and if we have active channels
     */
    private void refreshHangupButton() {
        if (SettingsActivity.getUseMobile(this)) {
            setPhoneOffHook(isMobileOffHook());
        } else {
            setPhoneOffHook(sOnThePhoneCode.contains(getMyHintstatus()));
        }
    }
    
    /**
     * Check if XiVO is off hook with the user's hintstatus.
     * @return
     */
    private boolean isXivoOffHook() {
        return sOnThePhoneCode.contains(getMyHintstatus());
    }
    
    /**
     * Check the phone status returns true if it's off hook (or ringing)
     * 
     * @return
     */
    private boolean isMobileOffHook() {
        if (telephonyManager == null)
            return false;
        switch (telephonyManager.getCallState()) {
        case TelephonyManager.CALL_STATE_OFFHOOK:
        case TelephonyManager.CALL_STATE_RINGING:
            return true;
        default:
            return false;
        }
    }
    
    /**
     * Shows the dialog to notify the user that the call is being executed.
     */
    private void showCallDialog() {
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.xlet_dialer_call);
        dialog.setTitle(R.string.calling_title);
        dialog.setOnCancelListener(callTask);
        dialog.setCancelable(true);
        ((TextView) dialog.findViewById(R.id.call_message)).setText(dialogText);
        dialog.show();
    }
    
    /**
     * Hide the call dialog.
     */
    private void cancelCallDialog() {
        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
    }
    
    /**
     * Creating a AsyncTask to run call process
     * 
     * @author cquaquin
     */
    private class CallTask extends AsyncTask<Void, Integer, Integer>
            implements android.content.DialogInterface.OnCancelListener {
        
        public String progress = null;
        private TextView text = null;
        public boolean completeOrCancel = false;
        
        @Override
        protected void onPreExecute() {
            showCallDialog();
            progress = getString(R.string.calling).replace("%s", "\n"+
            				phoneNumber.getText().toString());
            completeOrCancel = false;
            phoneNumber.setEnabled(false);
            text = (TextView) dialog.findViewById(R.id.call_message);
            text.setText(progress);
            dialogText = progress;
            
            super.onPreExecute();
        }
        
        @Override
        protected Integer doInBackground(Void... params) {
        	autoPickup = true;
        	Log.i("Xlet Dialer", "Now picking up all your calls!");
            int result = Constants.OK;
            completeOrCancel = true;
            try {
                while (xivoConnectionService == null) timer(100);
                int res = xivoConnectionService.call(phoneNumber.getText().toString().replace("(", "")
                        .replace(")", "").replace("-", "").trim());
                if(res != Constants.OK) {
                	runOnUiThread(new Runnable() {
						@Override
						public void run() {
							//display an error message in the "making call" dialog.
		                	try {
		                		if(SipManager.isApiSupported(XletDialer.this)
		                				&& SipManager.isVoipSupported(XletDialer.this)) {
		                			if(xivoConnectionService.getSSID().length() != 0) {
		                				//"SIP is unavailable"
										text.setText(getString(R.string.callback_forbidden)
												.replace("%s", xivoConnectionService.getSSID()));
										dialogText = getString(R.string.callback_forbidden)
												.replace("%s", xivoConnectionService.getSSID());
		                			} else {
		                				//"please configure your company's SSID"
										text.setText(getString(R.string.configure_ssid));
										dialogText = getString(R.string.configure_ssid);
		                			}
		                		} else {
		                			//"you won't be able to call with this app"
									text.setText(getString(R.string.were_doomed)
											.replace("%s", xivoConnectionService.getSSID()));
		                			dialogText = getString(R.string.were_doomed)
											.replace("%s", xivoConnectionService.getSSID());
		                		}
							} catch (RemoteException e) {}
						}
					});
                }
                while (!completeOrCancel) timer(200);
            } catch (RemoteException e) {
                result = Constants.REMOTE_EXCEPTION;
            }
            timer(15000);
            cancelCallDialog();
            autoPickup = false;
        	Log.i("Xlet Dialer", "Not picking up anything now! (timed out)");
        	if(isAutoClose) XletDialer.this.finish();
            return result;
        }
        
        private void timer(int milliseconds) {
            try {
                synchronized (this) {
                    this.wait(milliseconds);
                }
            } catch (InterruptedException e) {
                Log.i("Xlet Dialer", "Thread sleeping interrupted");
                onCancel(null);
            }
        }
        
        @Override
        protected void onProgressUpdate(Integer... values) {
            text.setText(progress);
            super.onProgressUpdate(values);
        }
        
        @Override
        protected void onPostExecute(Integer result) {
            phoneNumber.setEnabled(true);
            switch (result) {
            case Constants.OK:
                break;
            case Constants.REMOTE_EXCEPTION:
                showToast(R.string.remote_exception);
                break;
            }
        }
        
        private void showToast(int messageId) {
            Toast.makeText(XletDialer.this, getString(messageId), Toast.LENGTH_SHORT).show();
        }
        
        @Override
        public void onCancel(DialogInterface dialog) {
        	//the user certainly canceled the dialog, so stop everything and hang up
	    	Log.i("Xlet Dialer", "Call Cancelled!");
        	cancelCallDialog();
	        autoPickup = false;
	    	Log.i("Xlet Dialer", "Not picking up anything now! (hung up)");
            hangup();
            this.cancel(true);
        }
    }
    
    /**
     * BroadcastReceiver, intercept Intents
     * 
     * @author cquaquin
     * 
     */
    private class IncomingReceiver extends BroadcastReceiver {
        
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (action.equals(Constants.ACTION_OFFHOOK)) {
                Log.d(LOG_TAG, "OffHook action received");
                phoneNumber.setEnabled(true);
                phoneNumber.setText("");
            } else if (action.equals(Constants.ACTION_MWI_UPDATE)) {
                Log.d(LOG_TAG, "MWI update received");
                int[] mwi = intent.getExtras().getIntArray("mwi");
                newVoiceMail(mwi[0] == 1);
            } else if (action.equals(Constants.ACTION_CALL_PROGRESS)) {
                final String status = intent.getStringExtra("status");
                final String code = intent.getStringExtra("code");
                if (code.equals(Constants.CALLING_STATUS_CODE)) {
                    phoneNumber.setEnabled(true);
                    phoneNumber.setText("");
                }
                if (callTask != null) {
                    if (Integer.parseInt(code) == Constants.HINTSTATUS_AVAILABLE_CODE) {
                        callTask.completeOrCancel = true;
                    }
                    callTask.progress = status;
                    callTask.onProgressUpdate();
                }
            } else if (action.equals(Constants.ACTION_ONGOING_CALL)) {
                phoneNumber.setEnabled(true);
                phoneNumber.setText("");
            } else if (action.equals(Constants.ACTION_MY_PHONE_CHANGE)) {
                if (intent.getStringExtra("code").equals(Constants.AVAILABLE_STATUS_CODE)) {
                }
            } else if (action.equals(Constants.ACTION_UPDATE_PEER_HINTSTATUS)) {
                if (intent.hasExtra("status")) {
                    String status = intent.getStringExtra("status");
                    if (status.equals("ringing")) {
                        setCancellable(true);
                    } else if (status.equals("linked-called")){
                        cancelCallDialog();
                    }
                }
            }
        }
    }
    
    /**
     * Change the status of the dialog box to make it cancelable if the call can be cancelled
     * @param cancellable
     */
    private void setCancellable(final boolean cancellable) {
        if (dialog != null) {
            dialog.setCancelable(cancellable);
            if (cancellable) {
                ((TextView) dialog.findViewById(R.id.call_message)).setText(
                        getString(R.string.back_cancel));
            }
        }
    }
    
    public void clickOn1(View v) {
        keyPressed(KeyEvent.KEYCODE_1);
    }
    
    public void clickOn2(View v) {
    	keyPressed(KeyEvent.KEYCODE_2);
    }
    
    public void clickOn3(View v) {
   	 	keyPressed(KeyEvent.KEYCODE_3);
    }
    
    public void clickOn4(View v) {
    	keyPressed(KeyEvent.KEYCODE_4);
    }
    
    public void clickOn5(View v) {
    	keyPressed(KeyEvent.KEYCODE_5);
    }
    
    public void clickOn6(View v) {
    	keyPressed(KeyEvent.KEYCODE_6);
    }
    
    public void clickOn7(View v) {
    	keyPressed(KeyEvent.KEYCODE_7);
    }
    
    public void clickOn8(View v) {
    	keyPressed(KeyEvent.KEYCODE_8);
    }
    
    public void clickOn9(View v) {
    	keyPressed(KeyEvent.KEYCODE_9);
    }
    
    public void clickOn0(View v) {
    	keyPressed(KeyEvent.KEYCODE_0);
    }
    
    public void clickOn0Long(View v) {
    	keyPressed(KeyEvent.KEYCODE_PLUS);
    }
    
    public void clickOnStar(View v) {
    	keyPressed(KeyEvent.KEYCODE_STAR);
    }
    
    public void clickOnSharp(View v) {
    	keyPressed(KeyEvent.KEYCODE_POUND);
    }
    
    public void clickOnDelete(View v) {
        keyPressed(KeyEvent.KEYCODE_DEL);
    }
    
    /**
     * Simulates a keyboard stroke with the specified key code, on the phone number text field.
     * @param keyCode the key code
     */
    private void keyPressed(int keyCode) {
        KeyEvent event = new KeyEvent(KeyEvent.ACTION_DOWN, keyCode);
        phoneNumber.onKeyDown(keyCode, event);
    }
    
    /**
     * Click on a button that is invisible...
     * @param v
     */
    public void clickVoiceMail(View v) {
        if (SettingsActivity.getUseMobile(this)) {
            Toast.makeText(this, "Not available when using your mobile number.", Toast.LENGTH_LONG)
                    .show();
        } else {
            ((EditText) findViewById(R.id.number)).setText("*98");
            new CallTask().execute();
        }
    }
    
    /**
     * Sets the last dialed value.
     */
    @Override
    protected void onResume() {
        if (phoneNumber != null && phoneNumber.getText().toString().equals("")) {
            phoneNumber.setText(SettingsActivity.getLastDialerValue(this));
            phoneNumber.setSelection(phoneNumber.getText().toString().length());
        }
        super.onResume();
    }
    
    /**
     * Record the last dialed value, and remove the dialog.
     */
    @Override
    protected void onPause() {
        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
        if (phoneNumber != null) {
            phoneNumber.setEnabled(true);
            SettingsActivity.setLastDialerValue(this, phoneNumber.getText().toString());
        }
        super.onPause();
    }
    
    /**
     * Unregisters the receivers.
     */
    @Override
    protected void onDestroy() {
    	Log.d("Xlet Dialer", "Activity destroyed");
    	
        unregisterReceiver(receiver);
        telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_NONE);
        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
        super.onDestroy();
    }
    
    /**
     * Disables the call button and the phone number field.
     */
    @Override
    protected void setUiEnabled(boolean state) {
        super.setUiEnabled(state);
        if (dialButton != null) {
            dialButton.setEnabled(state);
        }
        if (phoneNumber != null) {
            phoneNumber.setEnabled(state);
        }
    }
    

    /**
     * Called when the user clicks the contacts button.
     * @param v
     */
    public void clickContacts(View v) {
    	Intent i = new Intent(this, XletContactSearch.class);
    	i.putExtra("contactFromDialer", true);
    	startActivityForResult(i, 1);
    }
    
    /**
     * Called when the contact search window has given us a number. (request code = 1)
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode,
            Intent data) {
    	if(requestCode == 1) {
    		if(resultCode == RESULT_OK) {
    			String number = data.getStringExtra("number");
    			phoneNumber.setText(number);
    		}
    	}
    }
}
