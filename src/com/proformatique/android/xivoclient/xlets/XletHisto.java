/* XiVO Client Android
 * Copyright (C) 2010-2011, Proformatique
 *
 * This file is part of XiVO Client Android.
 *
 * XiVO Client Android is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * XiVO Client Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with XiVO client Android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.proformatique.android.xivoclient.xlets;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.sip.SipException;
import android.net.sip.SipManager;
import android.os.Bundle;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.markupartist.android.widget.PullToRefreshListView;
import com.markupartist.android.widget.PullToRefreshListView.OnRefreshListener;
import com.proformatique.android.xivoclient.NavigationDrawerActivity;
import com.proformatique.android.xivoclient.R;
import com.proformatique.android.xivoclient.SettingsActivity;
import com.proformatique.android.xivoclient.dao.HistoryProvider;
import com.proformatique.android.xivoclient.sip.SipApiUser;
import com.proformatique.android.xivoclient.tools.Constants;
import com.proformatique.android.xivoclient.tools.GraphicsManager;

/**
 * Displays the user's history.
 */
public class XletHisto extends NavigationDrawerActivity implements OnItemClickListener, OnItemLongClickListener {
    
    private static final String LOG_TAG = "XiVO " + XletHisto.class.getSimpleName();
    private Cursor xletList = null;
    AlternativeAdapter xletAdapter = null;
    ListView lv;
    ImageView phoneOnHeader;
    IncomingReceiver receiver;
    byte filterSelected = 0;
    
    /**
     * Inits the UI.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        //set up the index
        Intent i = getIntent();
        if(i.getBooleanExtra("duringCall", false)) {
			//tells the app not to display the navigation drawer, and to close after launching a call
			index = SPECIAL_CONTACT_DURING_CALL;
			//set the same flags as the call screen (the user should not have to unlock the phone
			//between the call screen and the contact list)
	        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
	        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
		} else {
			index = 3;
		}
        
        //UI setup
        setContentView(R.layout.xlet_history);
        setUpNavigationDrawer(R.id.navigation_drawer_history, R.id.drawer_layout_history);
        registerButtons();
        
        //variable init
        sp = PreferenceManager.getDefaultSharedPreferences(this);
		filterSelected = (byte) sp.getInt("selectedFilter", 0);
		int[] imageRes = new int[] { R.drawable.new_call_header, R.drawable.new_call_in,
				R.drawable.new_call_out, R.drawable.new_call_miss };
        
		//list setup
        lv = (ListView) findViewById(R.id.history_list);
        lv.setOnItemClickListener(this);
        lv.setOnItemLongClickListener(this);
        xletList = HistoryProvider.getList(this, filterSelected);
        xletAdapter = new AlternativeAdapter(
                this, xletList, R.layout.xlet_history_items,
                new String[] {"fullname", "ts", "duration"},
                new int[] {R.id.history_fullname, R.id.history_date, R.id.history_duration});
        lv.setAdapter(xletAdapter);
        
        /*
         * Register a BroadcastReceiver for Intent action that trigger a change
         * in the list from the Activity
         */
        receiver = new IncomingReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Constants.ACTION_LOAD_HISTORY_LIST);
        registerReceiver(receiver, new IntentFilter(filter));
        registerForContextMenu(lv);
        
        //react when the list is "pulled" to refresh.
        ((PullToRefreshListView) lv).setOnRefreshListener(new OnRefreshListener() {
			@Override
			public void onRefresh() {
				try {
					if(xivoConnectionService != null && xivoConnectionService.isAuthenticated()
							&& index != SPECIAL_CONTACT_DURING_CALL) //if the connection is available, of course.
						//otherwise it erases all the entries, leading to a white screen. Great.
						xivoConnectionService.refreshHistory();
					else 
						((PullToRefreshListView) lv).onRefreshComplete();
				} catch (RemoteException e) {
					e.printStackTrace();
				}
			}
		});
        
        //filter spinner setup
        final Spinner spi = (Spinner) findViewById(R.id.call_filter);
        int[] texts = new int[] { R.string.call_all, R.string.call_in, 
        		R.string.call_out, R.string.call_miss};
        int[] pics = new int[] { R.drawable.new_call_header, R.drawable.new_call_in,
        		R.drawable.new_call_out, R.drawable.new_call_miss};
        ArrayList<HashMap<String, Integer>> identityStateList = new ArrayList<HashMap<String,Integer>>();
		for(int k = 0; k < 4; k++) {
			HashMap<String, Integer> map = new HashMap<String, Integer>();
			map.put("text", texts[k]);
			map.put("image", pics[k]);
			identityStateList.add(map);
		}
		FilterAdapter stateAdapter = new FilterAdapter(
				this, identityStateList, R.layout.call_filter_item,
				new String[0], new int[0]);
		spi.setAdapter(stateAdapter);
		//react when a new filter is selected
		spi.setOnItemSelectedListener(new OnItemSelectedListener() {
			boolean firstTime = true;
			
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				if(firstTime) {
					//don't react the first time, when the spinner was just created
					firstTime = false;
					return;
				}
				
				@SuppressWarnings("unchecked")
				final HashMap<String, Integer> line = (HashMap<String, Integer>) spi.getItemAtPosition(arg2);
				
				//save the selected filter
				filterSelected = (byte) arg2;
				sp.edit().putInt("selectedFilter", filterSelected).apply();
				Log.d(LOG_TAG, "Now selected filter "+filterSelected+"!");
				
				//update the list
				runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                    	Log.i(LOG_TAG, "Refreshing the displayed logs");
                        xletList = HistoryProvider.getList(XletHisto.this, filterSelected);
                        xletAdapter.setList(xletList);
                        lv.setAdapter(xletAdapter);
                        //show the new icon in the header
                        phoneOnHeader.setImageResource(line.get("image"));
                    }
                });
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// Nothing to do here... but have to implement this.
			}
		});
		
		//restore the last selected filter.
		phoneOnHeader = (ImageView) findViewById(R.id.call_filter_display);
		phoneOnHeader.setImageResource(imageRes[filterSelected]);
		spi.setSelection(filterSelected);
    }
    
    SharedPreferences sp;
    
    /**
     * Refresh history when the activity is resumed.
     */
    @Override
    public void onResume() {
    	super.onResume();
    	
    	//must use it in onResume() because xivoConnectionService is null before
        try {
        	if(xivoConnectionService != null && xivoConnectionService.isAuthenticated()
        			&& index != SPECIAL_CONTACT_DURING_CALL) //if the connection is available, of course.
        		//otherwise it erases all the entries, leading to a white screen. Great.
        		xivoConnectionService.refreshHistory();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
    }
    
    
    /**
     * BroadcastReceiver, intercept Intents with action ACTION_LOAD_HISTORY_LIST
     * to perform an reload of the displayed list
     * 
     * @author cquaquin
     * 
     */
    private class IncomingReceiver extends BroadcastReceiver {
        int numberOfIntents = 0;
        
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Constants.ACTION_LOAD_HISTORY_LIST)) {
                numberOfIntents++;
                Log.d(LOG_TAG, "Received Broadcast " + Constants.ACTION_LOAD_HISTORY_LIST+" "+numberOfIntents);
                //intents to refresh history always come 3 times, because incoming, outgoing
                //and missed calls are obtained independently.
                //Only refresh on the third messages.
                if (xletAdapter != null && numberOfIntents % 3 == 0) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                        	Log.i(LOG_TAG, "Refreshing the displayed logs");
                        	//update the list itself.
                            xletList = HistoryProvider.getList(XletHisto.this, filterSelected);
                            xletAdapter.setList(xletList);
                            lv.setAdapter(xletAdapter);
                            //notify the PullToRefreshListView of the refresh.
                            SimpleDateFormat sd1 = new SimpleDateFormat("HH:mm:ss");
                            ((PullToRefreshListView) lv).onRefreshComplete();
                            ((PullToRefreshListView) lv).setLastUpdated(getString(R.string.last_update)+" "+
                            		sd1.format(new Date()));
                        }
                    });
                }
            }
        }
    }
    
    /**
     * Adapts the given Cursor to the history ListView.
     */
    private class AlternativeAdapter extends SimpleCursorAdapter {
    	Cursor cursor;

    	/**
    	 * Inits the SimpleCursorAdapter and gets the cursor.
    	 * @param context
    	 * @param c the Cursor to use.
    	 * @param resource the layout to use for the views.
    	 * @param from
    	 * @param to
    	 */
        public AlternativeAdapter(Context context, Cursor c, int resource, String[] from, int[] to) {
        	super(context, resource, c, from, to, 0);
        	cursor = c;
        }
        
        /**
         * Change the cursor.
         * @param data the new cursor
         */
        public void setList(Cursor data) {
        	cursor = data;
        }
        
        /**
         * Get the number of history entries.
         */
        @Override
        public int getCount() {
        	return cursor.getCount();
        }
        
        /**
         * Gets a particular line.
         */
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
        	View view = null;
        	try {
        		view = super.getView(position, convertView, parent);
        	} catch(Exception e) {
        		//this super-call seems to throw a NullPointerException only for the app's
        		//first launch ... ? Anyway, if something gets wrong, we are going to
        		//inflate the view ourselves. It works as well.
        	}
        	if(view == null)
        		view = getLayoutInflater().inflate(R.layout.xlet_history_items, null);
            
        	cursor.moveToPosition(position);

            //update the call direction icon
            String direction = cursor.getString(cursor.getColumnIndex(HistoryProvider.DIRECTION));
            ImageView icon = (ImageView) view.findViewById(R.id.callStatus);
            icon.setBackgroundResource(GraphicsManager.getCallIcon(direction));
            
            //update the caller
            //with these replaces, "Hello World" <1003> becomes Hello World - 1003
            //but a number like 1234 remains 1234
            ((TextView) view.findViewById(R.id.history_fullname))
            	.setText(cursor.getString(cursor.getColumnIndex(HistoryProvider.FULLNAME)).replace("\"", "")
            			.replace("<", "- ").replace(">", ""));
            
            //update the date
            ((TextView) view.findViewById(R.id.history_date))
            	.setText(cursor.getString(cursor.getColumnIndex(HistoryProvider.TS)));

            //reformat the duration
            TextView duration = (TextView) view.findViewById(R.id.history_duration);
            duration.setText(cursor.getString(cursor.getColumnIndex(HistoryProvider.DURATION)));
            try {
            	double duree = Double.parseDouble(duration.getText().toString());
            	int dur = (int) duree;
            	String text = "";
            	if(dur / 3600 != 0) {
            		text += (dur / 3600) + " h ";
            		dur -= (dur / 3600) * 3600;
            	}
            	if(dur / 60 != 0) {
            		text += (dur / 60) + " min ";
            		dur -= (dur /60) * 60;
            	}
            	if(dur != 0) {
	        		text += dur + " s";
            	}
            	if(text.length() == 0) {
            		text = "---";
            	}
            	duration.setText(text);
            } catch(NumberFormatException e) {
            	duration.setText(getString(R.string.unknown));
            }
            
            //reformat the date as well
            TextView date = (TextView) view.findViewById(R.id.history_date);
            if(Locale.getDefault().getLanguage().startsWith("fr")) {
            	//French format.
            	SimpleDateFormat sd1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //source
            	SimpleDateFormat sd2 = new SimpleDateFormat("dd/MM HH:mm"); //destination
            	try {
					Date da = sd1.parse(date.getText().toString());
					date.setText(sd2.format(da));
				} catch (ParseException e) {
					e.printStackTrace();
				}
            } else {
            	//English format.
            	SimpleDateFormat sd1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //source
            	SimpleDateFormat sd2 = new SimpleDateFormat("MM-dd HH:mm"); //destination
            	try {
					Date da = sd1.parse(date.getText().toString());
					date.setText(sd2.format(da));
				} catch (ParseException e) {
					e.printStackTrace();
				}
            }
            
            return view;
        }
    }
    
    /**
     * Be sure the cursor is closed and that the receivers are unregistered.
     */
    @Override
    protected void onDestroy() {
        unregisterReceiver(receiver);
        xletList.close();
        super.onDestroy();
    }

    /**
     * React when an item is clicked (call the contact).
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    		xletList.moveToPosition(position-1);
    		String fullname = xletList.getString(xletList.getColumnIndex(HistoryProvider.FULLNAME));
            String number = "";
            //gets the number from the text.
            try {
                Long.parseLong(fullname);
                number = fullname;
            } catch (Exception e) {
            	//seek the contact number between < and >
                Pattern p = Pattern.compile(".*?<([^>]+)>",
                        Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
                Matcher m = p.matcher(fullname);
                if (m.find())
                    number = m.group(1);
            }
            if (number == null || number.equals("")) {
            	//"this contact has no telephone number."
                Toast.makeText(this, R.string.no_phone_error, Toast.LENGTH_SHORT).show();
                return;
            }
            final String phoneNumber = number;
            
            //call right away!
            call(phoneNumber);
    }
    
    /**
     * React when an item is long clicked.
     */
    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        try {
    		xletList.moveToPosition(position-1);
    		String fullname = xletList.getString(xletList.getColumnIndex(HistoryProvider.FULLNAME));
    		
            String number = "";
            //gets the number from the text.
            try {
                Long.parseLong(fullname);
                number = fullname;
            } catch (Exception e) {
            	//seek the phone number between < and >
                Pattern p = Pattern.compile(".*?<([^>]+)>",
                        Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
                Matcher m = p.matcher(fullname);
                if (m.find())
                    number = m.group(1);
            }
            if (number == null || number.equals("")) {
                Toast.makeText(this, R.string.no_phone_error, Toast.LENGTH_SHORT).show();
                return true;
            }
            final String phoneNumber = number;
            
            //shows the menu.
            String[] items;
            if (xivoConnectionService.isOnThePhone()) {
            	//transfer options
                String[] tmp = {
                        String.format(getString(R.string.atxfer_number), number),
                        String.format(getString(R.string.transfer_number), number),
                        getString(R.string.cancel_label)};
                items = tmp;
            } else {
            	//call options
                String[] tmp = {
                        String.format(getString(R.string.context_action_call), number),
                        String.format(getString(R.string.context_edit_before_call), number),
                        getString(R.string.cancel_label) };
                
                items = tmp;
            }
            
            TextView contactName = (TextView) view.findViewById(R.id.history_fullname);
            
            //show an alert so that the user makes a choice.
            new AlertDialog.Builder(this).setTitle(contactName.getText()).setItems(items,
                    new OnClickListener() {
                        
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            try {
                                if (xivoConnectionService.isOnThePhone()) {
                                	//transfer options...
                                    if (which == 0) {
                                        xivoConnectionService.atxfer(phoneNumber);
                                    } else if (which == 1) {
                                        xivoConnectionService.transfer(phoneNumber);
                                    }
                                } else if (which == 0) {
                                	//call
    								call(phoneNumber);
                                } else if(which == 1) {
    								//edit number in XletDialer
    								Intent intent = new Intent(XletHisto.this, XletDialer.class);
    								intent.putExtra("numToCall", phoneNumber);
    								intent.putExtra("edit", true);
    								if(index == SPECIAL_CONTACT_DURING_CALL) {
    									//notify the XletDialer that we are in call!
    									intent.putExtra("duringCall", true);
    									finish();
    								}
    								startActivity(intent);
                                }
                            } catch (RemoteException e) {
                                Toast.makeText(XletHisto.this, R.string.service_not_ready,
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    }).show();
        } catch (RemoteException e) {
            Toast.makeText(this, R.string.service_not_ready, Toast.LENGTH_SHORT).show();
            Log.d(LOG_TAG, "xivoConnectionService is null");
        }
        return true;
    }
    
    /**
     * Make a phone call using the most appropriate method 
     * @param phoneNumber the number to call
     */
    private void call(String phoneNumber) {
    	//see BootToCall.call for more comments
    	try {
			if(xivoConnectionService.getUsesSipNumb(phoneNumber)) {
				Log.i("call() method", "Calling "+phoneNumber+" using SIP!");
				try {
					if(SipApiUser.INSTANCE != null) {
						SipApiUser.INSTANCE.call(phoneNumber);
					    if(index == SPECIAL_CONTACT_DURING_CALL) finish();
					} else {
						boolean isSipCompat = SipManager.isApiSupported(this) && SipManager.isVoipSupported(this);
						AlertDialog.Builder builder = new AlertDialog.Builder(XletHisto.this);
						builder.setTitle(R.string.sip_error);
						SharedPreferences prefs = 
				        		PreferenceManager.getDefaultSharedPreferences(this);
						if(prefs.getString("HOME_SSID", "").length() == 0)
							builder.setMessage(R.string.configure_ssid);
						else if(isSipCompat)
							builder.setMessage(SipApiUser.badSettings ? R.string.sip_config : R.string.sip_not_set);
						else
							builder.setMessage(R.string.were_doomed);
						
						builder.setNegativeButton(R.string.ok, null);
						
						if(isSipCompat)
							builder.setPositiveButton(R.string.settings, 
									new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									Intent i = new Intent(XletHisto.this, SettingsActivity.class);
									i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
									startActivity(i);
								}
							});
						builder.show();
					}
						//Toast.makeText(XletHisto.this, getString(R.string.sip_not_set), Toast.LENGTH_LONG).show();
				} catch (SipException e) {
					e.printStackTrace();
				} catch (ParseException e) {
					e.printStackTrace();
				}
			} else {
				//old method ; pops up the XletDialer that will make the call itself
			    Intent iCall = new Intent(XletHisto.this, XletDialer.class);
			    iCall.putExtra("numToCall", phoneNumber);
			    startActivity(iCall);
			}
		} catch (RemoteException e) {
			//some exception thrown for no reason...
			e.printStackTrace();
		}
    }
    
    /**
     * This Adapter shows the 4 available filters in a dropdown view.
     */
    private class FilterAdapter extends SimpleAdapter implements android.widget.SpinnerAdapter {

		List<? extends Map<String, ?>> data;
		
		/**
    	 * Inits the SimpleCursorAdapter and gets the cursor.
    	 * @param context
    	 * @param c the Cursor to use.
    	 * @param resource the layout to use for the views.
    	 * @param from
    	 * @param to
    	 */
		public FilterAdapter(Context context,
				List<? extends Map<String, ?>> data, int resource,
				String[] from, int[] to) {
			super(context, data, resource, from, to);
			this.data = data;
		}
		
		public View getDropDownView (int position, View convertView, ViewGroup parent) {
			View view = super.getView(position, convertView, parent);
			@SuppressWarnings("unchecked")
			HashMap<String, Integer> line = (HashMap<String, Integer>) data.get(position);
			
			//set the icon
			ImageView icon = (ImageView) view.findViewById(R.id.call_filter_icon);
			icon.setImageResource(line.get("image"));
			
			//set the text
			TextView text = (TextView) view.findViewById(R.id.call_filter_text);
			text.setText(line.get("text"));
			return view;
		}
		
		/**
		 * Gets the number of options
		 */
		@Override
		public int getCount() {
			return data.size();
		}
    	
    }
}
