/* XiVO Client Android
 * Copyright (C) 2010-2011, Proformatique
 *
 * This file is part of XiVO Client Android.
 *
 * XiVO Client Android is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * XiVO Client Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with XiVO client Android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.proformatique.android.xivoclient.xlets;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.Toast;

import com.proformatique.android.xivoclient.NavigationDrawerActivity;
import com.proformatique.android.xivoclient.R;
import com.proformatique.android.xivoclient.dao.CapaservicesProvider;
import com.proformatique.android.xivoclient.tools.Constants;

import static com.proformatique.android.xivoclient.dao.CapaservicesProvider.getNumberForFeature;

/**
 * The screen used to show and toggle various services.
 */
public class XletServices extends NavigationDrawerActivity {
	private static final String LOG_TAG = "XLET SERVICES";
	private IncomingReceiver receiver;
	boolean wasNoneTheLast = true;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.xlet_services);
		setUpNavigationDrawer(R.id.navigation_drawer_services, R.id.drawer_layout_services);
		
		index = 4;
		
		/**
		 *  Register a BroadcastReceiver for Intent action that trigger a change
		 *  in the list from the Activity
		 */
		receiver = new IncomingReceiver();
		IntentFilter filter = new IntentFilter();
		filter.addAction(Constants.ACTION_LOAD_FEATURES);
		registerReceiver(receiver, new IntentFilter(filter));
		
		registerButtons();
	}
	
	@Override
	protected void onBindingComplete() {
		super.onBindingComplete();
		refreshFeatures();
	}
	
	public void clickOnEnabledual(View v){
		CheckBox checkbox = (CheckBox)v;
		if (checkbox.isChecked()){
			Log.w("Xlet Services", "Dual Ringing should be enabled!");
		} else {
			Log.w("Xlet Services", "Dual Ringing should be disabled!");
		}
	}
	/*
	public void clickOnCallrecord(View v){
		CheckBox checkbox = (CheckBox)v;
		if (checkbox.isChecked()){
			sendFeaturePut("callrecord", "1", null);
		} else {
			sendFeaturePut("callrecord", "0", null);
		}
	}
	
	public void clickOnIncallfilter(View v){
		CheckBox checkbox = (CheckBox)v;
		if (checkbox.isChecked()){
			sendFeaturePut("incallfilter", "1", null);
		} else {
			sendFeaturePut("incallfilter", "0", null);
		}
	}*/
	
	public void clickOnEnablednd(View v){
		CheckBox checkbox = (CheckBox)v;
		if (checkbox.isChecked()){
			sendFeaturePut("enablednd", "1", null);
		} else {
			sendFeaturePut("enablednd", "0", null);
		}
	}
	
	public void clickOnFwdrna(View v){
		CheckBox checkbox = (CheckBox)v;
		if (checkbox.isChecked()){
			checkbox.setClickable(false);
			Intent defineIntent = new Intent(this, XletServicesAsk.class);
			defineIntent.putExtra("serviceType", "rna");
			startActivityForResult(defineIntent, Constants.CODE_SERVICE_ASK1);
		} else {
			checkbox.setText(R.string.servicesFwdrna);
			sendFeaturePut("enablerna", "0", getNumberForFeature(this, "rna"));
		}
	}
	
	public void clickOnNoFwd(View v) {
		wasNoneTheLast = true;
		
		//disable all the call forwarding features
		sendFeaturePut("enablerna", "0", getNumberForFeature(this, "rna"));
		sendFeaturePut("enablebusy", "0", getNumberForFeature(this, "busy"));
		sendFeaturePut("enableunc", "0", getNumberForFeature(this, "unc"));
		
		setSimpleForwardEnabled(false);
	}
	
	public void clickOnFwdSimple(View v) {
		wasNoneTheLast = false;
		//disable unconditionnal forwarding.
		sendFeaturePut("enableunc", "0", getNumberForFeature(this, "unc"));
		setSimpleForwardEnabled(true);
	}
	
	public void clickOnFwdbusy(View v){
		CheckBox checkbox = (CheckBox)v;
		if (checkbox.isChecked()){
			checkbox.setClickable(false);
			Intent defineIntent = new Intent(this, XletServicesAsk.class);
			defineIntent.putExtra("serviceType", "busy");
			startActivityForResult(defineIntent, Constants.CODE_SERVICE_ASK2);
		} else {
			checkbox.setText(R.string.servicesFwdbusy);
			sendFeaturePut("enablebusy", "0", getNumberForFeature(this, "busy"));
		}
	}
	
	public void clickOnFwdunc(View v){
		RadioButton checkbox = (RadioButton)v;
		if (checkbox.isChecked()){
			checkbox.setClickable(false);
			Intent defineIntent = new Intent(this, XletServicesAsk.class);
			defineIntent.putExtra("serviceType", "unc");
			startActivityForResult(defineIntent, Constants.CODE_SERVICE_ASK3);
		} else {
			checkbox.setText(R.string.servicesFwdunc);
			sendFeaturePut("enableunc", "0", getNumberForFeature(this, "unc"));
		}
	}
	
	/**
	 * Receives the phone number from XletServicesAsk and send the right feature put.
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		//when clicking the "about" or "settings" button on this activity, "data" is null
		if(data == null) return; //nothing else to do
		
		CheckBox checkbox;
		String textDisplay;
		String phoneNumber = "";
		if (data.getStringExtra("phoneNumber") != null) {
		    phoneNumber = data.getStringExtra("phoneNumber");
		}
		
		//treats the 3 different call forwards
		if (requestCode == Constants.CODE_SERVICE_ASK1) {
			checkbox = (CheckBox) findViewById(R.id.fwdrna);
			textDisplay = getString(R.string.servicesFwdrna);
			setCheckboxDisplay(resultCode == Constants.OK, checkbox, phoneNumber, textDisplay);
			if (resultCode  == Constants.OK)
				sendFeaturePut("enablerna", "1", phoneNumber);
		} else if (requestCode == Constants.CODE_SERVICE_ASK2) {
			checkbox = (CheckBox) findViewById(R.id.fwdbusy);
			textDisplay = getString(R.string.servicesFwdbusy);
			setCheckboxDisplay(resultCode == Constants.OK, checkbox, phoneNumber, textDisplay);
			if (resultCode  == Constants.OK)
				sendFeaturePut("enablebusy", "1", phoneNumber);
		} else if (requestCode == Constants.CODE_SERVICE_ASK3) {
			RadioButton rb = (RadioButton) findViewById(R.id.fwdunc);
			textDisplay = getString(R.string.servicesFwdunc);
			setRBDisplay(resultCode == Constants.OK, rb, phoneNumber, textDisplay);
			
			if (resultCode  == Constants.OK) {
				sendFeaturePut("enableunc", "1", phoneNumber);

				//disable the other features
				sendFeaturePut("enablerna", "0", getNumberForFeature(this, "rna"));
				sendFeaturePut("enablebusy", "0", getNumberForFeature(this, "busy"));
				setSimpleForwardEnabled(false);
			} else {
				//revert to the last forward mode checked
				if(wasNoneTheLast)
					((RadioButton) findViewById(R.id.nofwd)).setChecked(true);
				else
					((RadioButton) findViewById(R.id.fwdsim)).setChecked(true);
			}
		}
	}
	
	/**
	 * Sets a {@link CheckBox} display.
	 * @param checked if the CheckBox is checked
	 * @param checkbox the {@link CheckBox} to change
	 * @param phoneNumber the phone number to forward to
	 * @param textDisplay the text to display
	 */
	private void setCheckboxDisplay(boolean checked, CheckBox checkbox, String phoneNumber,
			String textDisplay){
		if (checked) {
			//display the text + the number
			checkbox.setText(textDisplay + getString(R.string.servicesFwdTo) + phoneNumber);
		} else {
			//just display the text
			checkbox.setText(textDisplay);
		}
		//set the right checked state
		checkbox.setChecked(checked);
		checkbox.setClickable(true);
	}

	/**
	 * Sets a {@link RadioButton} display.
	 * @param checked if the RadioButton is checked
	 * @param checkbox the {@link RadioButton} to change
	 * @param phoneNumber the phone number to forward to
	 * @param textDisplay the text to display
	 */
	private void setRBDisplay(boolean checked, RadioButton checkbox, String phoneNumber,
			String textDisplay){
		if (checked) {
			//display the text + the number
			checkbox.setText(textDisplay + getString(R.string.servicesFwdTo) + phoneNumber);
		} else {
			//just display the text
			checkbox.setText(textDisplay);
		}
		//set the right checked state
		checkbox.setChecked(checked);
		checkbox.setClickable(true);
	}
	
	/**
	 * BroadcastReceiver, intercept Intents with action ACTION_LOAD_FEATURES
	 * to perform an reload of the displayed features
	 * @author cquaquin
	 *
	 */
	private class IncomingReceiver extends BroadcastReceiver {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals(Constants.ACTION_LOAD_FEATURES)) {
				Log.d(LOG_TAG, "Received Broadcast" + Constants.ACTION_LOAD_FEATURES);
				refreshFeatures();
			}
		}
	}
	
	/**
	 * Reads the features from the {@link CapaservicesProvider} and set up the checkboxes accordingly.
	 */
	public void refreshFeatures() {
		Cursor c = getContentResolver().query(CapaservicesProvider.CONTENT_URI,
				null, null, null, null);
		c.moveToFirst();
		if (c.moveToFirst()) {
			do {
				String service = c.getString(c.getColumnIndex(CapaservicesProvider.SERVICE));
				boolean enabled = c.getInt(c.getColumnIndex(
						CapaservicesProvider.ENABLED)) == 1 ? true : false;
				if (service.equals("busy"))
					enableFwdbusy(enabled);
				else if (service.equals("rna"))
					enableFwdrna(enabled);
				else if (service.equals("unc"))
					enableFwdunc(enabled);
				else if (service.equals("enablednd"))
					enableEnableDnd(enabled);/*
				else if (service.equals("callrecord"))
					enableCallrecord(enabled);
				else if (service.equals("incallfilter"))
					enableIncallfilter(enabled);*/
			} while (c.moveToNext());
			
			//no call forwarding is enabled, so just check the "no call" radio button
			if(!((RadioButton) findViewById(R.id.fwdsim)).isChecked()
		    && !((RadioButton) findViewById(R.id.fwdunc)).isChecked())
				((RadioButton) findViewById(R.id.nofwd)).setChecked(true);
			//also make the last radio buttons clickable
			((RadioButton) findViewById(R.id.nofwd)).setClickable(true);
			((RadioButton) findViewById(R.id.fwdsim)).setClickable(true);
			//make the "simple forward" checkboxes clickable if the radio button is selected
			setSimpleForwardEnabled(((RadioButton) findViewById(R.id.fwdsim)).isChecked());
		}
		c.close();
	}
	
	@Override
    public void onResume() {
    	super.onResume();
    	
    	//must use it in onResume() because xivoConnectionService is null before
        try {
        	if(xivoConnectionService != null && xivoConnectionService.isAuthenticated()
        			&& index != SPECIAL_CONTACT_DURING_CALL)
        		xivoConnectionService.refreshServices();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
    }
	
	private void enableFwdbusy(final boolean status) {
		CheckBox checkbox;
		checkbox = (CheckBox) findViewById(R.id.fwdbusy);
		setCheckboxDisplay(status, checkbox, getNumberForFeature(this, "busy"),
				getString(R.string.servicesFwdbusy));
		
		//simple call forwarding, check the radio button
		if(status && !((RadioButton) findViewById(R.id.fwdsim)).isChecked()) 
			((RadioButton) findViewById(R.id.fwdsim)).setChecked(true);
	}
	
	private void enableFwdrna(final boolean status) {
		CheckBox checkbox;
		checkbox = (CheckBox) findViewById(R.id.fwdrna);
		setCheckboxDisplay(status, checkbox, getNumberForFeature(this, "rna"),
				getString(R.string.servicesFwdrna));
		
		//simple call forwarding, check the radio button
		if(status && !((RadioButton) findViewById(R.id.fwdsim)).isChecked()) 
			((RadioButton) findViewById(R.id.fwdsim)).setChecked(true);
	}
	
	private void enableFwdunc(final boolean status) {
		RadioButton checkbox;
		checkbox = (RadioButton) findViewById(R.id.fwdunc);
		setRBDisplay(status, checkbox, getNumberForFeature(this, "unc"),
				getString(R.string.servicesFwdunc));
	}
	
	private void enableEnableDnd(final boolean status) {
		CheckBox checkbox;
		checkbox = (CheckBox) findViewById(R.id.enablednd);
		checkbox.setChecked(status);
	}
	
	/*
	private void enableCallrecord(final boolean status) {
		CheckBox checkbox;
		checkbox = (CheckBox) findViewById(R.id.callrecord);
		checkbox.setChecked(status);
	}
	
	private void enableIncallfilter(final boolean status) {
		CheckBox checkbox;
		checkbox = (CheckBox) findViewById(R.id.incallfilter);
		checkbox.setChecked(status);
	}
	*/
	
	/**
     * Change a feature value
     * 
     * @param feature The concerned feature (enablednd, enablerna, enablebusy, enableunc)
     * @param value 0 if disabled, 1 when enabled
     * @param phone The destination number for forwards, unused for DND
     */
	private void sendFeaturePut(String feature, String value, String phone){
		try {
			xivoConnectionService.sendFeature(feature, value, phone);
		} catch (RemoteException e) {
			Toast.makeText(this, getString(R.string.remote_exception), Toast.LENGTH_SHORT).show();
		}
	}
	
	/**
	 * Be sure that the receivers are unregistered.
	 */
	@Override
	protected void onDestroy() {
		unregisterReceiver(receiver);
		super.onDestroy();
	}
	
	/**
	 * Make the simple forward checkboxes clickable or not.
	 * @param isEnabled if they should be clickable.
	 */
	private void setSimpleForwardEnabled(boolean isEnabled) {
		((CheckBox) findViewById(R.id.fwdrna)).setClickable(isEnabled);
		((CheckBox) findViewById(R.id.fwdbusy)).setClickable(isEnabled);
		((CheckBox) findViewById(R.id.fwdrna)).setEnabled(isEnabled);
		((CheckBox) findViewById(R.id.fwdbusy)).setEnabled(isEnabled);
		((CheckBox) findViewById(R.id.fwdrna)).setTextColor
			(isEnabled ? getResources().getColor(R.color.appli_text_color)
					   : getResources().getColor(R.color.gray));
		((CheckBox) findViewById(R.id.fwdbusy)).setTextColor
		(isEnabled ? getResources().getColor(R.color.appli_text_color)
				   : getResources().getColor(R.color.gray));
	}
}
