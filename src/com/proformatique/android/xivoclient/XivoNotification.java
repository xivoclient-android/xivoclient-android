/* XiVO Client Android
 * Copyright (C) 2010-2011, Proformatique
 *
 * This file is part of XiVO Client Android.
 *
 * XiVO Client Android is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * XiVO Client Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with XiVO client Android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.proformatique.android.xivoclient;

import com.proformatique.android.xivoclient.service.XivoConnectionService.Favorite;
import com.proformatique.android.xivoclient.tools.Constants;
import com.proformatique.android.xivoclient.tools.GraphicsManager;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

/**
 * Manages the notification in Android's dropdown.
 */
public class XivoNotification {
	
	Context context;
	NotificationManager notifManager;
	Notification notification;
	int idRef = Constants.XIVO_NOTIF;
	Favorite[] favorites;
	
	boolean isAlreadyWaiting = false;
	boolean isLaunched = false;
	/**
	 * If the last call was to REMOVE the notification, it will be set to false.
	 * If it was to CREATE it, it will be true.
	 */
	boolean visible = false;
	long time = 0;
	
	/**
	 * Create the notification object.
	 * @param context The application Context
	 */
	public XivoNotification(Context context) {
		super();
		this.context = context;
	}
	
	/**
	 * Create the notification.
	 * @param favs The list of {@link Favorite} objects to show in the "big view". It will be ignored if the device is <= API 16 and if it is null, the "no favorite" message will be used.
	 */
	public void createNotification(Favorite[] favs){
		visible = true;
		favorites = favs;
		time = System.currentTimeMillis();
		if(isAlreadyWaiting) return; //another thread is already waiting to execute.
		
		//we need a thread again... because the notification generation is about a second long.
		Thread fil = new Thread("Notification Builder") {
			@Override public void run() {
				//wait until the previous thread is terminated
				isAlreadyWaiting = true;
				while(isLaunched) 
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				
				//wait 1 second (during the first loading, the create method is called
				//MANY times, so the notif. will be refreshed after 1 second without
				//any call to createNotification.)
				while(System.currentTimeMillis() - time < 1000)
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				isAlreadyWaiting = false;
				
				//give up if the notification should not be shown anymore
				if(!visible) return;
				
				isLaunched = true;
				Log.i("XiVO Notification", "Let's refresh the notification!");
				//build the "small" notification
				NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
				mBuilder.setSmallIcon(R.drawable.new_icon);
				mBuilder.setContentTitle(context.getString(R.string.notif_xivo_title));
				mBuilder.setContentText(context.getString(R.string.notif_xivo));
				mBuilder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.icon));
				
				//build the "big" notification
				//this array is for convenience (you want to display 6 contacts -> inflate layout[6])
				int[] layout = new int[] { 0, R.layout.notification_layout_1,
						R.layout.notification_layout_2,
						R.layout.notification_layout_3,
						R.layout.notification_layout_4,
						R.layout.notification_layout_5,
						R.layout.notification_layout_6,
						R.layout.notification_layout_7,
						R.layout.notification_layout_8,
						R.layout.notification_layout_9 };
				int viewId;
				if(favorites == null) {
					//favorites are not initialized yet.
					//we will display the "no favorite" message on a single line.
					viewId = 1;
				} else if(favorites.length == 0) {
					//later in the code, favorite == null displays the "no fav" message.
					favorites = null;
					//we will display the "no favorite" message on a single line.
					viewId = 1;
				} else if(favorites.length > 9) {
					//more than 9 people ; well, display 9.
					viewId = 9;
				} else {
					//just display the right amount of people.
					viewId = favorites.length;
				}
				
				//provides an Intent which makes the same basic thing as the launcher
				//icon in the app list (shows the last window the user was in).
				Intent intent = new Intent(context, AppOnBoot.class);
				intent.setAction(Intent.ACTION_MAIN);
				intent.addCategory(Intent.CATEGORY_LAUNCHER);
				
				//set up the pending intents
				PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, 
						intent, PendingIntent.FLAG_UPDATE_CURRENT);
				PendingIntent justDisconnect = PendingIntent.getActivity(context, 0, 
						new Intent(context, DisconnectNow.class), PendingIntent.FLAG_UPDATE_CURRENT);
				mBuilder.setContentIntent(pendingIntent);
				
				notification = mBuilder.build();
				
				//big notifications are only supported from Android 4.1.
				if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
					RemoteViews views = new RemoteViews(context.getPackageName(), layout[viewId]);
					
					//arrays of IDs for convenience (info about a user x is names[x], colors[x] and phones[x])
					int[] names = new int[] { R.id.name1, R.id.name2, R.id.name3, R.id.name4, R.id.name5,
							R.id.name6, R.id.name7, R.id.name8, R.id.name9 };
					int[] colors = new int[] { R.id.stat1, R.id.stat2, R.id.stat3, R.id.stat4, R.id.stat5,
							R.id.stat6, R.id.stat7, R.id.stat8, R.id.stat9 };
					int[] phones = new int[] { R.id.phone1, R.id.phone2, R.id.phone3, R.id.phone4, R.id.phone5,
							R.id.phone6, R.id.phone7, R.id.phone8, R.id.phone9 };
					
					if(favorites != null)
						for(int i = 0; i < 9; i++) {
							if(i < XivoNotification.this.favorites.length) {
								//update this favorite
								views.setTextViewText(names[i], favorites[i].userName);
								views.setImageViewBitmap(colors[i], 
										GraphicsManager.getIconStatusBitmap(context, favorites[i].userState));
								views.setImageViewBitmap(phones[i], 
										GraphicsManager.getIconPhoneBitmap(context, favorites[i].phoneState));
							}
							//show or hide the favorite, depending on if it exists or not.
							int visibility = i < XivoNotification.this.favorites.length ?
									View.VISIBLE : View.INVISIBLE;
							views.setViewVisibility(names[i], visibility);
							views.setViewVisibility(colors[i], visibility);
							views.setViewVisibility(phones[i], visibility);
						}
					else {
						//there is no favorite at all, just display this
						views.setTextViewText(names[0], context.getString(R.string.notification_no_favorites));
	
						for(int i = 0; i < 9; i++) {
							int visibility = i == 0 ? View.VISIBLE : View.INVISIBLE;
							views.setViewVisibility(names[i], visibility);
							views.setViewVisibility(colors[i], View.INVISIBLE);
							views.setViewVisibility(phones[i], View.INVISIBLE);
						}
					}
					views.setOnClickPendingIntent(R.id.disconnect_notif, justDisconnect);
					notification.bigContentView = views;
				}

				notifManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
				//the 2 flags are "Not clerable by simple swiping or by the Erase notifications button"
				//and "Do not display it in the "Notifications" part but in the "Ongoing events" part of the dropdown"
				//(this is the part where the non-clearable notifications usually go)
				notification.flags = Notification.FLAG_NO_CLEAR + Notification.FLAG_ONGOING_EVENT;
				
				if(visible) //the notification should always be built!
					try {
						notifManager.notify("XIVO", idRef, notification);
						isLaunched = false;
						Log.i("XiVO Notification", "Done!");
					} catch(RuntimeException ex) {
						//occasionally, an exception occurs here.
						//no notification update for this time...
						Log.w("XiVO Notification", "Error inflating notification! Dropping it.");
					}
			}
		};
		fil.start();
	}
	
	/**
	 * Clear the notification
	 */
	public void removeNotif() {
		visible = false;
		if(notifManager != null)
			notifManager.cancel("XIVO",idRef);
	}
}
