package com.proformatique.android.xivoclient;

import android.app.Activity;
import android.os.Bundle;

/**
 * Dummy activity.
 * It is used to bring XiVO Client to the foreground when a call is received.
 */
public class BringToFront extends Activity {

	/**
	 * Just closes on creation.
	 */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        finish();
    }

}